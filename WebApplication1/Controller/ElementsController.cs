﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;

namespace WebApplication1.Controller
{
    public class ElementsController : SurfaceController
    {
        private const string Partials_Views_Path = "~/Views/Partials/Elements/";

        public ActionResult RenderSectionText()
        {
            return PartialView(string.Format("{0}SectionText.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionTable()
        {
            return PartialView(string.Format("{0}SectionText.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionLists()
        {
            return PartialView(string.Format("{0}SectionLists.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionImage()
        {
            return PartialView(string.Format("{0}SectionImage.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionForm()
        {
            return PartialView(string.Format("{0}SectionForm.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionButtons()
        {
            return PartialView(string.Format("{0}SectionButtons.cshtml", Partials_Views_Path));
        }
    }
}