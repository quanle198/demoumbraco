﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace WebApplication1.Controller
{
    public class ContactController : SurfaceController
    {
        private const string Partials_Views_Path = "~/Views/Partials/SharedLayout/";
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RenderContact()
        {
            return PartialView(string.Format("{0}ContactPartialView.cshtml", Partials_Views_Path));
        }

        public ActionResult HandleSubmitForm(Contact model)
        {
            if (ModelState.IsValid)
            {
                var mesage = Services.ContentService
                    .CreateAndSave(string.Format("{0}-{1}", model.Name + "-", DateTime.Now.ToString()),
                    CurrentPage.Id, "contactContent");
                mesage.SetValue("userName", model.Name);
                mesage.SetValue("email", model.Email);
                mesage.SetValue("message", model.Message);
                mesage.SetValue("umbracoNaviHide", true);
                TempData["ContactUsSuccess"] = true;
                Services.ContentService.SaveAndPublish(mesage);
                SendMail(model);
                return RedirectToCurrentUmbracoPage();
            }
            return CurrentUmbracoPage();
        }
        private void SendMail(Contact model)
        {
            #region First way to send Email
            //var fromEmail = new MailAddress(ConfigurationManager.AppSettings["SendEmailFrom"]);
            //var fromPassword = ConfigurationManager.AppSettings["EmailPassword"];
            //var toAddress = new MailAddress(model.Email);
            //string subject = ConfigurationManager.AppSettings["EmailSubject"];
            //string body = model.Message;

            //var smtp = new SmtpClient
            //{
            //    Host = "smtp.gmail.com",
            //    Port = 587,
            //    EnableSsl = true,
            //    DeliveryMethod = SmtpDeliveryMethod.Network,
            //    UseDefaultCredentials = false,
            //    Credentials = new NetworkCredential(fromEmail.Address, fromPassword)
            //};

            //var message = new MailMessage(fromEmail, toAddress)
            //{
            //    Subject = subject,
            //    Body = body
            //};
            //smtp.Send(message);
            #endregion

            #region Second way to send Email
            var fromEmail = new MailAddress(ConfigurationManager.AppSettings["SendEmailFrom"]);
            var toAddress = new MailAddress(model.Email);
            string subject = ConfigurationManager.AppSettings["EmailSubject"];
            string body = model.Message;
            var message = new MailMessage(fromEmail, toAddress)
            {
                Subject = subject,
                Body = body
            };
            // connect to SMTP credentials in web.config
            try
            {
                var smtp = new SmtpClient();
                smtp.Send(message);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
            #endregion
        }
    }
}