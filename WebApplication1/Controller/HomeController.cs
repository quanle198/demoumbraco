﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace WebApplication1.Controller
{
    public class HomeController : SurfaceController
    {
        private const string Partials_Views_Path = "~/Views/Partials/Home/";

        public ActionResult RenderBanner()
        {
            return PartialView(string.Format("{0}Banner.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionOne()
        {
            return PartialView(string.Format("{0}SectionOne.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionTwo()
        {
            return PartialView(string.Format("{0}SectionTwo.cshtml", Partials_Views_Path));
        }

        public ActionResult RenderSectionThree()
        {
            return PartialView(string.Format("{0}SectionThree.cshtml", Partials_Views_Path));
        }
    }
}