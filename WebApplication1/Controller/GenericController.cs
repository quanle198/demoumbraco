﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace WebApplication1.Controller
{
    public class GenericController : SurfaceController
    {
        private const string Partials_Views_Path = "~/Views/Partials/Generic/";

        public ActionResult RenderGenericMainContent()
        {
            return PartialView(string.Format("{0}GenericMainContent.cshtml", Partials_Views_Path));
        }
    }
}