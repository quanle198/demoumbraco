﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;
using WebApplication1.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace WebApplication1.Controller
{
    public class SiteSharedLayoutController : SurfaceController
    {
        private const string Partials_Views_Path = "~/Views/Partials/SharedLayout/";

        public ActionResult RenderHeader()
        {
            List<NavigationList> nav = GetNavigationModel();
            return PartialView(string.Format("{0}Header.cshtml", Partials_Views_Path), nav);
        }

        public ActionResult RenderFooter()
        {
            return PartialView(string.Format("{0}Footer.cshtml", Partials_Views_Path));
        }

        public List<NavigationList> GetNavigationModel()
        {
            int pageId = int.Parse(CurrentPage.Path.Split(',')[1]); // There is the page position in path
            var pageInfo = Umbraco.Content(pageId);
            var nav = new List<NavigationList>
            {
                new NavigationList(new NavigationLinkInfo (pageInfo.Url, pageInfo.Name))
            };
            nav.AddRange(GetSubNavigationList(pageInfo));
            return nav;
        }

        public List<NavigationList> GetSubNavigationList(IPublishedContent page)
        {
            List<NavigationList> navList = null;
            var subPages = page.Children.Where(x => x.IsVisible());
            if (subPages != null)
            {
                navList = new List<NavigationList>();
                foreach (var subPage in subPages)
                {
                    var listItem = new NavigationList(new NavigationLinkInfo(subPage.Url, subPage.Name))
                    {
                        NavItems = GetSubNavigationList(subPage)
                    };
                    navList.Add(listItem);
                }
            }
            return navList;
        }
    }
}