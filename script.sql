USE [DemoUmbraco]
GO
/****** Object:  Table [dbo].[cmsContentNu]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsContentNu](
	[nodeId] [int] NOT NULL,
	[published] [bit] NOT NULL,
	[data] [ntext] NOT NULL,
	[rv] [bigint] NOT NULL,
 CONSTRAINT [PK_cmsContentNu] PRIMARY KEY CLUSTERED 
(
	[nodeId] ASC,
	[published] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsContentType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsContentType](
	[pk] [int] IDENTITY(1,1) NOT NULL,
	[nodeId] [int] NOT NULL,
	[alias] [nvarchar](255) NULL,
	[icon] [nvarchar](255) NULL,
	[thumbnail] [nvarchar](255) NOT NULL,
	[description] [nvarchar](1500) NULL,
	[isContainer] [bit] NOT NULL,
	[isElement] [bit] NOT NULL,
	[allowAtRoot] [bit] NOT NULL,
	[variations] [int] NOT NULL,
 CONSTRAINT [PK_cmsContentType] PRIMARY KEY CLUSTERED 
(
	[pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsContentType2ContentType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsContentType2ContentType](
	[parentContentTypeId] [int] NOT NULL,
	[childContentTypeId] [int] NOT NULL,
 CONSTRAINT [PK_cmsContentType2ContentType] PRIMARY KEY CLUSTERED 
(
	[parentContentTypeId] ASC,
	[childContentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsContentTypeAllowedContentType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsContentTypeAllowedContentType](
	[Id] [int] NOT NULL,
	[AllowedId] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_cmsContentTypeAllowedContentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[AllowedId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsDictionary]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsDictionary](
	[pk] [int] IDENTITY(1,1) NOT NULL,
	[id] [uniqueidentifier] NOT NULL,
	[parent] [uniqueidentifier] NULL,
	[key] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_cmsDictionary] PRIMARY KEY CLUSTERED 
(
	[pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsDocumentType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsDocumentType](
	[contentTypeNodeId] [int] NOT NULL,
	[templateNodeId] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_cmsDocumentType] PRIMARY KEY CLUSTERED 
(
	[contentTypeNodeId] ASC,
	[templateNodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsLanguageText]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsLanguageText](
	[pk] [int] IDENTITY(1,1) NOT NULL,
	[languageId] [int] NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[value] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_cmsLanguageText] PRIMARY KEY CLUSTERED 
(
	[pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsMacro]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsMacro](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uniqueId] [uniqueidentifier] NOT NULL,
	[macroUseInEditor] [bit] NOT NULL,
	[macroRefreshRate] [int] NOT NULL,
	[macroAlias] [nvarchar](255) NOT NULL,
	[macroName] [nvarchar](255) NULL,
	[macroCacheByPage] [bit] NOT NULL,
	[macroCachePersonalized] [bit] NOT NULL,
	[macroDontRender] [bit] NOT NULL,
	[macroSource] [nvarchar](255) NOT NULL,
	[macroType] [int] NOT NULL,
 CONSTRAINT [PK_cmsMacro] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsMacroProperty]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsMacroProperty](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uniquePropertyId] [uniqueidentifier] NOT NULL,
	[editorAlias] [nvarchar](255) NOT NULL,
	[macro] [int] NOT NULL,
	[macroPropertySortOrder] [int] NOT NULL,
	[macroPropertyAlias] [nvarchar](50) NOT NULL,
	[macroPropertyName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_cmsMacroProperty] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsMember]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsMember](
	[nodeId] [int] NOT NULL,
	[Email] [nvarchar](1000) NOT NULL,
	[LoginName] [nvarchar](1000) NOT NULL,
	[Password] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_cmsMember] PRIMARY KEY CLUSTERED 
(
	[nodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsMember2MemberGroup]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsMember2MemberGroup](
	[Member] [int] NOT NULL,
	[MemberGroup] [int] NOT NULL,
 CONSTRAINT [PK_cmsMember2MemberGroup] PRIMARY KEY CLUSTERED 
(
	[Member] ASC,
	[MemberGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsMemberType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsMemberType](
	[pk] [int] IDENTITY(1,1) NOT NULL,
	[NodeId] [int] NOT NULL,
	[propertytypeId] [int] NOT NULL,
	[memberCanEdit] [bit] NOT NULL,
	[viewOnProfile] [bit] NOT NULL,
	[isSensitive] [bit] NOT NULL,
 CONSTRAINT [PK_cmsMemberType] PRIMARY KEY CLUSTERED 
(
	[pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsPropertyType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsPropertyType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dataTypeId] [int] NOT NULL,
	[contentTypeId] [int] NOT NULL,
	[propertyTypeGroupId] [int] NULL,
	[Alias] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[sortOrder] [int] NOT NULL,
	[mandatory] [bit] NOT NULL,
	[validationRegExp] [nvarchar](255) NULL,
	[Description] [nvarchar](2000) NULL,
	[variations] [int] NOT NULL,
	[UniqueID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_cmsPropertyType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsPropertyTypeGroup]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsPropertyTypeGroup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[contenttypeNodeId] [int] NOT NULL,
	[text] [nvarchar](255) NOT NULL,
	[sortorder] [int] NOT NULL,
	[uniqueID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_cmsPropertyTypeGroup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsTagRelationship]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsTagRelationship](
	[nodeId] [int] NOT NULL,
	[tagId] [int] NOT NULL,
	[propertyTypeId] [int] NOT NULL,
 CONSTRAINT [PK_cmsTagRelationship] PRIMARY KEY CLUSTERED 
(
	[nodeId] ASC,
	[propertyTypeId] ASC,
	[tagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsTags]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsTags](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group] [nvarchar](100) NOT NULL,
	[languageId] [int] NULL,
	[tag] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_cmsTags] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cmsTemplate]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cmsTemplate](
	[pk] [int] IDENTITY(1,1) NOT NULL,
	[nodeId] [int] NOT NULL,
	[alias] [nvarchar](100) NULL,
 CONSTRAINT [PK_cmsTemplate] PRIMARY KEY CLUSTERED 
(
	[pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoAccess]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoAccess](
	[id] [uniqueidentifier] NOT NULL,
	[nodeId] [int] NOT NULL,
	[loginNodeId] [int] NOT NULL,
	[noAccessNodeId] [int] NOT NULL,
	[createDate] [datetime] NOT NULL,
	[updateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_umbracoAccess] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoAccessRule]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoAccessRule](
	[id] [uniqueidentifier] NOT NULL,
	[accessId] [uniqueidentifier] NOT NULL,
	[ruleValue] [nvarchar](255) NOT NULL,
	[ruleType] [nvarchar](255) NOT NULL,
	[createDate] [datetime] NOT NULL,
	[updateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_umbracoAccessRule] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoAudit]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoAudit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[performingUserId] [int] NOT NULL,
	[performingDetails] [nvarchar](1024) NULL,
	[performingIp] [nvarchar](64) NULL,
	[eventDateUtc] [datetime] NOT NULL,
	[affectedUserId] [int] NOT NULL,
	[affectedDetails] [nvarchar](1024) NULL,
	[eventType] [nvarchar](256) NOT NULL,
	[eventDetails] [nvarchar](1024) NULL,
 CONSTRAINT [PK_umbracoAudit] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoCacheInstruction]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoCacheInstruction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[utcStamp] [datetime] NOT NULL,
	[jsonInstruction] [ntext] NOT NULL,
	[originated] [nvarchar](500) NOT NULL,
	[instructionCount] [int] NOT NULL,
 CONSTRAINT [PK_umbracoCacheInstruction] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoConsent]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoConsent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[current] [bit] NOT NULL,
	[source] [nvarchar](512) NOT NULL,
	[context] [nvarchar](128) NOT NULL,
	[action] [nvarchar](512) NOT NULL,
	[createDate] [datetime] NOT NULL,
	[state] [int] NOT NULL,
	[comment] [nvarchar](255) NULL,
 CONSTRAINT [PK_umbracoConsent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoContent]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoContent](
	[nodeId] [int] NOT NULL,
	[contentTypeId] [int] NOT NULL,
 CONSTRAINT [PK_umbracoContent] PRIMARY KEY CLUSTERED 
(
	[nodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoContentSchedule]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoContentSchedule](
	[id] [uniqueidentifier] NOT NULL,
	[nodeId] [int] NOT NULL,
	[languageId] [int] NULL,
	[date] [datetime] NOT NULL,
	[action] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_umbracoContentSchedule] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoContentVersion]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoContentVersion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nodeId] [int] NOT NULL,
	[versionDate] [datetime] NOT NULL,
	[userId] [int] NULL,
	[current] [bit] NOT NULL,
	[text] [nvarchar](255) NULL,
 CONSTRAINT [PK_umbracoContentVersion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoContentVersionCultureVariation]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoContentVersionCultureVariation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[versionId] [int] NOT NULL,
	[languageId] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[date] [datetime] NOT NULL,
	[availableUserId] [int] NULL,
 CONSTRAINT [PK_umbracoContentVersionCultureVariation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoDataType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoDataType](
	[nodeId] [int] NOT NULL,
	[propertyEditorAlias] [nvarchar](255) NOT NULL,
	[dbType] [nvarchar](50) NOT NULL,
	[config] [ntext] NULL,
 CONSTRAINT [PK_umbracoDataType] PRIMARY KEY CLUSTERED 
(
	[nodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoDocument]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoDocument](
	[nodeId] [int] NOT NULL,
	[published] [bit] NOT NULL,
	[edited] [bit] NOT NULL,
 CONSTRAINT [PK_umbracoDocument] PRIMARY KEY CLUSTERED 
(
	[nodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoDocumentCultureVariation]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoDocumentCultureVariation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nodeId] [int] NOT NULL,
	[languageId] [int] NOT NULL,
	[edited] [bit] NOT NULL,
	[available] [bit] NOT NULL,
	[published] [bit] NOT NULL,
	[name] [nvarchar](255) NULL,
 CONSTRAINT [PK_umbracoDocumentCultureVariation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoDocumentVersion]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoDocumentVersion](
	[id] [int] NOT NULL,
	[templateId] [int] NULL,
	[published] [bit] NOT NULL,
 CONSTRAINT [PK_umbracoDocumentVersion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoDomain]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoDomain](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[domainDefaultLanguage] [int] NULL,
	[domainRootStructureID] [int] NULL,
	[domainName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_umbracoDomain] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoExternalLogin]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoExternalLogin](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[loginProvider] [nvarchar](4000) NOT NULL,
	[providerKey] [nvarchar](4000) NOT NULL,
	[createDate] [datetime] NOT NULL,
 CONSTRAINT [PK_umbracoExternalLogin] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoKeyValue]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoKeyValue](
	[key] [nvarchar](256) NOT NULL,
	[value] [nvarchar](255) NULL,
	[updated] [datetime] NOT NULL,
 CONSTRAINT [PK_umbracoKeyValue] PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoLanguage]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoLanguage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[languageISOCode] [nvarchar](14) NULL,
	[languageCultureName] [nvarchar](100) NULL,
	[isDefaultVariantLang] [bit] NOT NULL,
	[mandatory] [bit] NOT NULL,
	[fallbackLanguageId] [int] NULL,
 CONSTRAINT [PK_umbracoLanguage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoLock]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoLock](
	[id] [int] NOT NULL,
	[value] [int] NOT NULL,
	[name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_umbracoLock] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoLog]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[NodeId] [int] NOT NULL,
	[entityType] [nvarchar](50) NULL,
	[Datestamp] [datetime] NOT NULL,
	[logHeader] [nvarchar](50) NOT NULL,
	[logComment] [nvarchar](4000) NULL,
	[parameters] [nvarchar](500) NULL,
 CONSTRAINT [PK_umbracoLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoMediaVersion]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoMediaVersion](
	[id] [int] NOT NULL,
	[path] [nvarchar](255) NULL,
 CONSTRAINT [PK_umbracoMediaVersion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoNode]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoNode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uniqueId] [uniqueidentifier] NOT NULL,
	[parentId] [int] NOT NULL,
	[level] [int] NOT NULL,
	[path] [nvarchar](150) NOT NULL,
	[sortOrder] [int] NOT NULL,
	[trashed] [bit] NOT NULL,
	[nodeUser] [int] NULL,
	[text] [nvarchar](255) NULL,
	[nodeObjectType] [uniqueidentifier] NULL,
	[createDate] [datetime] NOT NULL,
 CONSTRAINT [PK_umbracoNode] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoPropertyData]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoPropertyData](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[versionId] [int] NOT NULL,
	[propertyTypeId] [int] NOT NULL,
	[languageId] [int] NULL,
	[segment] [nvarchar](256) NULL,
	[intValue] [int] NULL,
	[decimalValue] [decimal](38, 6) NULL,
	[dateValue] [datetime] NULL,
	[varcharValue] [nvarchar](512) NULL,
	[textValue] [ntext] NULL,
 CONSTRAINT [PK_umbracoPropertyData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoRedirectUrl]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoRedirectUrl](
	[id] [uniqueidentifier] NOT NULL,
	[contentKey] [uniqueidentifier] NOT NULL,
	[createDateUtc] [datetime] NOT NULL,
	[url] [nvarchar](255) NOT NULL,
	[culture] [nvarchar](255) NULL,
	[urlHash] [nvarchar](40) NOT NULL,
 CONSTRAINT [PK_umbracoRedirectUrl] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoRelation]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoRelation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parentId] [int] NOT NULL,
	[childId] [int] NOT NULL,
	[relType] [int] NOT NULL,
	[datetime] [datetime] NOT NULL,
	[comment] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_umbracoRelation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoRelationType]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoRelationType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[typeUniqueId] [uniqueidentifier] NOT NULL,
	[dual] [bit] NOT NULL,
	[parentObjectType] [uniqueidentifier] NOT NULL,
	[childObjectType] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[alias] [nvarchar](100) NULL,
 CONSTRAINT [PK_umbracoRelationType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoServer]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoServer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[address] [nvarchar](500) NOT NULL,
	[computerName] [nvarchar](255) NOT NULL,
	[registeredDate] [datetime] NOT NULL,
	[lastNotifiedDate] [datetime] NOT NULL,
	[isActive] [bit] NOT NULL,
	[isMaster] [bit] NOT NULL,
 CONSTRAINT [PK_umbracoServer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUser]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUser](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userDisabled] [bit] NOT NULL,
	[userNoConsole] [bit] NOT NULL,
	[userName] [nvarchar](255) NOT NULL,
	[userLogin] [nvarchar](125) NOT NULL,
	[userPassword] [nvarchar](500) NOT NULL,
	[passwordConfig] [nvarchar](500) NULL,
	[userEmail] [nvarchar](255) NOT NULL,
	[userLanguage] [nvarchar](10) NULL,
	[securityStampToken] [nvarchar](255) NULL,
	[failedLoginAttempts] [int] NULL,
	[lastLockoutDate] [datetime] NULL,
	[lastPasswordChangeDate] [datetime] NULL,
	[lastLoginDate] [datetime] NULL,
	[emailConfirmedDate] [datetime] NULL,
	[invitedDate] [datetime] NULL,
	[createDate] [datetime] NOT NULL,
	[updateDate] [datetime] NOT NULL,
	[avatar] [nvarchar](500) NULL,
	[tourData] [ntext] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUser2NodeNotify]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUser2NodeNotify](
	[userId] [int] NOT NULL,
	[nodeId] [int] NOT NULL,
	[action] [nchar](1) NOT NULL,
 CONSTRAINT [PK_umbracoUser2NodeNotify] PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[nodeId] ASC,
	[action] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUser2UserGroup]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUser2UserGroup](
	[userId] [int] NOT NULL,
	[userGroupId] [int] NOT NULL,
 CONSTRAINT [PK_user2userGroup] PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[userGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUserGroup]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUserGroup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userGroupAlias] [nvarchar](200) NOT NULL,
	[userGroupName] [nvarchar](200) NOT NULL,
	[userGroupDefaultPermissions] [nvarchar](50) NULL,
	[createDate] [datetime] NOT NULL,
	[updateDate] [datetime] NOT NULL,
	[icon] [nvarchar](255) NULL,
	[startContentId] [int] NULL,
	[startMediaId] [int] NULL,
 CONSTRAINT [PK_umbracoUserGroup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUserGroup2App]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUserGroup2App](
	[userGroupId] [int] NOT NULL,
	[app] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_userGroup2App] PRIMARY KEY CLUSTERED 
(
	[userGroupId] ASC,
	[app] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUserGroup2NodePermission]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUserGroup2NodePermission](
	[userGroupId] [int] NOT NULL,
	[nodeId] [int] NOT NULL,
	[permission] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_umbracoUserGroup2NodePermission] PRIMARY KEY CLUSTERED 
(
	[userGroupId] ASC,
	[nodeId] ASC,
	[permission] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUserLogin]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUserLogin](
	[sessionId] [uniqueidentifier] NOT NULL,
	[userId] [int] NOT NULL,
	[loggedInUtc] [datetime] NOT NULL,
	[lastValidatedUtc] [datetime] NOT NULL,
	[loggedOutUtc] [datetime] NULL,
	[ipAddress] [nvarchar](255) NULL,
 CONSTRAINT [PK_umbracoUserLogin] PRIMARY KEY CLUSTERED 
(
	[sessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[umbracoUserStartNode]    Script Date: 10/11/2019 6:08:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[umbracoUserStartNode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[startNode] [int] NOT NULL,
	[startNodeType] [int] NOT NULL,
 CONSTRAINT [PK_userStartNode] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1119, 0, N'{"properties":{},"cultureData":{},"urlSegment":"design"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1120, 0, N'{"properties":{},"cultureData":{},"urlSegment":"people"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1121, 0, N'{"properties":{},"cultureData":{},"urlSegment":"products"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1122, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/662af6ca411a4c93a6c722c4845698e7/00000006000000000000000000000000/16403439029_f500be349b_o.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"umbraco-campari-meeting-room"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1123, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/55514845b8bd487cb3709724852fd6bb/00000006000000000000000000000000/4730684907_8a7f8759cb_b.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"biker-jacket"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1124, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/20e3a8ffad1b4fe9b48cb8461c46d2d0/00000006000000000000000000000000/7371127652_e01b6ab56f_b.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"tattoo"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1125, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/1bc5280b8658402789d958e2576e469b/00000006000000000000000000000000/14272036539_469ca21d5c_h.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"unicorn"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1126, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/c09ec77f08e3466aac58c979befd3cd6/00000006000000000000000000000000/5852022211_9028df67c0_b.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"ping-pong-ball"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1127, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/b76ddb4ee603401499066087984740ec/00000006000000000000000000000000/5852022091_87c5d045ab_b.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"bowling-ball"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1128, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/46a025d6095a4b148b961b59ca4e951c/00000006000000000000000000000000/7377957524_347859faac_b.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"jumpsuit"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1129, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/17552d12081d4d01b68132c495d6576f/00000006000000000000000000000000/7373036290_5e8420bf36_b.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"banjo"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1130, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/1d0b713a022a49c8b842a2463c83a553/00000006000000000000000000000000/7373036208_30257976a0_b.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"knitted-west"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1131, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/cf1ab8dcad0f4a8e974b87b84777b0d6/00000006000000000000000000000000/18720470241_ff77768544_h.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"jan-skovgaard"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1132, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/eee91c05b2e84031a056dcd7f28eff89/00000006000000000000000000000000/18531852339_981b067419_h.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"matt-brailsford"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1133, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/fa763e0d0ceb408c8720365d57e06e32/00000006000000000000000000000000/18531854019_351c579559_h.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"lee-kelleher"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1134, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/c0969cab13ab4de9819a848619ac2b5d/00000006000000000000000000000000/18095416144_44a566a5f4_h.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"jeavon-leopold"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1135, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/34371d0892c84015912ebaacd002c5d0/00000006000000000000000000000000/18530280048_459b8b61b2_h.jpg\",\"focalPoint\":null,\"crops\":null}"}],"umbracoWidth":[],"umbracoHeight":[],"umbracoBytes":[],"umbracoExtension":[]},"cultureData":{},"urlSegment":"jeroen-breuer"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1137, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/lnnnp4wz/2bd58947-57fb-4dbd-b202-f5884830bcfb_4c1a0bcd-e53c-4ba9-96a6-f8cbe1935013_2.jpg\",\"crops\":null}"}],"umbracoWidth":[{"culture":"","seg":"","val":1920}],"umbracoHeight":[{"culture":"","seg":"","val":1256}],"umbracoBytes":[{"culture":"","seg":"","val":"77482"}],"umbracoExtension":[{"culture":"","seg":"","val":"jpg"}]},"cultureData":{},"urlSegment":"2bd58947-57fb-4dbd-b202-f5884830bcfb-4c1a0bcd-e53c-4ba9-96a6-f8cbe1935013-2"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1141, 0, N'{"properties":{"introduction":[{"culture":"","seg":"","val":"<p>Welcome to my channel !!!</p>"}],"helloBanner":[{"culture":"","seg":"","val":"/media/y1eottnb/banner.jpg"}],"introHyperLink":[{"culture":"","seg":"","val":"/about-us"}],"introLinkLabel":[{"culture":"","seg":"","val":"About"}],"welcomeTitle":[{"culture":"","seg":"","val":"MAGNA ETIAM LOREM Quan"}],"welcomeSummary":[{"culture":"","seg":"","val":"Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante."}],"welcomeLink":[{"culture":"","seg":"","val":"/welcome"}],"hyperLinkText":[{"culture":"","seg":"","val":"Click Here"}],"languageFlag":[{"culture":"","seg":"","val":"/media/k30bkhnr/american.png"}]},"cultureData":{},"urlSegment":"en"}', 3)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1141, 1, N'{"properties":{"introduction":[{"culture":"","seg":"","val":"<p>Welcome to my channel !!!</p>"}],"helloBanner":[{"culture":"","seg":"","val":"/media/y1eottnb/banner.jpg"}],"introHyperLink":[{"culture":"","seg":"","val":"/about-us"}],"introLinkLabel":[{"culture":"","seg":"","val":"About"}],"welcomeTitle":[{"culture":"","seg":"","val":"MAGNA ETIAM LOREM Quan"}],"welcomeSummary":[{"culture":"","seg":"","val":"Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante."}],"welcomeLink":[{"culture":"","seg":"","val":"/welcome"}],"hyperLinkText":[{"culture":"","seg":"","val":"Click Here"}],"languageFlag":[{"culture":"","seg":"","val":"/media/k30bkhnr/american.png"}]},"cultureData":{},"urlSegment":"en"}', 3)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1147, 0, N'{"properties":{"umbracoNaviHide":[]},"cultureData":{},"urlSegment":"generic"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1148, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":0}]},"cultureData":{},"urlSegment":"generic"}', 5)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1148, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":0}]},"cultureData":{},"urlSegment":"generic"}', 5)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1149, 0, N'{"properties":{},"cultureData":{},"urlSegment":"elements"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1149, 1, N'{"properties":{},"cultureData":{},"urlSegment":"elements"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1150, 0, N'{"properties":{"introduction":[],"helloBanner":[],"introHyperLink":[],"introLinkLabel":[],"welcomeTitle":[],"welcomeSummary":[],"welcomeLink":[],"hyperLinkText":[],"languageFlag":[]},"cultureData":{},"urlSegment":"testfornewcontent"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1150, 1, N'{"properties":{"introduction":[],"helloBanner":[],"introHyperLink":[],"introLinkLabel":[],"welcomeTitle":[],"welcomeSummary":[],"welcomeLink":[],"hyperLinkText":[],"languageFlag":[]},"cultureData":{},"urlSegment":"testfornewcontent"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1154, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"about-us"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1154, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"about-us"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1155, 0, N'{"properties":{"umbracoNavHides":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"company-overview"}', 2)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1155, 1, N'{"properties":{"umbracoNavHides":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"company-overview"}', 2)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1156, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"privacy"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1156, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"privacy"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1157, 0, N'{"properties":{"umbracoNaviHide":[]},"cultureData":{},"urlSegment":"disclamer"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1157, 1, N'{"properties":{"umbracoNaviHide":[]},"cultureData":{},"urlSegment":"disclamer"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1162, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"welcome"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1162, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"welcome"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1170, 0, N'{"properties":{"newsBodyText":[{"culture":"","seg":"","val":"<p>This is the news page which contains the list of news in the system should be display here.</p>"}]},"cultureData":{},"urlSegment":"news"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1170, 1, N'{"properties":{"newsBodyText":[{"culture":"","seg":"","val":"<p>This is the news page which contains the list of news in the system should be display here.</p>"}]},"cultureData":{},"urlSegment":"news"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1171, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-1"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1171, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-1"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1172, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-2"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1172, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-2"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1173, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-3"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1173, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-3"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1178, 0, N'{"properties":{},"cultureData":{},"urlSegment":"contact-us"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1178, 1, N'{"properties":{},"cultureData":{},"urlSegment":"contact-us"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1179, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Quan Le"}],"email":[{"culture":"","seg":"","val":"quanle@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaaaa"}]},"cultureData":{},"urlSegment":"quanle-2907"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1179, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Quan Le"}],"email":[{"culture":"","seg":"","val":"quanle@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaaaa"}]},"cultureData":{},"urlSegment":"quanle-2907"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1180, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quan"}],"email":[{"culture":"","seg":"","val":"quan"}],"message":[{"culture":"","seg":"","val":"quan"}]},"cultureData":{},"urlSegment":"quan-10-9-2019-11-59-07-am"}', 2)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1180, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quan"}],"email":[{"culture":"","seg":"","val":"quan"}],"message":[{"culture":"","seg":"","val":"quan"}]},"cultureData":{},"urlSegment":"quan-10-9-2019-11-59-07-am"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1181, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"Xin chào các bạn, \r\nMình tên là quân.\r\nHCM"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-12-00-39-pm"}', 2)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1181, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"Xin chào các bạn, \r\nMình tên là quân.\r\nHCM"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-12-00-39-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1182, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"adminne"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaaasssss"}]},"cultureData":{},"urlSegment":"adminne-10-9-2019-1-43-08-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1182, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"adminne"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaaasssss"}]},"cultureData":{},"urlSegment":"adminne-10-9-2019-1-43-08-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1183, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Nhi"}],"email":[{"culture":"","seg":"","val":"quanfoxy@gmail.com"}],"message":[{"culture":"","seg":"","val":"hahahaahah"}]},"cultureData":{},"urlSegment":"nhi-10-9-2019-1-51-19-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1183, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Nhi"}],"email":[{"culture":"","seg":"","val":"quanfoxy@gmail.com"}],"message":[{"culture":"","seg":"","val":"hahahaahah"}]},"cultureData":{},"urlSegment":"nhi-10-9-2019-1-51-19-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1184, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaa"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-39-06-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1184, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaa"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-39-06-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1185, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaa"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-49-32-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1185, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaa"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-49-32-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1186, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"welcome to my chanel"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-50-04-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1186, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"welcome to my chanel"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-50-04-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1187, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"welcome to my chanel"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-2-50-46-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1187, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"welcome to my chanel"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-2-50-46-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1188, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Product"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"second way to send mail"}]},"cultureData":{},"urlSegment":"product-10-9-2019-3-33-13-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1188, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Product"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"second way to send mail"}]},"cultureData":{},"urlSegment":"product-10-9-2019-3-33-13-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1189, 0, N'{"properties":{"introduction":[{"culture":"","seg":"","val":"<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>"}],"helloBanner":[{"culture":"","seg":"","val":"/media/y1eottnb/banner.jpg"}],"introHyperLink":[{"culture":"","seg":"","val":"/about-us"}],"introLinkLabel":[{"culture":"","seg":"","val":"About"}],"welcomeTitle":[{"culture":"","seg":"","val":"MAGNA ETIAM LOREM Quan"}],"welcomeSummary":[{"culture":"","seg":"","val":"Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante."}],"welcomeLink":[{"culture":"","seg":"","val":"/welcome"}],"hyperLinkText":[{"culture":"","seg":"","val":"Click Here"}],"languageFlag":[{"culture":"","seg":"","val":"/media/dy4gvogl/vn.png"}]},"cultureData":{},"urlSegment":"vn"}', 4)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1189, 1, N'{"properties":{"introduction":[{"culture":"","seg":"","val":"<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>"}],"helloBanner":[{"culture":"","seg":"","val":"/media/y1eottnb/banner.jpg"}],"introHyperLink":[{"culture":"","seg":"","val":"/about-us"}],"introLinkLabel":[{"culture":"","seg":"","val":"About"}],"welcomeTitle":[{"culture":"","seg":"","val":"MAGNA ETIAM LOREM Quan"}],"welcomeSummary":[{"culture":"","seg":"","val":"Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante."}],"welcomeLink":[{"culture":"","seg":"","val":"/welcome"}],"hyperLinkText":[{"culture":"","seg":"","val":"Click Here"}],"languageFlag":[{"culture":"","seg":"","val":"/media/dy4gvogl/vn.png"}]},"cultureData":{},"urlSegment":"vn"}', 4)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1190, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":0}]},"cultureData":{},"urlSegment":"generic"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1190, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":0}]},"cultureData":{},"urlSegment":"generic"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1191, 0, N'{"properties":{},"cultureData":{},"urlSegment":"elements"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1191, 1, N'{"properties":{},"cultureData":{},"urlSegment":"elements"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1192, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"gioi-thieu-ve-cong-ty"}', 2)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1192, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"gioi-thieu-ve-cong-ty"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1193, 0, N'{"properties":{"umbracoNaviHide":[]},"cultureData":{},"urlSegment":"company-overview"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1194, 0, N'{"properties":{"umbracoNaviHide":[]},"cultureData":{},"urlSegment":"privacy"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1195, 0, N'{"properties":{"umbracoNaviHide":[]},"cultureData":{},"urlSegment":"disclamer"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1196, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"chao-mung"}', 2)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1196, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}]},"cultureData":{},"urlSegment":"chao-mung"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1197, 0, N'{"properties":{"newsBodyText":[{"culture":"","seg":"","val":"<p>This is the news page which contains the list of news in the system should be display here.</p>"}]},"cultureData":{},"urlSegment":"tin-tuc"}', 4)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1197, 1, N'{"properties":{"newsBodyText":[{"culture":"","seg":"","val":"<p>This is the news page which contains the list of news in the system should be display here.</p>"}]},"cultureData":{},"urlSegment":"tin-tuc"}', 3)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1198, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"tin-tuc-1"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1198, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"tin-tuc-1"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1199, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"tin-tuc-2"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1199, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p> </p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"tin-tuc-2"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1200, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-3"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1200, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}],"newsImage":[]},"cultureData":{},"urlSegment":"news-3"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1201, 0, N'{"properties":{},"cultureData":{},"urlSegment":"lien-he"}', 2)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1201, 1, N'{"properties":{},"cultureData":{},"urlSegment":"lien-he"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1202, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quan"}],"email":[{"culture":"","seg":"","val":"quan"}],"message":[{"culture":"","seg":"","val":"quan"}]},"cultureData":{},"urlSegment":"quan-10-9-2019-11-59-07-am"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1203, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"Xin chào các bạn, \r\nMình tên là quân.\r\nHCM"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-12-00-39-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1204, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"adminne"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaaasssss"}]},"cultureData":{},"urlSegment":"adminne-10-9-2019-1-43-08-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1205, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Nhi"}],"email":[{"culture":"","seg":"","val":"quanfoxy@gmail.com"}],"message":[{"culture":"","seg":"","val":"hahahaahah"}]},"cultureData":{},"urlSegment":"nhi-10-9-2019-1-51-19-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1206, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaa"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-39-06-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1207, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"aaa"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-49-32-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1208, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Lux A2.0"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"welcome to my chanel"}]},"cultureData":{},"urlSegment":"lux-a2-0-10-9-2019-2-50-04-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1209, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"welcome to my chanel"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-2-50-46-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1210, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"Product"}],"email":[{"culture":"","seg":"","val":"quanfx98@gmail.com"}],"message":[{"culture":"","seg":"","val":"second way to send mail"}]},"cultureData":{},"urlSegment":"product-10-9-2019-3-33-13-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1212, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"chào bạn quan "}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-8-10-05-pm"}', 1)
GO
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1212, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"chào bạn quan "}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-8-10-05-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1213, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"sssssssssssss"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-9-29-27-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1213, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"quanfx"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"sssssssssssss"}]},"cultureData":{},"urlSegment":"quanfx-10-9-2019-9-29-27-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1214, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"admin"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"cuong an cac"}]},"cultureData":{},"urlSegment":"admin-10-9-2019-9-30-37-pm"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1214, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"userName":[{"culture":"","seg":"","val":"admin"}],"email":[{"culture":"","seg":"","val":"q.foxy98@gmail.com"}],"message":[{"culture":"","seg":"","val":"cuong an cac"}]},"cultureData":{},"urlSegment":"admin-10-9-2019-9-30-37-pm"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1215, 0, N'{"properties":{"umbracoFile":[{"culture":"","seg":"","val":"{\"src\":\"/media/hwwde1d1/vn.png\",\"crops\":null}"}],"umbracoWidth":[{"culture":"","seg":"","val":320}],"umbracoHeight":[{"culture":"","seg":"","val":320}],"umbracoBytes":[{"culture":"","seg":"","val":"40947"}],"umbracoExtension":[{"culture":"","seg":"","val":"png"}]},"cultureData":{},"urlSegment":"vn"}', 0)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1216, 0, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>New4 content</p>"}],"newsImage":[{"culture":"","seg":"","val":"umb://media/6c32ad3b197b4edea3efa7b934bb1561"}]},"cultureData":{},"urlSegment":"new4"}', 1)
INSERT [dbo].[cmsContentNu] ([nodeId], [published], [data], [rv]) VALUES (1216, 1, N'{"properties":{"umbracoNaviHide":[{"culture":"","seg":"","val":1}],"newsBodyText":[{"culture":"","seg":"","val":"<p>New4 content</p>"}],"newsImage":[{"culture":"","seg":"","val":"umb://media/6c32ad3b197b4edea3efa7b934bb1561"}]},"cultureData":{},"urlSegment":"new4"}', 1)
SET IDENTITY_INSERT [dbo].[cmsContentType] ON 

INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (531, 1044, N'Member', N'icon-user', N'icon-user', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (532, 1031, N'Folder', N'icon-folder', N'icon-folder', NULL, 0, 0, 1, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (533, 1032, N'Image', N'icon-picture', N'icon-picture', NULL, 0, 0, 1, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (534, 1033, N'File', N'icon-document', N'icon-document', NULL, 0, 0, 1, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (547, 1140, N'home', N'icon-home color-black', N'folder.png', NULL, 0, 0, 1, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (548, 1145, N'generic', N'icon-comb color-cyan', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (549, 1146, N'elements', N'icon-axis-rotation color-cyan', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (550, 1152, N'aboutUs', N'icon-theif color-cyan', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (551, 1153, N'companyOverview', N'icon-zoom-out color-cyan', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (552, 1160, N'introductionControl', N'icon-notepad color-black', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (553, 1164, N'hidePageFromNavigation', N'icon-document', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (554, 1167, N'news', N'icon-newspaper color-blue', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (555, 1169, N'newsDetail', N'icon-newspaper-alt color-blue', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (556, 1175, N'contact', N'icon-old-phone color-blue', N'folder.png', NULL, 0, 0, 0, 0)
INSERT [dbo].[cmsContentType] ([pk], [nodeId], [alias], [icon], [thumbnail], [description], [isContainer], [isElement], [allowAtRoot], [variations]) VALUES (557, 1177, N'contactContent', N'icon-document', N'folder.png', NULL, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[cmsContentType] OFF
INSERT [dbo].[cmsContentType2ContentType] ([parentContentTypeId], [childContentTypeId]) VALUES (1160, 1140)
INSERT [dbo].[cmsContentType2ContentType] ([parentContentTypeId], [childContentTypeId]) VALUES (1164, 1145)
INSERT [dbo].[cmsContentType2ContentType] ([parentContentTypeId], [childContentTypeId]) VALUES (1164, 1152)
INSERT [dbo].[cmsContentType2ContentType] ([parentContentTypeId], [childContentTypeId]) VALUES (1164, 1153)
INSERT [dbo].[cmsContentType2ContentType] ([parentContentTypeId], [childContentTypeId]) VALUES (1164, 1169)
INSERT [dbo].[cmsContentType2ContentType] ([parentContentTypeId], [childContentTypeId]) VALUES (1164, 1177)
INSERT [dbo].[cmsContentType2ContentType] ([parentContentTypeId], [childContentTypeId]) VALUES (1167, 1169)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1031, 1031, 0)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1031, 1032, 0)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1031, 1033, 0)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1140, 1145, 0)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1140, 1146, 1)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1140, 1152, 2)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1140, 1167, 3)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1140, 1175, 4)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1152, 1145, 0)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1152, 1153, 1)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1167, 1169, 0)
INSERT [dbo].[cmsContentTypeAllowedContentType] ([Id], [AllowedId], [SortOrder]) VALUES (1175, 1177, 0)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1140, 1139, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1145, 1143, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1146, 1144, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1152, 1143, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1153, 1144, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1164, 1163, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1167, 1166, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1169, 1168, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1175, 1174, 1)
INSERT [dbo].[cmsDocumentType] ([contentTypeNodeId], [templateNodeId], [IsDefault]) VALUES (1177, 1176, 1)
SET IDENTITY_INSERT [dbo].[cmsMacro] ON 

INSERT [dbo].[cmsMacro] ([id], [uniqueId], [macroUseInEditor], [macroRefreshRate], [macroAlias], [macroName], [macroCacheByPage], [macroCachePersonalized], [macroDontRender], [macroSource], [macroType]) VALUES (1, N'c7e22520-3396-4b82-acfa-8201c4a2e9b2', 1, 0, N'latestBlogposts', N'Get Latest Blogposts', 0, 0, 0, N'~/Views/MacroPartials/LatestBlogposts.cshtml', 7)
INSERT [dbo].[cmsMacro] ([id], [uniqueId], [macroUseInEditor], [macroRefreshRate], [macroAlias], [macroName], [macroCacheByPage], [macroCachePersonalized], [macroDontRender], [macroSource], [macroType]) VALUES (2, N'9eca72e2-71e5-4ce1-90de-f63e2c154a3a', 1, 0, N'featuredProduct', N'Select Featured Products', 0, 0, 0, N'~/Views/MacroPartials/FeaturedProducts.cshtml', 7)
SET IDENTITY_INSERT [dbo].[cmsMacro] OFF
SET IDENTITY_INSERT [dbo].[cmsMacroProperty] ON 

INSERT [dbo].[cmsMacroProperty] ([id], [uniquePropertyId], [editorAlias], [macro], [macroPropertySortOrder], [macroPropertyAlias], [macroPropertyName]) VALUES (1, N'87ea65f0-e70a-4a81-aa74-f9d54b2e957e', N'Umbraco.Integer', 1, 0, N'numberOfPosts', N'How many posts should be shown')
INSERT [dbo].[cmsMacroProperty] ([id], [uniquePropertyId], [editorAlias], [macro], [macroPropertySortOrder], [macroPropertyAlias], [macroPropertyName]) VALUES (2, N'2d07e4d7-80ae-4182-a20a-24835ec7ffdd', N'Umbraco.ContentPicker', 1, 1, N'startNodeId', N'Where to get blog posts from')
INSERT [dbo].[cmsMacroProperty] ([id], [uniquePropertyId], [editorAlias], [macro], [macroPropertySortOrder], [macroPropertyAlias], [macroPropertyName]) VALUES (3, N'ad93b4ff-3d20-4053-94d2-21a84ec64363', N'Umbraco.ContentPicker', 2, 0, N'product', N'Choose Product')
SET IDENTITY_INSERT [dbo].[cmsMacroProperty] OFF
SET IDENTITY_INSERT [dbo].[cmsPropertyType] ON 

INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (6, 1043, 1032, 3, N'umbracoFile', N'Upload image', 0, 1, NULL, NULL, 0, N'00000006-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (7, -91, 1032, 3, N'umbracoWidth', N'Width', 0, 0, NULL, N'in pixels', 0, N'00000007-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (8, -91, 1032, 3, N'umbracoHeight', N'Height', 0, 0, NULL, N'in pixels', 0, N'00000008-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (9, -93, 1032, 3, N'umbracoBytes', N'Size', 0, 0, NULL, N'in bytes', 0, N'00000009-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (10, -92, 1032, 3, N'umbracoExtension', N'Type', 0, 0, NULL, NULL, 0, N'0000000a-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (24, -90, 1033, 4, N'umbracoFile', N'Upload file', 0, 1, NULL, NULL, 0, N'00000018-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (25, -92, 1033, 4, N'umbracoExtension', N'Type', 0, 0, NULL, NULL, 0, N'00000019-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (26, -93, 1033, 4, N'umbracoBytes', N'Size', 0, 0, NULL, N'in bytes', 0, N'0000001a-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (28, -89, 1044, 11, N'umbracoMemberComments', N'Comments', 0, 0, NULL, NULL, 0, N'0000001c-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (29, -91, 1044, 11, N'umbracoMemberFailedPasswordAttempts', N'Failed Password Attempts', 1, 0, NULL, NULL, 0, N'0000001d-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (30, -49, 1044, 11, N'umbracoMemberApproved', N'Is Approved', 2, 0, NULL, NULL, 0, N'0000001e-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (31, -49, 1044, 11, N'umbracoMemberLockedOut', N'Is Locked Out', 3, 0, NULL, NULL, 0, N'0000001f-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (32, -94, 1044, 11, N'umbracoMemberLastLockoutDate', N'Last Lockout Date', 4, 0, NULL, NULL, 0, N'00000020-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (33, -94, 1044, 11, N'umbracoMemberLastLogin', N'Last Login Date', 5, 0, NULL, NULL, 0, N'00000021-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (34, -94, 1044, 11, N'umbracoMemberLastPasswordChangeDate', N'Last Password Change Date', 6, 0, NULL, NULL, 0, N'00000022-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (35, -94, 1044, NULL, N'umbracoMemberPasswordRetrievalQuestion', N'Password Question', 7, 0, NULL, NULL, 0, N'00000023-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (36, -94, 1044, NULL, N'umbracoMemberPasswordRetrievalAnswer', N'Password Answer', 8, 0, NULL, NULL, 0, N'00000024-0000-0000-0000-000000000000')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (37, 1159, 1160, 12, N'introduction', N'Introduction', 0, 0, NULL, N'Enter your introduction text', 0, N'8258192d-a6b7-41f6-a0e0-250bd4b1e514')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (38, 1161, 1140, 13, N'helloBanner', N'Hello Banner', 1, 0, NULL, N'Enter your hello banner', 0, N'7624a273-bd9b-46f5-a4a5-96a48558431a')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (39, -88, 1140, 14, N'welcomeTitle', N'Welcome Title', 0, 1, N'', N'Enter welcome title', 0, N'0d8e06bc-3714-442e-8c3f-331800aa5e4f')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (40, -89, 1140, 14, N'welcomeSummary', N'Welcome Summary', 1, 1, NULL, N'enter welcome summary', 0, N'73448008-7a94-4b3a-af35-e1866e38a3a3')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (41, -88, 1140, 14, N'welcomeLink', N'Welcome Link', 2, 1, NULL, N'enter welcome link', 0, N'a2487039-23b3-4146-b6f3-efdfb55be6ba')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (42, -88, 1140, 14, N'hyperLinkText', N'Hyper Link Text', 3, 1, NULL, N'Enter label for hyper link', 0, N'8ffec739-114c-4827-bb10-34902ca6eb54')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (43, -88, 1140, 13, N'introHyperLink', N'Intro Hyper Link', 2, 1, NULL, NULL, 0, N'3b7c4642-2345-43e0-9fcb-9c7a16518fcf')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (44, -88, 1140, 13, N'introLinkLabel', N'Intro Link Label', 3, 1, NULL, NULL, 0, N'e80de1cb-b3ab-4443-a417-da0dfcfd4333')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (48, -49, 1164, 15, N'umbracoNaviHide', N'Umbraco Navi Hide', 0, 0, NULL, NULL, 0, N'f4429da8-4d4e-421e-a08e-1185c38160fd')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (49, -87, 1167, 16, N'newsBodyText', N'News Body Text', 0, 1, NULL, N'Enter the content for News', 0, N'fffb0215-0250-41fd-b310-5b9048840f21')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (50, -88, 1177, 17, N'userName', N'Name', 0, 1, NULL, N'Enter the user name', 0, N'8f17bedc-3063-42f6-ae65-e84bfe9d72b3')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (51, -88, 1177, 17, N'email', N'Email', 1, 0, N'', N'Enter the email', 0, N'f260d06a-c7e3-461a-9c0a-ddfb5d970998')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (52, -89, 1177, 17, N'message', N'Message', 2, 1, NULL, N'Enter your message', 0, N'2d5eff52-a6b6-4b36-87a9-90eb05d8e556')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (53, 1211, 1140, 18, N'languageFlag', N'Language flag', 0, 0, NULL, NULL, 0, N'03a756ab-67f1-4c3d-9e12-415d834fb60e')
INSERT [dbo].[cmsPropertyType] ([id], [dataTypeId], [contentTypeId], [propertyTypeGroupId], [Alias], [Name], [sortOrder], [mandatory], [validationRegExp], [Description], [variations], [UniqueID]) VALUES (54, 1048, 1169, 19, N'newsImage', N'News Image', 1, 0, NULL, NULL, 0, N'8091f72c-72f5-4edd-90e0-b65b4cfba412')
SET IDENTITY_INSERT [dbo].[cmsPropertyType] OFF
SET IDENTITY_INSERT [dbo].[cmsPropertyTypeGroup] ON 

INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (3, 1032, N'Image', 1, N'79ed4d07-254a-42cf-8fa9-ebe1c116a596')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (4, 1033, N'File', 1, N'50899f9c-023a-4466-b623-aba9049885fe')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (11, 1044, N'Membership', 1, N'0756729d-d665-46e3-b84a-37aceaa614f8')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (12, 1160, N'Content', 0, N'e2943169-4256-4dd1-8af6-444ec08603a5')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (13, 1140, N'Content', 0, N'4b4e1f9d-33b1-4eee-8c60-6ce7b54e4862')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (14, 1140, N'Welcome Section', 1, N'63adff91-f078-4104-a000-92fbd4cd4918')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (15, 1164, N'Navigation Page Setting', 0, N'11eabcb2-59a8-430e-9751-a4342fe641e3')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (16, 1167, N'News Content', 0, N'ce7e6607-c7c7-4dbe-9152-1e1cfcb8b616')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (17, 1177, N'Content', 0, N'00cc9e6b-877f-4257-a7ef-fef83938ad78')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (18, 1140, N'Language', 2, N'4370b10c-f99b-4426-91d1-25e2f777986e')
INSERT [dbo].[cmsPropertyTypeGroup] ([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) VALUES (19, 1169, N'News Content', 0, N'7721909c-5b98-4abd-8c27-ad13eb495690')
SET IDENTITY_INSERT [dbo].[cmsPropertyTypeGroup] OFF
SET IDENTITY_INSERT [dbo].[cmsTags] ON 

INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (1, N'default', NULL, N'dedication')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (2, N'default', NULL, N'tattoo')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (3, N'default', NULL, N'animals')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (4, N'default', NULL, N'bingo')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (5, N'default', NULL, N'sports')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (6, N'default', NULL, N'fashion')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (7, N'default', NULL, N'music')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (8, N'default', NULL, N'clothing')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (9, N'default', NULL, N'Denmark')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (10, N'default', NULL, N'mvp')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (11, N'default', NULL, N'United Kingdom')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (12, N'default', NULL, N'Netherlands')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (13, N'default', NULL, N'demo')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (14, N'default', NULL, N'starter kit')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (15, N'default', NULL, N'umbraco')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (16, N'default', NULL, N'cg16')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (17, N'default', NULL, N'codegarden')
INSERT [dbo].[cmsTags] ([id], [group], [languageId], [tag]) VALUES (18, N'default', NULL, N'great')
SET IDENTITY_INSERT [dbo].[cmsTags] OFF
SET IDENTITY_INSERT [dbo].[cmsTemplate] ON 

INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (11, 1138, N'webbaseTemplate')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (12, 1139, N'home')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (14, 1143, N'generic')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (15, 1144, N'elements')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (16, 1151, N'aboutUs')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (17, 1163, N'HidePageFromNavigation')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (18, 1166, N'News')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (19, 1168, N'NewsDetail')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (20, 1174, N'Contact')
INSERT [dbo].[cmsTemplate] ([pk], [nodeId], [alias]) VALUES (21, 1176, N'ContactContent')
SET IDENTITY_INSERT [dbo].[cmsTemplate] OFF
SET IDENTITY_INSERT [dbo].[umbracoAudit] ON 

INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (1, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-03T10:05:36.647' AS DateTime), -1, N'User "Administrator" ', N'umbraco/user/save', N'updating RawPasswordValue, LastPasswordChangeDate, UpdateDate, SecurityStamp')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (2, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-03T10:05:36.853' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating Email, Name, Username, UpdateDate, SecurityStamp')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (3, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-03T10:06:07.613' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (4, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-03T10:07:52.187' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (5, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-03T10:07:52.210' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (6, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-03T10:08:55.607' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating TourData, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (7, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-03T10:13:07.727' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating TourData, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (8, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-03T10:19:11.367' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating TourData, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (9, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-03T10:20:44.613' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating TourData, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (10, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-03T10:23:10.083' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating TourData, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (11, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T02:42:17.370' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (12, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T02:42:17.870' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (13, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T04:33:18.123' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (14, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T04:33:18.130' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (15, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T06:19:19.580' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating FailedPasswordAttempts, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (16, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T06:19:57.327' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating FailedPasswordAttempts, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (17, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T06:20:04.837' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating FailedPasswordAttempts, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (18, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T06:20:04.847' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (19, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T06:20:04.853' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (20, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T07:14:46.783' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (21, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T07:14:46.830' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (22, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-04T07:19:24.807' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (23, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T07:19:24.813' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (24, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T08:38:46.743' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (25, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T08:38:46.917' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (26, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T08:38:49.737' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/logout', N'logout success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (27, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T08:38:52.187' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (28, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T08:38:52.230' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (29, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T09:19:34.820' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (30, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T09:19:34.867' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (31, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T16:15:30.783' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (32, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-04T16:15:31.010' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (33, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-06T14:00:27.167' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (34, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-06T14:00:27.577' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (35, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T02:32:37.097' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (36, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T02:32:37.270' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (37, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T02:54:39.387' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (38, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T02:54:39.397' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (39, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T03:27:22.160' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (40, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T03:27:22.243' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (41, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-07T04:03:11.517' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (42, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T04:03:11.527' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (43, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T04:30:26.317' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (44, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T04:30:26.323' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (45, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T06:26:09.553' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (46, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T06:26:09.563' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (47, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T08:24:45.843' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (48, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T08:24:46.097' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (49, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T13:59:44.030' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (50, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T13:59:44.130' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (51, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T14:27:32.707' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (52, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T14:27:32.723' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (53, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-07T14:59:14.040' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (54, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T14:59:14.050' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (55, 0, N'User "SYTEM" ', N'127.0.0.1', CAST(N'2019-10-07T15:51:21.540' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (56, 0, N'User "SYTEM" ', N'127.0.0.1', CAST(N'2019-10-07T15:51:21.627' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (57, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T16:21:27.350' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (58, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-07T16:21:27.400' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (59, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T02:37:37.800' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (60, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T02:37:37.900' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (61, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T03:56:18.577' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (62, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T03:56:18.623' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (63, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T06:15:54.457' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (64, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T06:15:54.520' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (65, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T06:57:10.643' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (66, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T06:57:10.660' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (67, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T08:08:27.383' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (68, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T08:08:27.560' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (69, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T08:30:50.420' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (70, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T08:30:50.450' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (71, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T09:31:44.910' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (72, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T09:31:44.927' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (73, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T10:09:01.013' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (74, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-08T10:09:01.030' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (75, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T02:32:04.330' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (76, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T02:32:04.453' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (77, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-09T03:07:12.007' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (78, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T03:07:12.040' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (79, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-09T03:26:35.590' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (80, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T03:26:35.643' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (81, -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'::1', CAST(N'2019-10-09T03:46:13.270' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (82, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T03:46:13.273' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (83, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T04:43:39.153' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (84, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T04:43:39.197' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (85, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T06:36:31.057' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (86, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T06:36:31.097' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (87, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T07:35:35.870' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (88, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T07:35:35.960' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (89, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T08:36:43.580' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (90, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T08:36:43.783' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (91, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T09:39:43.150' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (92, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T09:39:43.193' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (93, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T10:33:52.013' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (94, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T10:33:52.023' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (95, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T13:09:17.720' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (96, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T13:09:17.807' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (97, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T13:57:45.250' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (98, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-09T13:57:45.367' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (99, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-10T08:07:19.320' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
GO
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (100, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-10T08:07:19.430' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (101, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-10T09:15:15.380' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (102, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-10T09:15:15.467' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (103, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-10T09:53:21.437' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (104, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-10T09:53:21.480' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (105, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-11T06:57:06.250' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (106, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-11T06:57:06.463' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (107, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-11T07:26:11.387' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/save', N'updating LastLoginDate, UpdateDate')
INSERT [dbo].[umbracoAudit] ([id], [performingUserId], [performingDetails], [performingIp], [eventDateUtc], [affectedUserId], [affectedDetails], [eventType], [eventDetails]) VALUES (108, 0, N'User "SYTEM" ', N'::1', CAST(N'2019-10-11T07:26:11.433' AS DateTime), -1, N'User "Le Hoang Quan" <q.foxy98@gmail.com>', N'umbraco/user/sign-in/login', N'login success')
SET IDENTITY_INSERT [dbo].[umbracoAudit] OFF
SET IDENTITY_INSERT [dbo].[umbracoCacheInstruction] ON 

INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (271, CAST(N'2019-10-09T13:09:19.057' AS DateTime), N'[{"RefreshType":3,"RefresherId":"e057af6d-2ee6-41f4-8045-3694010f0aa6","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":"[-1]","JsonIdCount":1,"JsonPayload":null}]', N'QUANFX//LM/W3SVC/2/ROOT [P6728/D2] C7FDA32318CC4012A7C85A90283B8BFF', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (272, CAST(N'2019-10-09T13:09:28.320' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1141,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P6728/D2] C7FDA32318CC4012A7C85A90283B8BFF', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (273, CAST(N'2019-10-09T13:09:30.550' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1189,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P6728/D2] C7FDA32318CC4012A7C85A90283B8BFF', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (274, CAST(N'2019-10-09T13:10:12.163' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1212,\"ChangeTypes\":2}]"},{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1212,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P6728/D2] C7FDA32318CC4012A7C85A90283B8BFF', 2)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (275, CAST(N'2019-10-09T13:57:46.167' AS DateTime), N'[{"RefreshType":3,"RefresherId":"e057af6d-2ee6-41f4-8045-3694010f0aa6","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":"[-1]","JsonIdCount":1,"JsonPayload":null}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (276, CAST(N'2019-10-09T13:58:29.940' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1141,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (277, CAST(N'2019-10-09T14:00:33.890' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1197,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (278, CAST(N'2019-10-09T14:01:10.467' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1197,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (279, CAST(N'2019-10-09T14:01:14.137' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1199,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (280, CAST(N'2019-10-09T14:01:17.143' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1200,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (281, CAST(N'2019-10-09T14:01:19.610' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1198,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (282, CAST(N'2019-10-09T14:03:28.657' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1198,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (283, CAST(N'2019-10-09T14:03:36.923' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1199,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (284, CAST(N'2019-10-09T14:04:03.063' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1189,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P16068/D2] CB53ACF70F114E0AB2BB1EACC616F1AA', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (285, CAST(N'2019-10-09T14:29:35.773' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1213,\"ChangeTypes\":2}]"},{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1213,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P12688/D2] 5B01F2E56515456DA11CE1EA38A2EC0E', 2)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (286, CAST(N'2019-10-09T14:30:39.483' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1214,\"ChangeTypes\":2}]"},{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1214,\"ChangeTypes\":4}]"}]', N'QUANFX//LM/W3SVC/2/ROOT [P12688/D2] 5B01F2E56515456DA11CE1EA38A2EC0E', 2)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (287, CAST(N'2019-10-10T08:07:20.123' AS DateTime), N'[{"RefreshType":3,"RefresherId":"e057af6d-2ee6-41f4-8045-3694010f0aa6","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":"[-1]","JsonIdCount":1,"JsonPayload":null}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P12428/D2] 47F4EC4B397F4695AB1A6199D9DFF316', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (288, CAST(N'2019-10-10T09:15:16.300' AS DateTime), N'[{"RefreshType":3,"RefresherId":"e057af6d-2ee6-41f4-8045-3694010f0aa6","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":"[-1]","JsonIdCount":1,"JsonPayload":null}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P4728/D2] 7F4F5279D5B74412986BD4D38EC101AE', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (289, CAST(N'2019-10-10T09:53:21.977' AS DateTime), N'[{"RefreshType":3,"RefresherId":"e057af6d-2ee6-41f4-8045-3694010f0aa6","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":"[-1]","JsonIdCount":1,"JsonPayload":null}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P4728/D3] 736185449CF14919A0C9966F36032373', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (290, CAST(N'2019-10-11T06:57:07.293' AS DateTime), N'[{"RefreshType":3,"RefresherId":"e057af6d-2ee6-41f4-8045-3694010f0aa6","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":"[-1]","JsonIdCount":1,"JsonPayload":null}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P8948/D2] D30F27C646244D19B3D28729B2E20A0D', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (291, CAST(N'2019-10-11T06:58:30.070' AS DateTime), N'[{"RefreshType":4,"RefresherId":"6902e22c-9c10-483c-91f3-66b7cae9e2f5","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"ItemType\":\"IContentType\",\"Id\":1169,\"ChangeTypes\":4}]"}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P8948/D2] D30F27C646244D19B3D28729B2E20A0D', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (292, CAST(N'2019-10-11T06:59:13.703' AS DateTime), N'[{"RefreshType":4,"RefresherId":"b29286dd-2d40-4ddb-b325-681226589fec","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1215,\"ChangeTypes\":2}]"}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P8948/D2] D30F27C646244D19B3D28729B2E20A0D', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (293, CAST(N'2019-10-11T06:59:25.130' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1216,\"ChangeTypes\":2}]"}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P8948/D2] D30F27C646244D19B3D28729B2E20A0D', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (294, CAST(N'2019-10-11T07:26:11.440' AS DateTime), N'[{"RefreshType":3,"RefresherId":"e057af6d-2ee6-41f4-8045-3694010f0aa6","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":"[-1]","JsonIdCount":1,"JsonPayload":null}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P8948/D2] D30F27C646244D19B3D28729B2E20A0D', 1)
INSERT [dbo].[umbracoCacheInstruction] ([id], [utcStamp], [jsonInstruction], [originated], [instructionCount]) VALUES (295, CAST(N'2019-10-11T07:43:41.667' AS DateTime), N'[{"RefreshType":4,"RefresherId":"900a4fbe-df3c-41e6-bb77-be896cd158ea","GuidId":"00000000-0000-0000-0000-000000000000","IntId":0,"JsonIds":null,"JsonIdCount":1,"JsonPayload":"[{\"Id\":1216,\"ChangeTypes\":4}]"}]', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT [P8948/D2] D30F27C646244D19B3D28729B2E20A0D', 1)
SET IDENTITY_INSERT [dbo].[umbracoCacheInstruction] OFF
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1119, 1031)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1120, 1031)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1121, 1031)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1122, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1123, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1124, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1125, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1126, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1127, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1128, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1129, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1130, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1131, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1132, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1133, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1134, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1135, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1137, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1141, 1140)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1147, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1148, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1149, 1146)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1150, 1140)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1154, 1152)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1155, 1153)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1156, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1157, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1162, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1170, 1167)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1171, 1169)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1172, 1169)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1173, 1169)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1178, 1175)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1179, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1180, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1181, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1182, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1183, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1184, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1185, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1186, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1187, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1188, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1189, 1140)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1190, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1191, 1146)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1192, 1152)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1193, 1153)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1194, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1195, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1196, 1145)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1197, 1167)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1198, 1169)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1199, 1169)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1200, 1169)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1201, 1175)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1202, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1203, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1204, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1205, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1206, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1207, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1208, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1209, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1210, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1212, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1213, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1214, 1177)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1215, 1032)
INSERT [dbo].[umbracoContent] ([nodeId], [contentTypeId]) VALUES (1216, 1169)
SET IDENTITY_INSERT [dbo].[umbracoContentVersion] ON 

INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (25, 1119, CAST(N'2019-10-04T11:43:09.417' AS DateTime), NULL, 1, N'Design')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (26, 1120, CAST(N'2019-10-04T11:42:56.210' AS DateTime), NULL, 1, N'People')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (27, 1121, CAST(N'2019-10-04T11:43:02.457' AS DateTime), NULL, 1, N'Products')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (28, 1122, CAST(N'2019-10-04T11:43:09.433' AS DateTime), NULL, 1, N'Umbraco Campari Meeting Room')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (29, 1123, CAST(N'2019-10-04T11:43:02.470' AS DateTime), NULL, 1, N'Biker Jacket')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (30, 1124, CAST(N'2019-10-04T11:43:02.473' AS DateTime), NULL, 1, N'Tattoo')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (31, 1125, CAST(N'2019-10-04T11:43:02.477' AS DateTime), NULL, 1, N'Unicorn')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (32, 1126, CAST(N'2019-10-04T11:43:02.480' AS DateTime), NULL, 1, N'Ping Pong Ball')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (33, 1127, CAST(N'2019-10-04T11:43:02.487' AS DateTime), NULL, 1, N'Bowling Ball')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (34, 1128, CAST(N'2019-10-04T11:43:02.490' AS DateTime), NULL, 1, N'Jumpsuit')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (35, 1129, CAST(N'2019-10-04T11:43:02.493' AS DateTime), NULL, 1, N'Banjo')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (36, 1130, CAST(N'2019-10-04T11:43:02.497' AS DateTime), NULL, 1, N'Knitted West')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (37, 1131, CAST(N'2019-10-04T11:42:56.283' AS DateTime), NULL, 1, N'Jan Skovgaard')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (38, 1132, CAST(N'2019-10-04T11:42:56.290' AS DateTime), NULL, 1, N'Matt Brailsford')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (39, 1133, CAST(N'2019-10-04T11:42:56.293' AS DateTime), NULL, 1, N'Lee Kelleher')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (40, 1134, CAST(N'2019-10-04T11:42:56.297' AS DateTime), NULL, 1, N'Jeavon Leopold')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (41, 1135, CAST(N'2019-10-04T11:42:56.300' AS DateTime), NULL, 1, N'Jeroen Breuer')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (69, 1137, CAST(N'2019-10-04T11:43:09.437' AS DateTime), NULL, 1, N'2Bd58947 57Fb 4Dbd B202 F5884830bcfb 4C1a0bcd E53c 4Ba9 96A6 F8cbe1935013 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (70, 1141, CAST(N'2019-10-04T11:50:05.297' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (71, 1141, CAST(N'2019-10-04T15:05:31.303' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (72, 1147, CAST(N'2019-10-04T14:35:58.843' AS DateTime), -1, 1, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (73, 1148, CAST(N'2019-10-04T15:05:15.603' AS DateTime), -1, 0, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (74, 1149, CAST(N'2019-10-04T15:05:19.683' AS DateTime), -1, 0, N'Elements')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (75, 1148, CAST(N'2019-10-07T23:21:41.207' AS DateTime), -1, 0, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (76, 1149, CAST(N'2019-10-04T15:05:19.683' AS DateTime), -1, 1, N'Elements')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (77, 1141, CAST(N'2019-10-07T11:41:34.563' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (78, 1150, CAST(N'2019-10-07T10:28:16.107' AS DateTime), -1, 0, N'TestForNewContent')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (79, 1150, CAST(N'2019-10-07T10:29:12.220' AS DateTime), -1, 1, N'TestForNewContent')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (80, 1154, CAST(N'2019-10-07T10:36:37.810' AS DateTime), -1, 0, N'About us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (81, 1154, CAST(N'2019-10-08T09:53:39.067' AS DateTime), -1, 0, N'About us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (82, 1155, CAST(N'2019-10-07T10:37:06.953' AS DateTime), -1, 0, N'Company Overview')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (83, 1155, CAST(N'2019-10-07T22:23:32.183' AS DateTime), -1, 0, N'Company Overview')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (84, 1156, CAST(N'2019-10-07T11:04:45.690' AS DateTime), -1, 0, N'Privacy')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (85, 1156, CAST(N'2019-10-07T22:23:37.250' AS DateTime), -1, 0, N'Privacy')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (86, 1157, CAST(N'2019-10-07T11:05:23.800' AS DateTime), -1, 0, N'Disclamer')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (87, 1157, CAST(N'2019-10-07T11:05:23.800' AS DateTime), -1, 1, N'Disclamer')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (88, 1141, CAST(N'2019-10-07T11:48:46.183' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (89, 1141, CAST(N'2019-10-07T13:37:04.583' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (90, 1141, CAST(N'2019-10-07T13:57:25.447' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (91, 1141, CAST(N'2019-10-07T14:08:22.407' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (92, 1141, CAST(N'2019-10-07T14:08:42.683' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (93, 1141, CAST(N'2019-10-07T14:09:46.023' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (94, 1141, CAST(N'2019-10-07T14:10:49.003' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (95, 1141, CAST(N'2019-10-07T15:34:36.367' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (96, 1141, CAST(N'2019-10-07T15:44:42.553' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (97, 1141, CAST(N'2019-10-07T21:00:15.810' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (98, 1141, CAST(N'2019-10-07T22:00:59.260' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (99, 1162, CAST(N'2019-10-07T22:00:29.827' AS DateTime), -1, 0, N'Welcome')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (100, 1162, CAST(N'2019-10-08T09:53:23.433' AS DateTime), -1, 0, N'Welcome')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (101, 1141, CAST(N'2019-10-07T22:01:50.963' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (102, 1141, CAST(N'2019-10-07T22:02:29.180' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (103, 1141, CAST(N'2019-10-07T22:04:24.933' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (104, 1141, CAST(N'2019-10-07T22:09:56.120' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (105, 1141, CAST(N'2019-10-07T22:12:43.113' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (106, 1141, CAST(N'2019-10-08T09:38:03.020' AS DateTime), -1, 0, N'Home')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (107, 1155, CAST(N'2019-10-07T23:25:29.783' AS DateTime), -1, 0, N'Company Overview')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (108, 1156, CAST(N'2019-10-07T22:23:37.250' AS DateTime), -1, 1, N'Privacy')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (109, 1148, CAST(N'2019-10-07T23:21:51.667' AS DateTime), -1, 0, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (110, 1148, CAST(N'2019-10-07T23:26:09.497' AS DateTime), -1, 0, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (111, 1155, CAST(N'2019-10-07T23:25:29.783' AS DateTime), -1, 1, N'Company Overview')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (112, 1148, CAST(N'2019-10-07T23:27:20.633' AS DateTime), -1, 0, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (113, 1148, CAST(N'2019-10-08T09:53:56.027' AS DateTime), -1, 0, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (114, 1141, CAST(N'2019-10-09T15:47:27.707' AS DateTime), -1, 0, N'EN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (115, 1170, CAST(N'2019-10-08T09:51:13.083' AS DateTime), -1, 0, N'News')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (116, 1170, CAST(N'2019-10-08T09:51:13.083' AS DateTime), -1, 1, N'News')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (117, 1171, CAST(N'2019-10-08T09:52:57.620' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (118, 1171, CAST(N'2019-10-08T09:53:07.530' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (119, 1171, CAST(N'2019-10-08T09:54:29.540' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (120, 1162, CAST(N'2019-10-08T09:53:23.433' AS DateTime), -1, 1, N'Welcome')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (121, 1154, CAST(N'2019-10-08T09:53:39.067' AS DateTime), -1, 1, N'About us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (122, 1148, CAST(N'2019-10-08T09:53:56.027' AS DateTime), -1, 1, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (123, 1172, CAST(N'2019-10-08T09:54:20.113' AS DateTime), -1, 0, N'News 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (124, 1172, CAST(N'2019-10-08T11:40:39.340' AS DateTime), -1, 0, N'News 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (125, 1171, CAST(N'2019-10-08T11:40:36.333' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (126, 1173, CAST(N'2019-10-08T09:55:37.663' AS DateTime), -1, 0, N'News 3')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (127, 1173, CAST(N'2019-10-08T11:40:43.330' AS DateTime), -1, 0, N'News 3')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (128, 1171, CAST(N'2019-10-08T11:41:03.980' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (129, 1172, CAST(N'2019-10-08T13:25:31.990' AS DateTime), -1, 0, N'News 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (130, 1173, CAST(N'2019-10-08T13:25:49.590' AS DateTime), -1, 0, N'News 3')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (131, 1171, CAST(N'2019-10-08T11:42:02.007' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (132, 1171, CAST(N'2019-10-08T13:25:14.940' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (133, 1171, CAST(N'2019-10-08T13:33:56.777' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (134, 1172, CAST(N'2019-10-08T13:33:53.553' AS DateTime), -1, 0, N'News 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (135, 1173, CAST(N'2019-10-08T13:33:51.140' AS DateTime), -1, 0, N'News 3')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (136, 1173, CAST(N'2019-10-08T13:33:51.140' AS DateTime), -1, 1, N'News 3')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (137, 1172, CAST(N'2019-10-08T13:59:00.597' AS DateTime), -1, 0, N'News 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (138, 1171, CAST(N'2019-10-08T13:33:56.777' AS DateTime), -1, 1, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (139, 1172, CAST(N'2019-10-08T13:59:00.597' AS DateTime), -1, 1, N'News 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (140, 1178, CAST(N'2019-10-09T09:36:03.423' AS DateTime), -1, 0, N'Contact Us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (141, 1178, CAST(N'2019-10-09T09:53:04.237' AS DateTime), -1, 0, N'Contact Us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (142, 1179, CAST(N'2019-10-09T09:37:41.443' AS DateTime), -1, 0, N'quanle-2907')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (143, 1179, CAST(N'2019-10-09T09:41:13.843' AS DateTime), -1, 0, N'quanle-2907')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (144, 1179, CAST(N'2019-10-09T09:53:09.820' AS DateTime), -1, 0, N'quanle-2907')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (145, 1178, CAST(N'2019-10-09T09:53:04.237' AS DateTime), -1, 1, N'Contact Us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (146, 1179, CAST(N'2019-10-09T11:58:38.000' AS DateTime), -1, 1, N'quanle-2907')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (147, 1180, CAST(N'2019-10-09T11:59:08.210' AS DateTime), -1, 0, N'quan--10/9/2019 11:59:07 AM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (148, 1180, CAST(N'2019-10-09T13:43:25.967' AS DateTime), -1, 0, N'quan--10/9/2019 11:59:07 AM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (149, 1181, CAST(N'2019-10-09T12:00:39.863' AS DateTime), -1, 0, N'quanfx--10/9/2019 12:00:39 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (150, 1181, CAST(N'2019-10-09T13:43:30.003' AS DateTime), -1, 0, N'quanfx--10/9/2019 12:00:39 PM')
GO
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (151, 1182, CAST(N'2019-10-09T13:43:09.767' AS DateTime), -1, 0, N'adminne--10/9/2019 1:43:08 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (152, 1182, CAST(N'2019-10-09T13:43:09.767' AS DateTime), -1, 1, N'adminne--10/9/2019 1:43:08 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (153, 1180, CAST(N'2019-10-09T13:43:25.967' AS DateTime), -1, 1, N'quan--10/9/2019 11:59:07 AM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (154, 1181, CAST(N'2019-10-09T13:43:30.003' AS DateTime), -1, 1, N'quanfx--10/9/2019 12:00:39 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (155, 1183, CAST(N'2019-10-09T13:51:19.953' AS DateTime), -1, 0, N'Nhi--10/9/2019 1:51:19 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (156, 1183, CAST(N'2019-10-09T13:51:19.953' AS DateTime), -1, 1, N'Nhi--10/9/2019 1:51:19 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (157, 1184, CAST(N'2019-10-09T14:39:07.790' AS DateTime), -1, 0, N'Lux A2.0--10/9/2019 2:39:06 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (158, 1184, CAST(N'2019-10-09T14:39:07.790' AS DateTime), -1, 1, N'Lux A2.0--10/9/2019 2:39:06 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (159, 1185, CAST(N'2019-10-09T14:49:32.967' AS DateTime), -1, 0, N'Lux A2.0--10/9/2019 2:49:32 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (160, 1185, CAST(N'2019-10-09T14:49:32.967' AS DateTime), -1, 1, N'Lux A2.0--10/9/2019 2:49:32 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (161, 1186, CAST(N'2019-10-09T14:50:05.173' AS DateTime), -1, 0, N'Lux A2.0--10/9/2019 2:50:04 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (162, 1186, CAST(N'2019-10-09T14:50:05.173' AS DateTime), -1, 1, N'Lux A2.0--10/9/2019 2:50:04 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (163, 1187, CAST(N'2019-10-09T14:50:47.080' AS DateTime), -1, 0, N'quanfx--10/9/2019 2:50:46 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (164, 1187, CAST(N'2019-10-09T14:50:47.080' AS DateTime), -1, 1, N'quanfx--10/9/2019 2:50:46 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (165, 1188, CAST(N'2019-10-09T15:33:14.077' AS DateTime), -1, 0, N'Product--10/9/2019 3:33:13 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (166, 1188, CAST(N'2019-10-09T15:33:14.077' AS DateTime), -1, 1, N'Product--10/9/2019 3:33:13 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (167, 1189, CAST(N'2019-10-09T15:47:33.480' AS DateTime), -1, 0, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (168, 1190, CAST(N'2019-10-09T15:50:37.290' AS DateTime), -1, 0, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (169, 1191, CAST(N'2019-10-09T15:50:43.100' AS DateTime), -1, 0, N'Elements')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (170, 1192, CAST(N'2019-10-09T15:50:45.463' AS DateTime), -1, 0, N'About us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (171, 1193, CAST(N'2019-10-09T15:45:59.217' AS DateTime), -1, 1, N'Company Overview')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (172, 1194, CAST(N'2019-10-09T15:45:59.220' AS DateTime), -1, 1, N'Privacy')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (173, 1195, CAST(N'2019-10-09T15:45:59.223' AS DateTime), -1, 1, N'Disclamer')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (174, 1196, CAST(N'2019-10-09T15:50:47.453' AS DateTime), -1, 0, N'Welcome')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (175, 1197, CAST(N'2019-10-09T15:50:49.180' AS DateTime), -1, 0, N'News')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (176, 1198, CAST(N'2019-10-09T21:01:19.447' AS DateTime), -1, 0, N'News 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (177, 1199, CAST(N'2019-10-09T21:01:13.883' AS DateTime), -1, 0, N'News 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (178, 1200, CAST(N'2019-10-09T21:01:16.997' AS DateTime), -1, 0, N'News 3')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (179, 1201, CAST(N'2019-10-09T15:50:52.083' AS DateTime), -1, 0, N'Contact Us')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (180, 1202, CAST(N'2019-10-09T15:45:59.247' AS DateTime), -1, 1, N'quan--10/9/2019 11:59:07 AM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (181, 1203, CAST(N'2019-10-09T15:45:59.250' AS DateTime), -1, 1, N'quanfx--10/9/2019 12:00:39 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (182, 1204, CAST(N'2019-10-09T15:45:59.253' AS DateTime), -1, 1, N'adminne--10/9/2019 1:43:08 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (183, 1205, CAST(N'2019-10-09T15:45:59.257' AS DateTime), -1, 1, N'Nhi--10/9/2019 1:51:19 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (184, 1206, CAST(N'2019-10-09T15:45:59.260' AS DateTime), -1, 1, N'Lux A2.0--10/9/2019 2:39:06 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (185, 1207, CAST(N'2019-10-09T15:45:59.263' AS DateTime), -1, 1, N'Lux A2.0--10/9/2019 2:49:32 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (186, 1208, CAST(N'2019-10-09T15:45:59.267' AS DateTime), -1, 1, N'Lux A2.0--10/9/2019 2:50:04 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (187, 1209, CAST(N'2019-10-09T15:45:59.273' AS DateTime), -1, 1, N'quanfx--10/9/2019 2:50:46 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (188, 1210, CAST(N'2019-10-09T15:45:59.277' AS DateTime), -1, 1, N'Product--10/9/2019 3:33:13 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (189, 1141, CAST(N'2019-10-09T16:47:01.430' AS DateTime), -1, 0, N'EN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (190, 1189, CAST(N'2019-10-09T15:49:21.180' AS DateTime), -1, 0, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (191, 1189, CAST(N'2019-10-09T15:49:45.483' AS DateTime), -1, 0, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (192, 1189, CAST(N'2019-10-09T16:47:13.837' AS DateTime), -1, 0, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (193, 1190, CAST(N'2019-10-09T15:50:37.290' AS DateTime), -1, 1, N'Generic')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (194, 1191, CAST(N'2019-10-09T15:50:43.100' AS DateTime), -1, 1, N'Elements')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (195, 1192, CAST(N'2019-10-09T15:55:26.207' AS DateTime), -1, 0, N'Giới thiệu về công ty')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (196, 1196, CAST(N'2019-10-09T15:55:02.663' AS DateTime), -1, 0, N'Chào mừng')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (197, 1197, CAST(N'2019-10-09T15:54:52.727' AS DateTime), -1, 0, N'Tin tức')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (198, 1201, CAST(N'2019-10-09T15:54:45.737' AS DateTime), -1, 0, N'Liên hệ')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (199, 1201, CAST(N'2019-10-09T15:54:45.737' AS DateTime), -1, 1, N'Liên hệ')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (200, 1197, CAST(N'2019-10-09T21:00:33.670' AS DateTime), -1, 0, N'Tin tức')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (201, 1196, CAST(N'2019-10-09T15:55:02.663' AS DateTime), -1, 1, N'Chào mừng')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (202, 1192, CAST(N'2019-10-09T15:55:26.207' AS DateTime), -1, 1, N'Giới thiệu về công ty')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (203, 1141, CAST(N'2019-10-09T20:09:27.237' AS DateTime), -1, 0, N'EN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (204, 1189, CAST(N'2019-10-09T16:47:21.243' AS DateTime), -1, 0, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (205, 1189, CAST(N'2019-10-09T20:09:30.357' AS DateTime), -1, 0, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (206, 1141, CAST(N'2019-10-09T20:58:29.363' AS DateTime), -1, 0, N'EN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (207, 1189, CAST(N'2019-10-09T21:04:02.870' AS DateTime), -1, 0, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (208, 1212, CAST(N'2019-10-09T20:10:06.237' AS DateTime), -1, 0, N'quanfx--10/9/2019 8:10:05 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (209, 1212, CAST(N'2019-10-09T20:10:06.237' AS DateTime), -1, 1, N'quanfx--10/9/2019 8:10:05 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (210, 1141, CAST(N'2019-10-09T20:58:29.363' AS DateTime), -1, 1, N'EN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (211, 1197, CAST(N'2019-10-09T21:01:10.307' AS DateTime), -1, 0, N'Tin tức')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (212, 1197, CAST(N'2019-10-09T21:01:10.307' AS DateTime), -1, 1, N'Tin tức')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (213, 1199, CAST(N'2019-10-09T21:03:36.750' AS DateTime), -1, 0, N'Tin tuc 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (214, 1200, CAST(N'2019-10-09T21:01:16.997' AS DateTime), -1, 1, N'News 3')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (215, 1198, CAST(N'2019-10-09T21:03:28.390' AS DateTime), -1, 0, N'Tin tuc 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (216, 1198, CAST(N'2019-10-09T21:03:28.390' AS DateTime), -1, 1, N'Tin tuc 1')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (217, 1199, CAST(N'2019-10-09T21:03:36.750' AS DateTime), -1, 1, N'Tin tuc 2')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (218, 1189, CAST(N'2019-10-09T21:04:02.870' AS DateTime), -1, 1, N'VN')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (219, 1213, CAST(N'2019-10-09T21:29:31.373' AS DateTime), -1, 0, N'quanfx--10/9/2019 9:29:27 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (220, 1213, CAST(N'2019-10-09T21:29:31.373' AS DateTime), -1, 1, N'quanfx--10/9/2019 9:29:27 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (221, 1214, CAST(N'2019-10-09T21:30:37.667' AS DateTime), -1, 0, N'admin--10/9/2019 9:30:37 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (222, 1214, CAST(N'2019-10-09T21:30:37.667' AS DateTime), -1, 1, N'admin--10/9/2019 9:30:37 PM')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (223, 1215, CAST(N'2019-10-11T13:59:13.520' AS DateTime), NULL, 1, N'Vn')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (224, 1216, CAST(N'2019-10-11T13:59:24.763' AS DateTime), -1, 0, N'New4')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (225, 1216, CAST(N'2019-10-11T14:43:41.403' AS DateTime), -1, 0, N'New4')
INSERT [dbo].[umbracoContentVersion] ([id], [nodeId], [versionDate], [userId], [current], [text]) VALUES (226, 1216, CAST(N'2019-10-11T14:43:41.403' AS DateTime), -1, 1, N'New4')
SET IDENTITY_INSERT [dbo].[umbracoContentVersion] OFF
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-99, N'Umbraco.Label', N'Decimal', N'{"umbracoDataValueType":"DECIMAL"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-98, N'Umbraco.Label', N'Date', N'{"umbracoDataValueType":"TIME"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-97, N'Umbraco.ListView', N'Nvarchar', N'{"pageSize":10, "orderBy":"username", "orderDirection":"asc", "includeProperties":[{"alias":"username","isSystem":1},{"alias":"email","isSystem":1},{"alias":"updateDate","header":"Last edited","isSystem":1}]}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-96, N'Umbraco.ListView', N'Nvarchar', N'{"pageSize":100, "orderBy":"updateDate", "orderDirection":"desc", "layouts":[{"name": "Grid","path": "views/propertyeditors/listview/layouts/grid/grid.html", "icon": "icon-thumbnails-small", "isSystem": 1, "selected": true},{"name": "List","path": "views/propertyeditors/listview/layouts/list/list.html","icon": "icon-list", "isSystem": 1,"selected": true}], "includeProperties":[{"alias":"updateDate","header":"Last edited","isSystem":1},{"alias":"owner","header":"Updated by","isSystem":1}]}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-95, N'Umbraco.ListView', N'Nvarchar', N'{"pageSize":100, "orderBy":"updateDate", "orderDirection":"desc", "layouts":[{"name": "Grid","path": "views/propertyeditors/listview/layouts/grid/grid.html", "icon": "icon-thumbnails-small", "isSystem": 1, "selected": true},{"name": "List","path": "views/propertyeditors/listview/layouts/list/list.html","icon": "icon-list", "isSystem": 1,"selected": true}], "includeProperties":[{"alias":"updateDate","header":"Last edited","isSystem":1},{"alias":"owner","header":"Updated by","isSystem":1}]}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-94, N'Umbraco.Label', N'Date', N'{"umbracoDataValueType":"DATETIME"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-93, N'Umbraco.Label', N'Nvarchar', N'{"umbracoDataValueType":"BIGINT"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-92, N'Umbraco.Label', N'Nvarchar', N'{"umbracoDataValueType":"STRING"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-91, N'Umbraco.Label', N'Integer', N'{"umbracoDataValueType":"INT"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-90, N'Umbraco.UploadField', N'Nvarchar', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-89, N'Umbraco.TextArea', N'Ntext', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-88, N'Umbraco.TextBox', N'Nvarchar', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-87, N'Umbraco.TinyMCE', N'Ntext', N'{"value":",code,undo,redo,cut,copy,mcepasteword,stylepicker,bold,italic,bullist,numlist,outdent,indent,mcelink,unlink,mceinsertanchor,mceimage,umbracomacro,mceinserttable,umbracoembed,mcecharmap,|1|1,2,3,|0|500,400|1049,|true|"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-51, N'Umbraco.Integer', N'Integer', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-49, N'Umbraco.TrueFalse', N'Integer', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-43, N'Umbraco.CheckBoxList', N'Nvarchar', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-42, N'Umbraco.DropDown.Flexible', N'Nvarchar', N'{"multiple":true}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-41, N'Umbraco.DateTime', N'Date', N'{"format":"YYYY-MM-DD"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-40, N'Umbraco.RadioButtonList', N'Nvarchar', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-39, N'Umbraco.DropDown.Flexible', N'Nvarchar', N'{"multiple":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-37, N'Umbraco.ColorPicker', N'Nvarchar', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (-36, N'Umbraco.DateTime', N'Date', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1041, N'Umbraco.Tags', N'Ntext', N'{"group":"default", "storageType":"Json"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1043, N'Umbraco.ImageCropper', N'Ntext', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1046, N'Umbraco.ContentPicker', N'Nvarchar', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1047, N'Umbraco.MemberPicker', N'Nvarchar', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1048, N'Umbraco.MediaPicker', N'Ntext', N'{"multiPicker":false,"onlyImages":false,"disableFolderSelect":false,"startNodeId":null,"ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1049, N'Umbraco.MediaPicker', N'Ntext', N'{"multiPicker":1}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1050, N'Umbraco.MultiUrlPicker', N'Ntext', NULL)
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1051, N'Umbraco.ContentPicker', N'Nvarchar', N'{"showOpenButton":false,"startNodeId":"umb://document/ca4249ed2b234337b52263cabe5587d1","ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1052, N'Umbraco.RadioButtonList', N'Nvarchar', N'{"items":[{"id":1,"value":"water"},{"id":2,"value":"earth"},{"id":3,"value":"sun"}]}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1053, N'Umbraco.Grid', N'Ntext', N'{"items":{"styles":[],"config":[],"columns":12,"templates":[{"name":"1 column layout","sections":[{"grid":12}]}],"layouts":[{"name":"Full Width","areas":[{"grid":12,"allowAll":false,"allowed":["media","macro","embed","headline"]}]},{"name":"Half and half","areas":[{"grid":6,"allowAll":false,"allowed":["rte","media","macro","embed","headline","quote"]},{"grid":6,"allowAll":false,"allowed":["quote","headline","embed","macro","media","rte"]}]},{"name":"Testimonials","areas":[{"grid":4,"allowAll":false,"allowed":[]},{"grid":4,"allowAll":false,"allowed":[]},{"grid":4,"allowAll":false,"allowed":[]}]}]},"rte":{"toolbar":["code","bold","italic","styleselect","alignleft","aligncenter","alignright","bullist","numlist","outdent","indent","link","image","ace"],"stylesheets":[],"maxImageSize":500,"mode":"classic","dimensions":{"height":500}},"ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1054, N'Umbraco.RadioButtonList', N'Nvarchar', N'{"items":[{"id":1,"value":"standard"},{"id":2,"value":"serif"},{"id":3,"value":"mono"}]}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1055, N'Umbraco.MediaPicker', N'Ntext', N'{"multiPicker":false,"onlyImages":true,"disableFolderSelect":true,"startNodeId":"umb://document/b61e08cd366445759a7295848cb803ac","ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1056, N'Umbraco.Slider', N'Nvarchar', N'{"enableRange":false,"initVal1":0.0,"initVal2":0.0,"minVal":0.0,"maxVal":0.0,"step":0.0}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1057, N'Umbraco.Tags', N'Nvarchar', N'{"group":"default","storageType":1,"Delimiter":"\u0000"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1058, N'UmbracoForms.FormPicker', N'Nvarchar', N'{"allowedForms":[]}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1059, N'Umbraco.TinyMCE', N'Ntext', N'{"editor":{"toolbar":["undo","redo","styleselect","bold","italic","alignleft","aligncenter","alignright","bullist","numlist","outdent","indent","link"],"stylesheets":[],"maxImageSize":500,"mode":"classic","dimensions":{"height":500}},"hideLabel":false,"ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1060, N'Umbraco.Grid', N'Ntext', N'{"items":{"styles":[],"config":[],"columns":12,"templates":[{"name":"1 column layout","sections":[{"grid":12,"allowAll":true}]}],"layouts":[{"label":"Article","name":"Article","areas":[{"grid":4},{"grid":8}]},{"name":"Three Columns","areas":[{"grid":4,"allowAll":true},{"grid":4,"allowAll":true},{"grid":4,"allowAll":true}]},{"name":"FullWidth","areas":[{"grid":12,"allowAll":true}]}]},"rte":{"toolbar":["code","bold","italic","styleselect","alignleft","aligncenter","alignright","bullist","numlist","outdent","indent","link","image","ace","redo","undo","strikethrough","table"],"stylesheets":[],"maxImageSize":500,"mode":"classic","dimensions":{"height":500}},"ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1061, N'Umbraco.MultiNodeTreePicker', N'Ntext', N'{"startNode":null,"filter":"person","minNumber":0,"maxNumber":0,"showOpenButton":false,"ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1062, N'Umbraco.Tags', N'Nvarchar', N'{"group":"default","storageType":0,"Delimiter":"\u0000"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1063, N'Umbraco.MediaPicker', N'Ntext', N'{"multiPicker":false,"onlyImages":true,"disableFolderSelect":true,"startNodeId":"umb://media/1fd2ecaff3714c009306867fa4585e7a","ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1064, N'Umbraco.Tags', N'Nvarchar', N'{"group":"default","storageType":0,"Delimiter":"\u0000"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1065, N'Umbraco.Tags', N'Nvarchar', N'{"group":"default","storageType":1,"Delimiter":"\u0000"}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1066, N'Umbraco.NestedContent', N'Ntext', N'{"contentTypes":[{"ncAlias":"feature","ncTabAlias":"Feature","nameTemplate":"{{featureName}}"}],"minItems":0,"maxItems":0,"confirmDeletes":true,"showIcons":true,"hideLabel":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1067, N'Umbraco.MediaPicker', N'Ntext', N'{"multiPicker":false,"onlyImages":true,"disableFolderSelect":true,"startNodeId":"umb://media/6d5bf746cb8245c5bd15dd3798209b87","ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1068, N'Umbraco.Decimal', N'Decimal', N'{}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1069, N'Umbraco.DropDown.Flexible', N'Nvarchar', N'{"multiple":false,"items":[{"id":1,"value":"€"},{"id":2,"value":"£"},{"id":3,"value":"$"},{"id":4,"value":"DKK"}]}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1070, N'Umbraco.MultiNodeTreePicker', N'Ntext', N'{"startNode":null,"filter":"product","minNumber":0,"maxNumber":9,"showOpenButton":false,"ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1071, N'Our.Umbraco.OsmMaps', N'Nvarchar', N'{}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1159, N'Umbraco.TinyMCE', N'Ntext', N'{"editor":{"toolbar":["ace","styleselect","bold","italic","alignleft","aligncenter","alignright","bullist","numlist","outdent","indent","link","umbmediapicker","umbmacro","umbembeddialog"],"stylesheets":[],"maxImageSize":500,"mode":"classic"},"hideLabel":false,"ignoreUserStartNodes":false}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1161, N'Umbraco.UploadField', N'Nvarchar', N'{}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1165, N'Umbraco.TextBox', N'Nvarchar', N'{}')
INSERT [dbo].[umbracoDataType] ([nodeId], [propertyEditorAlias], [dbType], [config]) VALUES (1211, N'Umbraco.UploadField', N'Nvarchar', N'{}')
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1141, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1147, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1148, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1149, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1150, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1154, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1155, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1156, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1157, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1162, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1170, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1171, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1172, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1173, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1178, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1179, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1180, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1181, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1182, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1183, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1184, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1185, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1186, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1187, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1188, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1189, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1190, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1191, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1192, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1193, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1194, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1195, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1196, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1197, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1198, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1199, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1200, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1201, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1202, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1203, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1204, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1205, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1206, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1207, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1208, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1209, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1210, 0, 1)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1212, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1213, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1214, 1, 0)
INSERT [dbo].[umbracoDocument] ([nodeId], [published], [edited]) VALUES (1216, 1, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (70, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (71, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (72, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (73, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (74, 1144, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (75, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (76, 1144, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (77, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (78, 1139, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (79, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (80, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (81, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (82, 1144, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (83, 1144, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (84, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (85, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (86, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (87, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (88, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (89, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (90, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (91, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (92, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (93, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (94, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (95, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (96, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (97, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (98, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (99, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (100, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (101, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (102, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (103, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (104, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (105, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (106, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (107, 1144, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (108, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (109, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (110, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (111, 1144, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (112, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (113, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (114, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (115, 1166, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (116, 1166, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (117, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (118, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (119, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (120, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (121, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (122, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (123, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (124, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (125, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (126, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (127, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (128, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (129, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (130, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (131, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (132, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (133, 1168, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (134, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (135, 1168, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (136, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (137, 1168, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (138, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (139, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (140, 1174, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (141, 1174, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (142, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (143, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (144, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (145, 1174, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (146, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (147, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (148, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (149, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (150, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (151, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (152, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (153, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (154, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (155, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (156, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (157, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (158, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (159, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (160, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (161, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (162, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (163, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (164, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (165, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (166, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (167, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (168, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (169, 1144, 1)
GO
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (170, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (171, 1144, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (172, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (173, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (174, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (175, 1166, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (176, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (177, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (178, 1168, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (179, 1174, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (180, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (181, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (182, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (183, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (184, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (185, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (186, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (187, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (188, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (189, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (190, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (191, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (192, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (193, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (194, 1144, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (195, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (196, 1143, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (197, 1166, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (198, 1174, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (199, 1174, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (200, 1166, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (201, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (202, 1143, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (203, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (204, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (205, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (206, 1139, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (207, 1139, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (208, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (209, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (210, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (211, 1166, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (212, 1166, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (213, 1168, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (214, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (215, 1168, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (216, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (217, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (218, 1139, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (219, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (220, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (221, 1176, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (222, 1176, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (224, 1168, 0)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (225, 1168, 1)
INSERT [dbo].[umbracoDocumentVersion] ([id], [templateId], [published]) VALUES (226, 1168, 0)
INSERT [dbo].[umbracoKeyValue] ([key], [value], [updated]) VALUES (N'Umbraco.Core.Upgrader.State+Umbraco.Core', N'{5B1E0D93-F5A3-449B-84BA-65366B84E2D4}', CAST(N'2019-10-03T17:05:36.217' AS DateTime))
SET IDENTITY_INSERT [dbo].[umbracoLanguage] ON 

INSERT [dbo].[umbracoLanguage] ([id], [languageISOCode], [languageCultureName], [isDefaultVariantLang], [mandatory], [fallbackLanguageId]) VALUES (1, N'en-US', N'English (United States)', 1, 0, NULL)
SET IDENTITY_INSERT [dbo].[umbracoLanguage] OFF
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-340, 1, N'Languages')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-339, 1, N'KeyValues')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-338, 1, N'Domains')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-337, 1, N'MemberTypes')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-336, 1, N'MediaTypes')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-335, 1, N'MemberTree')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-334, 1, N'MediaTree')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-333, -1, N'ContentTree')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-332, -1, N'ContentTypes')
INSERT [dbo].[umbracoLock] ([id], [value], [name]) VALUES (-331, 1, N'Servers')
SET IDENTITY_INSERT [dbo].[umbracoLog] ON 

INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (1, -1, -1, N'Package', CAST(N'2019-10-03T17:05:53.423' AS DateTime), N'PackagerInstall', N'Package files installed for package ''The Starter Kit''.', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (2, -1, 1048, N'DataType', CAST(N'2019-10-03T17:05:59.593' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (3, -1, -1, N'DataType', CAST(N'2019-10-03T17:05:59.747' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (4, -1, -1, N'Macro', CAST(N'2019-10-03T17:05:59.870' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (5, -1, -1, N'Macro', CAST(N'2019-10-03T17:05:59.880' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (6, -1, -1, N'Template', CAST(N'2019-10-03T17:06:00.120' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (7, -1, -1, N'DocumentType', CAST(N'2019-10-03T17:06:00.547' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (8, -1, -1, N'DocumentType', CAST(N'2019-10-03T17:06:00.817' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (9, NULL, -1, N'Document', CAST(N'2019-10-03T17:06:01.177' AS DateTime), N'Save', N'Saved multiple content', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (10, -1, 1092, N'DocumentType', CAST(N'2019-10-03T17:06:01.493' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (11, -1, 1074, N'Template', CAST(N'2019-10-03T17:06:01.523' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (12, -1, 1072, N'Template', CAST(N'2019-10-03T17:06:01.587' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (13, -1, 1073, N'Template', CAST(N'2019-10-03T17:06:01.593' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (14, -1, 1074, N'Template', CAST(N'2019-10-03T17:06:01.603' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (15, -1, 1075, N'Template', CAST(N'2019-10-03T17:06:01.607' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (16, -1, 1076, N'Template', CAST(N'2019-10-03T17:06:01.617' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (17, -1, 1078, N'Template', CAST(N'2019-10-03T17:06:01.623' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (18, -1, 1079, N'Template', CAST(N'2019-10-03T17:06:01.630' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (19, -1, 1080, N'Template', CAST(N'2019-10-03T17:06:01.637' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (20, -1, 1081, N'Template', CAST(N'2019-10-03T17:06:01.643' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (21, -1, 1119, N'Media', CAST(N'2019-10-03T17:06:01.707' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (22, -1, 1120, N'Media', CAST(N'2019-10-03T17:06:01.720' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (23, -1, 1121, N'Media', CAST(N'2019-10-03T17:06:01.733' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (24, -1, 1122, N'Media', CAST(N'2019-10-03T17:06:01.770' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (25, -1, 1123, N'Media', CAST(N'2019-10-03T17:06:01.777' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (26, -1, 1124, N'Media', CAST(N'2019-10-03T17:06:01.790' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (27, -1, 1125, N'Media', CAST(N'2019-10-03T17:06:01.800' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (28, -1, 1126, N'Media', CAST(N'2019-10-03T17:06:01.810' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (29, -1, 1127, N'Media', CAST(N'2019-10-03T17:06:01.820' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (30, -1, 1128, N'Media', CAST(N'2019-10-03T17:06:01.827' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (31, -1, 1129, N'Media', CAST(N'2019-10-03T17:06:01.837' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (32, -1, 1130, N'Media', CAST(N'2019-10-03T17:06:01.847' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (33, -1, 1131, N'Media', CAST(N'2019-10-03T17:06:01.887' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (34, -1, 1132, N'Media', CAST(N'2019-10-03T17:06:01.893' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (35, -1, 1133, N'Media', CAST(N'2019-10-03T17:06:01.903' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (36, -1, 1134, N'Media', CAST(N'2019-10-03T17:06:01.910' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (37, -1, 1135, N'Media', CAST(N'2019-10-03T17:06:01.920' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (38, -1, 1095, N'Document', CAST(N'2019-10-03T17:06:02.010' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (39, -1, 1096, N'Document', CAST(N'2019-10-03T17:06:02.080' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (40, -1, 1097, N'Document', CAST(N'2019-10-03T17:06:02.123' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (41, -1, 1098, N'Document', CAST(N'2019-10-03T17:06:02.147' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (42, -1, 1099, N'Document', CAST(N'2019-10-03T17:06:02.173' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (43, -1, 1100, N'Document', CAST(N'2019-10-03T17:06:02.190' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (44, -1, 1101, N'Document', CAST(N'2019-10-03T17:06:02.217' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (45, -1, 1102, N'Document', CAST(N'2019-10-03T17:06:02.240' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (46, -1, 1103, N'Document', CAST(N'2019-10-03T17:06:02.263' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (47, -1, 1104, N'Document', CAST(N'2019-10-03T17:06:02.300' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (48, -1, 1105, N'Document', CAST(N'2019-10-03T17:06:02.310' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (49, -1, 1106, N'Document', CAST(N'2019-10-03T17:06:02.333' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (50, -1, 1107, N'Document', CAST(N'2019-10-03T17:06:02.360' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (51, -1, 1108, N'Document', CAST(N'2019-10-03T17:06:02.377' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (52, -1, 1109, N'Document', CAST(N'2019-10-03T17:06:02.393' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (53, -1, 1110, N'Document', CAST(N'2019-10-03T17:06:02.420' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (54, -1, 1111, N'Document', CAST(N'2019-10-03T17:06:02.433' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (55, -1, 1112, N'Document', CAST(N'2019-10-03T17:06:02.443' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (56, -1, 1113, N'Document', CAST(N'2019-10-03T17:06:02.457' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (57, -1, 1114, N'Document', CAST(N'2019-10-03T17:06:02.463' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (58, -1, 1115, N'Document', CAST(N'2019-10-03T17:06:02.490' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (59, -1, 1116, N'Document', CAST(N'2019-10-03T17:06:02.517' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (60, -1, 1117, N'Document', CAST(N'2019-10-03T17:06:02.543' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (61, -1, 1118, N'Document', CAST(N'2019-10-03T17:06:02.557' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (62, -1, 1095, N'Document', CAST(N'2019-10-03T17:06:02.567' AS DateTime), N'Publish', N'Branch published', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (63, -1, -1, N'Package', CAST(N'2019-10-03T17:06:02.630' AS DateTime), N'PackagerInstall', N'Package data installed for package ''The Starter Kit''.', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (64, -1, 1095, N'Document', CAST(N'2019-10-03T17:12:08.730' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (65, -1, 1095, N'Document', CAST(N'2019-10-03T17:13:00.433' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (66, -1, 1095, N'Document', CAST(N'2019-10-03T17:13:06.340' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (67, -1, 1095, N'Document', CAST(N'2019-10-03T17:13:14.087' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (68, -1, 1095, N'Document', CAST(N'2019-10-03T17:13:21.200' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (69, -1, 1095, N'Document', CAST(N'2019-10-03T17:14:31.810' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (70, -1, 1111, N'Document', CAST(N'2019-10-03T17:16:25.533' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (71, -1, 1136, N'Document', CAST(N'2019-10-03T17:18:55.590' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (72, -1, 1136, N'Document', CAST(N'2019-10-03T17:18:58.353' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (73, -1, 1137, N'Media', CAST(N'2019-10-03T17:20:19.683' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (74, -1, 1095, N'Document', CAST(N'2019-10-03T17:23:01.387' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (75, -1, 1095, N'Document', CAST(N'2019-10-03T17:23:39.920' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (76, -1, 1095, N'Document', CAST(N'2019-10-03T17:54:56.737' AS DateTime), N'Move', N'Moved to recycle bin', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (77, -1, 1095, N'Document', CAST(N'2019-10-03T17:54:56.910' AS DateTime), N'Delete', N'Trashed content with Id: 1095 related to original parent content with Id: -1', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (78, -1, 1096, N'Document', CAST(N'2019-10-03T17:54:56.913' AS DateTime), N'Delete', N'Trashed content with Id: 1096 related to original parent content with Id: 1095', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (79, -1, 1097, N'Document', CAST(N'2019-10-03T17:54:56.917' AS DateTime), N'Delete', N'Trashed content with Id: 1097 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (80, -1, 1098, N'Document', CAST(N'2019-10-03T17:54:56.917' AS DateTime), N'Delete', N'Trashed content with Id: 1098 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (81, -1, 1099, N'Document', CAST(N'2019-10-03T17:54:56.920' AS DateTime), N'Delete', N'Trashed content with Id: 1099 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (82, -1, 1100, N'Document', CAST(N'2019-10-03T17:54:56.920' AS DateTime), N'Delete', N'Trashed content with Id: 1100 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (83, -1, 1101, N'Document', CAST(N'2019-10-03T17:54:56.923' AS DateTime), N'Delete', N'Trashed content with Id: 1101 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (84, -1, 1102, N'Document', CAST(N'2019-10-03T17:54:56.927' AS DateTime), N'Delete', N'Trashed content with Id: 1102 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (85, -1, 1103, N'Document', CAST(N'2019-10-03T17:54:56.933' AS DateTime), N'Delete', N'Trashed content with Id: 1103 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (86, -1, 1104, N'Document', CAST(N'2019-10-03T17:54:56.937' AS DateTime), N'Delete', N'Trashed content with Id: 1104 related to original parent content with Id: 1096', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (87, -1, 1105, N'Document', CAST(N'2019-10-03T17:54:56.937' AS DateTime), N'Delete', N'Trashed content with Id: 1105 related to original parent content with Id: 1095', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (88, -1, 1106, N'Document', CAST(N'2019-10-03T17:54:56.940' AS DateTime), N'Delete', N'Trashed content with Id: 1106 related to original parent content with Id: 1105', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (89, -1, 1107, N'Document', CAST(N'2019-10-03T17:54:56.940' AS DateTime), N'Delete', N'Trashed content with Id: 1107 related to original parent content with Id: 1105', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (90, -1, 1108, N'Document', CAST(N'2019-10-03T17:54:56.943' AS DateTime), N'Delete', N'Trashed content with Id: 1108 related to original parent content with Id: 1105', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (91, -1, 1109, N'Document', CAST(N'2019-10-03T17:54:56.947' AS DateTime), N'Delete', N'Trashed content with Id: 1109 related to original parent content with Id: 1105', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (92, -1, 1110, N'Document', CAST(N'2019-10-03T17:54:56.947' AS DateTime), N'Delete', N'Trashed content with Id: 1110 related to original parent content with Id: 1105', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (93, -1, 1111, N'Document', CAST(N'2019-10-03T17:54:56.950' AS DateTime), N'Delete', N'Trashed content with Id: 1111 related to original parent content with Id: 1095', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (94, -1, 1112, N'Document', CAST(N'2019-10-03T17:54:56.950' AS DateTime), N'Delete', N'Trashed content with Id: 1112 related to original parent content with Id: 1111', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (95, -1, 1113, N'Document', CAST(N'2019-10-03T17:54:56.953' AS DateTime), N'Delete', N'Trashed content with Id: 1113 related to original parent content with Id: 1111', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (96, -1, 1114, N'Document', CAST(N'2019-10-03T17:54:56.953' AS DateTime), N'Delete', N'Trashed content with Id: 1114 related to original parent content with Id: 1095', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (97, -1, 1115, N'Document', CAST(N'2019-10-03T17:54:56.957' AS DateTime), N'Delete', N'Trashed content with Id: 1115 related to original parent content with Id: 1114', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (98, -1, 1116, N'Document', CAST(N'2019-10-03T17:54:56.960' AS DateTime), N'Delete', N'Trashed content with Id: 1116 related to original parent content with Id: 1114', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (99, -1, 1117, N'Document', CAST(N'2019-10-03T17:54:56.963' AS DateTime), N'Delete', N'Trashed content with Id: 1117 related to original parent content with Id: 1114', NULL)
GO
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (100, -1, 1118, N'Document', CAST(N'2019-10-03T17:54:56.963' AS DateTime), N'Delete', N'Trashed content with Id: 1118 related to original parent content with Id: 1095', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (101, -1, 1136, N'Document', CAST(N'2019-10-03T17:54:56.967' AS DateTime), N'Delete', N'Trashed content with Id: 1136 related to original parent content with Id: 1095', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (102, -1, 1081, N'Template', CAST(N'2019-10-04T09:46:18.173' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (103, -1, 1080, N'Template', CAST(N'2019-10-04T09:46:21.230' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (104, -1, 1079, N'Template', CAST(N'2019-10-04T09:46:24.817' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (105, -1, 1072, N'Template', CAST(N'2019-10-04T09:48:19.957' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (106, -1, 1073, N'Template', CAST(N'2019-10-04T09:48:24.723' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (107, -1, 1074, N'Template', CAST(N'2019-10-04T09:48:26.870' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (108, -1, 1075, N'Template', CAST(N'2019-10-04T09:48:28.707' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (109, -1, 1076, N'Template', CAST(N'2019-10-04T09:48:30.747' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (110, -1, 1078, N'Template', CAST(N'2019-10-04T09:48:34.087' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (111, -1, 1077, N'Template', CAST(N'2019-10-04T09:48:36.933' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (112, -1, -1, N'Document', CAST(N'2019-10-04T09:49:17.343' AS DateTime), N'Delete', N'Delete content of type 1083', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (113, -1, 1083, N'DocumentType', CAST(N'2019-10-04T09:49:17.933' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (114, -1, -1, N'Document', CAST(N'2019-10-04T09:49:20.137' AS DateTime), N'Delete', N'Delete content of type 1090', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (115, -1, 1090, N'DocumentType', CAST(N'2019-10-04T09:49:20.147' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (116, -1, -1, N'Document', CAST(N'2019-10-04T09:49:25.393' AS DateTime), N'Delete', N'Delete content of type 1084', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (117, -1, 1084, N'DocumentType', CAST(N'2019-10-04T09:49:25.487' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (118, -1, -1, N'Document', CAST(N'2019-10-04T09:49:39.320' AS DateTime), N'Delete', N'Delete content of type 1094', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (119, -1, 1094, N'DocumentType', CAST(N'2019-10-04T09:49:39.330' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (120, -1, -1, N'Document', CAST(N'2019-10-04T09:49:46.037' AS DateTime), N'Delete', N'Delete content of type 1093', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (121, -1, 1093, N'DocumentType', CAST(N'2019-10-04T09:49:46.043' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (122, -1, -1, N'Document', CAST(N'2019-10-04T09:49:53.177' AS DateTime), N'Delete', N'Delete content of type 1092', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (123, -1, 1092, N'DocumentType', CAST(N'2019-10-04T09:49:53.183' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (124, -1, -1, N'Document', CAST(N'2019-10-04T09:49:56.320' AS DateTime), N'Delete', N'Delete content of type 1091', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (125, -1, 1091, N'DocumentType', CAST(N'2019-10-04T09:49:56.327' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (126, -1, -1, N'Document', CAST(N'2019-10-04T09:50:00.083' AS DateTime), N'Delete', N'Delete content of type 1088', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (127, -1, 1088, N'DocumentType', CAST(N'2019-10-04T09:50:00.093' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (128, -1, -1, N'Document', CAST(N'2019-10-04T09:50:02.937' AS DateTime), N'Delete', N'Delete content of type 1089', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (129, -1, 1089, N'DocumentType', CAST(N'2019-10-04T09:50:02.947' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (130, -1, -1, N'Document', CAST(N'2019-10-04T09:50:07.043' AS DateTime), N'Delete', N'Delete content of type 1087', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (131, -1, 1087, N'DocumentType', CAST(N'2019-10-04T09:50:07.050' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (132, -1, -1, N'Document', CAST(N'2019-10-04T09:50:10.483' AS DateTime), N'Delete', N'Delete content of type 1085', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (133, -1, 1085, N'DocumentType', CAST(N'2019-10-04T09:50:10.490' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (134, -1, -1, N'Document', CAST(N'2019-10-04T09:50:15.573' AS DateTime), N'Delete', N'Delete content of type 1086', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (135, -1, 1086, N'DocumentType', CAST(N'2019-10-04T09:50:15.637' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (136, -1, 1120, N'Media', CAST(N'2019-10-04T11:42:56.300' AS DateTime), N'Move', N'Move Media to recycle bin', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (137, -1, 1120, N'Media', CAST(N'2019-10-04T11:42:56.443' AS DateTime), N'Delete', N'Trashed media with Id: 1120 related to original parent media item with Id: -1', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (138, -1, 1131, N'Media', CAST(N'2019-10-04T11:42:56.500' AS DateTime), N'Delete', N'Trashed media with Id: 1131 related to original parent media item with Id: 1120', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (139, -1, 1132, N'Media', CAST(N'2019-10-04T11:42:56.523' AS DateTime), N'Delete', N'Trashed media with Id: 1132 related to original parent media item with Id: 1120', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (140, -1, 1133, N'Media', CAST(N'2019-10-04T11:42:56.527' AS DateTime), N'Delete', N'Trashed media with Id: 1133 related to original parent media item with Id: 1120', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (141, -1, 1134, N'Media', CAST(N'2019-10-04T11:42:56.530' AS DateTime), N'Delete', N'Trashed media with Id: 1134 related to original parent media item with Id: 1120', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (142, -1, 1135, N'Media', CAST(N'2019-10-04T11:42:56.533' AS DateTime), N'Delete', N'Trashed media with Id: 1135 related to original parent media item with Id: 1120', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (143, -1, 1121, N'Media', CAST(N'2019-10-04T11:43:02.500' AS DateTime), N'Move', N'Move Media to recycle bin', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (144, -1, 1121, N'Media', CAST(N'2019-10-04T11:43:02.673' AS DateTime), N'Delete', N'Trashed media with Id: 1121 related to original parent media item with Id: -1', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (145, -1, 1123, N'Media', CAST(N'2019-10-04T11:43:02.673' AS DateTime), N'Delete', N'Trashed media with Id: 1123 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (146, -1, 1124, N'Media', CAST(N'2019-10-04T11:43:02.677' AS DateTime), N'Delete', N'Trashed media with Id: 1124 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (147, -1, 1125, N'Media', CAST(N'2019-10-04T11:43:02.677' AS DateTime), N'Delete', N'Trashed media with Id: 1125 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (148, -1, 1126, N'Media', CAST(N'2019-10-04T11:43:02.680' AS DateTime), N'Delete', N'Trashed media with Id: 1126 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (149, -1, 1127, N'Media', CAST(N'2019-10-04T11:43:02.683' AS DateTime), N'Delete', N'Trashed media with Id: 1127 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (150, -1, 1128, N'Media', CAST(N'2019-10-04T11:43:02.687' AS DateTime), N'Delete', N'Trashed media with Id: 1128 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (151, -1, 1129, N'Media', CAST(N'2019-10-04T11:43:02.690' AS DateTime), N'Delete', N'Trashed media with Id: 1129 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (152, -1, 1130, N'Media', CAST(N'2019-10-04T11:43:02.703' AS DateTime), N'Delete', N'Trashed media with Id: 1130 related to original parent media item with Id: 1121', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (153, -1, 1119, N'Media', CAST(N'2019-10-04T11:43:09.443' AS DateTime), N'Move', N'Move Media to recycle bin', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (154, -1, 1119, N'Media', CAST(N'2019-10-04T11:43:09.547' AS DateTime), N'Delete', N'Trashed media with Id: 1119 related to original parent media item with Id: -1', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (155, -1, 1122, N'Media', CAST(N'2019-10-04T11:43:09.550' AS DateTime), N'Delete', N'Trashed media with Id: 1122 related to original parent media item with Id: 1119', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (156, -1, 1137, N'Media', CAST(N'2019-10-04T11:43:09.550' AS DateTime), N'Delete', N'Trashed media with Id: 1137 related to original parent media item with Id: 1119', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (157, -1, 1138, N'Template', CAST(N'2019-10-04T11:44:59.267' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (158, -1, 1139, N'Template', CAST(N'2019-10-04T11:45:21.757' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (159, -1, 1140, N'DocumentType', CAST(N'2019-10-04T11:46:24.293' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (160, -1, 1140, N'DocumentType', CAST(N'2019-10-04T11:46:27.050' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (161, -1, 1141, N'Document', CAST(N'2019-10-04T11:49:53.660' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (162, -1, 1141, N'Document', CAST(N'2019-10-04T11:49:54.780' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (163, -1, 1141, N'Document', CAST(N'2019-10-04T11:50:05.323' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (164, -1, 1142, N'Template', CAST(N'2019-10-04T14:31:04.017' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (165, -1, 1142, N'Template', CAST(N'2019-10-04T14:31:08.263' AS DateTime), N'Delete', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (166, -1, 1143, N'Template', CAST(N'2019-10-04T14:32:02.643' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (167, -1, 1144, N'Template', CAST(N'2019-10-04T14:32:42.303' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (168, -1, 1145, N'DocumentType', CAST(N'2019-10-04T14:33:05.290' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (169, -1, 1145, N'DocumentType', CAST(N'2019-10-04T14:33:34.977' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (170, -1, 1146, N'DocumentType', CAST(N'2019-10-04T14:33:58.727' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (171, -1, 1140, N'DocumentType', CAST(N'2019-10-04T14:34:28.147' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (172, -1, 1147, N'Document', CAST(N'2019-10-04T14:35:00.380' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (173, -1, 1147, N'Document', CAST(N'2019-10-04T14:35:58.900' AS DateTime), N'Move', N'Moved to recycle bin', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (174, -1, 1147, N'Document', CAST(N'2019-10-04T14:35:59.060' AS DateTime), N'Delete', N'Trashed content with Id: 1147 related to original parent content with Id: -1', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (175, -1, 1148, N'Document', CAST(N'2019-10-04T14:36:12.640' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (176, -1, 1149, N'Document', CAST(N'2019-10-04T14:36:27.357' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (177, -1, 1148, N'Document', CAST(N'2019-10-04T14:56:09.967' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (178, -1, -1, N'PartialView', CAST(N'2019-10-04T15:00:54.973' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (179, -1, 1148, N'Document', CAST(N'2019-10-04T15:02:17.790' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (180, -1, 1148, N'Document', CAST(N'2019-10-04T15:05:15.637' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (181, -1, 1149, N'Document', CAST(N'2019-10-04T15:05:19.687' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (182, -1, 1149, N'Document', CAST(N'2019-10-04T15:05:24.830' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (183, -1, 1148, N'Document', CAST(N'2019-10-04T15:05:27.463' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (184, -1, 1141, N'Document', CAST(N'2019-10-04T15:05:31.323' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (185, -1, 1141, N'Document', CAST(N'2019-10-04T15:05:31.753' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (186, -1, 1141, N'Document', CAST(N'2019-10-04T15:05:34.667' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (187, -1, -1, N'PartialView', CAST(N'2019-10-07T09:33:52.163' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (188, -1, 1150, N'Document', CAST(N'2019-10-07T10:28:16.190' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (189, -1, 1150, N'Document', CAST(N'2019-10-07T10:28:18.987' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (190, -1, 1150, N'Document', CAST(N'2019-10-07T10:28:19.883' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (191, -1, 1150, N'Document', CAST(N'2019-10-07T10:29:12.287' AS DateTime), N'Move', N'Moved to recycle bin', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (192, -1, 1150, N'Document', CAST(N'2019-10-07T10:29:12.450' AS DateTime), N'Delete', N'Trashed content with Id: 1150 related to original parent content with Id: -1', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (193, -1, 1151, N'Template', CAST(N'2019-10-07T10:32:24.270' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (194, -1, 1152, N'DocumentType', CAST(N'2019-10-07T10:32:54.803' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (195, -1, 1152, N'DocumentType', CAST(N'2019-10-07T10:33:06.693' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (196, -1, 1153, N'DocumentType', CAST(N'2019-10-07T10:34:39.073' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (197, -1, 1140, N'DocumentType', CAST(N'2019-10-07T10:35:13.497' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (198, -1, 1152, N'DocumentType', CAST(N'2019-10-07T10:35:20.827' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (199, -1, 1152, N'DocumentType', CAST(N'2019-10-07T10:35:59.200' AS DateTime), N'Save', NULL, NULL)
GO
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (200, -1, 1153, N'DocumentType', CAST(N'2019-10-07T10:36:09.260' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (201, -1, 1154, N'Document', CAST(N'2019-10-07T10:36:36.803' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (202, -1, 1154, N'Document', CAST(N'2019-10-07T10:36:37.823' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (203, -1, 1155, N'Document', CAST(N'2019-10-07T10:37:06.970' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (204, -1, 1152, N'DocumentType', CAST(N'2019-10-07T11:04:24.487' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (205, -1, 1156, N'Document', CAST(N'2019-10-07T11:04:45.707' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (206, -1, 1157, N'Document', CAST(N'2019-10-07T11:05:23.823' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (207, -1, 1159, N'DataType', CAST(N'2019-10-07T11:37:28.930' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (208, -1, 1160, N'DocumentType', CAST(N'2019-10-07T11:38:45.783' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (209, -1, 1140, N'DocumentType', CAST(N'2019-10-07T11:39:38.710' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (210, -1, 1141, N'Document', CAST(N'2019-10-07T11:41:34.600' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (211, -1, 1141, N'Document', CAST(N'2019-10-07T11:41:36.833' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (212, -1, 1141, N'Document', CAST(N'2019-10-07T11:48:46.203' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (213, -1, 1141, N'Document', CAST(N'2019-10-07T13:37:04.603' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (214, -1, 1161, N'DataType', CAST(N'2019-10-07T13:51:04.360' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (215, -1, 1140, N'DocumentType', CAST(N'2019-10-07T13:51:12.927' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (216, -1, 1141, N'Document', CAST(N'2019-10-07T13:57:25.470' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (217, -1, 1141, N'Document', CAST(N'2019-10-07T14:08:22.430' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (218, -1, 1141, N'Document', CAST(N'2019-10-07T14:08:42.690' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (219, -1, 1141, N'Document', CAST(N'2019-10-07T14:09:46.047' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (220, -1, 1141, N'Document', CAST(N'2019-10-07T14:10:49.027' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (221, -1, 1140, N'DocumentType', CAST(N'2019-10-07T15:30:20.010' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (222, -1, 1140, N'DocumentType', CAST(N'2019-10-07T15:33:40.557' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (223, -1, 1141, N'Document', CAST(N'2019-10-07T15:34:36.440' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (224, -1, 1141, N'Document', CAST(N'2019-10-07T15:44:42.577' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (225, -1, 1141, N'Document', CAST(N'2019-10-07T21:00:15.890' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (226, -1, 1162, N'Document', CAST(N'2019-10-07T22:00:29.900' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (227, -1, 1162, N'Document', CAST(N'2019-10-07T22:00:35.987' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (228, -1, 1141, N'Document', CAST(N'2019-10-07T22:00:59.303' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (229, -1, 1141, N'Document', CAST(N'2019-10-07T22:01:22.090' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (230, -1, 1141, N'Document', CAST(N'2019-10-07T22:01:28.550' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (231, -1, 1141, N'Document', CAST(N'2019-10-07T22:01:48.870' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (232, -1, 1141, N'Document', CAST(N'2019-10-07T22:01:51.023' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (233, -1, 1141, N'Document', CAST(N'2019-10-07T22:02:27.930' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (234, -1, 1141, N'Document', CAST(N'2019-10-07T22:02:29.197' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (235, -1, 1140, N'DocumentType', CAST(N'2019-10-07T22:04:06.587' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (236, -1, 1141, N'Document', CAST(N'2019-10-07T22:04:24.973' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (237, -1, 1140, N'DocumentType', CAST(N'2019-10-07T22:09:25.207' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (238, -1, 1141, N'Document', CAST(N'2019-10-07T22:09:56.163' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (239, -1, 1140, N'DocumentType', CAST(N'2019-10-07T22:12:31.950' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (240, -1, 1141, N'Document', CAST(N'2019-10-07T22:12:43.150' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (241, -1, 1163, N'Template', CAST(N'2019-10-07T22:18:01.553' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (242, -1, 1164, N'DocumentType', CAST(N'2019-10-07T22:18:01.627' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (243, -1, 1164, N'DocumentType', CAST(N'2019-10-07T22:19:38.393' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (244, -1, 1140, N'DocumentType', CAST(N'2019-10-07T22:20:16.833' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (245, -1, 1140, N'DocumentType', CAST(N'2019-10-07T22:21:15.463' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (246, -1, 1152, N'DocumentType', CAST(N'2019-10-07T22:21:53.497' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (247, -1, 1153, N'DocumentType', CAST(N'2019-10-07T22:22:00.897' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (248, -1, 1145, N'DocumentType', CAST(N'2019-10-07T22:22:34.747' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (249, -1, 1155, N'Document', CAST(N'2019-10-07T22:23:32.217' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (250, -1, 1156, N'Document', CAST(N'2019-10-07T22:23:37.263' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (251, -1, 1148, N'Document', CAST(N'2019-10-07T23:21:41.287' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (252, -1, 1148, N'Document', CAST(N'2019-10-07T23:21:51.677' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (253, -1, 1165, N'DataType', CAST(N'2019-10-07T23:24:11.197' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (254, -1, 1164, N'DocumentType', CAST(N'2019-10-07T23:25:08.673' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (255, -1, 1155, N'Document', CAST(N'2019-10-07T23:25:29.807' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (256, -1, 1164, N'DocumentType', CAST(N'2019-10-07T23:26:02.613' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (257, -1, 1148, N'Document', CAST(N'2019-10-07T23:26:09.523' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (258, -1, 1164, N'DocumentType', CAST(N'2019-10-07T23:27:10.130' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (259, -1, 1148, N'Document', CAST(N'2019-10-07T23:27:20.663' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (260, -1, 1141, N'Document', CAST(N'2019-10-08T09:37:50.643' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (261, -1, 1141, N'Document', CAST(N'2019-10-08T09:38:03.080' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (262, -1, 1166, N'Template', CAST(N'2019-10-08T09:43:59.017' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (263, -1, 1167, N'DocumentType', CAST(N'2019-10-08T09:43:59.077' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (264, -1, 1167, N'DocumentType', CAST(N'2019-10-08T09:45:19.710' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (265, -1, 1168, N'Template', CAST(N'2019-10-08T09:48:03.497' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (266, -1, 1169, N'DocumentType', CAST(N'2019-10-08T09:48:03.523' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (267, -1, 1166, N'Template', CAST(N'2019-10-08T09:49:13.237' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (268, -1, 1168, N'Template', CAST(N'2019-10-08T09:49:19.060' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (269, -1, 1140, N'DocumentType', CAST(N'2019-10-08T09:49:39.073' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (270, -1, 1167, N'DocumentType', CAST(N'2019-10-08T09:49:51.570' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (271, -1, 1170, N'Document', CAST(N'2019-10-08T09:51:13.123' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (272, -1, 1171, N'Document', CAST(N'2019-10-08T09:52:57.643' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (273, -1, 1171, N'Document', CAST(N'2019-10-08T09:53:07.553' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (274, -1, 1162, N'Document', CAST(N'2019-10-08T09:53:23.443' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (275, -1, 1154, N'Document', CAST(N'2019-10-08T09:53:39.070' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (276, -1, 1148, N'Document', CAST(N'2019-10-08T09:53:56.030' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (277, -1, 1172, N'Document', CAST(N'2019-10-08T09:54:20.140' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (278, -1, 1171, N'Document', CAST(N'2019-10-08T09:54:29.553' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (279, -1, 1173, N'Document', CAST(N'2019-10-08T09:55:37.687' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (280, -1, 1171, N'Document', CAST(N'2019-10-08T11:40:36.400' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (281, -1, 1172, N'Document', CAST(N'2019-10-08T11:40:39.343' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (282, -1, 1173, N'Document', CAST(N'2019-10-08T11:40:43.333' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (283, -1, 1171, N'Document', CAST(N'2019-10-08T11:41:04.003' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (284, -1, 1171, N'Document', CAST(N'2019-10-08T11:42:02.033' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (285, -1, 1171, N'Document', CAST(N'2019-10-08T13:25:15.003' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (286, -1, 1172, N'Document', CAST(N'2019-10-08T13:25:32.020' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (287, -1, 1173, N'Document', CAST(N'2019-10-08T13:25:49.617' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (288, -1, 1173, N'Document', CAST(N'2019-10-08T13:33:51.170' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (289, -1, 1172, N'Document', CAST(N'2019-10-08T13:33:53.563' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (290, -1, 1171, N'Document', CAST(N'2019-10-08T13:33:56.783' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (291, -1, 1172, N'Document', CAST(N'2019-10-08T13:59:00.620' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (292, -1, 1174, N'Template', CAST(N'2019-10-09T09:32:46.273' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (293, -1, 1175, N'DocumentType', CAST(N'2019-10-09T09:32:46.357' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (294, -1, 1176, N'Template', CAST(N'2019-10-09T09:33:00.473' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (295, -1, 1177, N'DocumentType', CAST(N'2019-10-09T09:33:00.483' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (296, -1, 1177, N'DocumentType', CAST(N'2019-10-09T09:34:59.017' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (297, -1, 1140, N'DocumentType', CAST(N'2019-10-09T09:35:34.043' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (298, -1, 1175, N'DocumentType', CAST(N'2019-10-09T09:35:43.670' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (299, -1, 1178, N'Document', CAST(N'2019-10-09T09:36:03.480' AS DateTime), N'Publish', NULL, NULL)
GO
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (300, -1, 1175, N'DocumentType', CAST(N'2019-10-09T09:36:29.530' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (301, -1, 1179, N'Document', CAST(N'2019-10-09T09:37:41.477' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (302, -1, 1175, N'DocumentType', CAST(N'2019-10-09T09:37:53.037' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (303, -1, 1174, N'Template', CAST(N'2019-10-09T09:38:43.880' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (304, -1, 1176, N'Template', CAST(N'2019-10-09T09:38:53.040' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (305, -1, 1177, N'DocumentType', CAST(N'2019-10-09T09:41:08.460' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (306, -1, 1179, N'Document', CAST(N'2019-10-09T09:41:13.883' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (307, -1, 1177, N'DocumentType', CAST(N'2019-10-09T09:48:05.550' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (308, -1, 1178, N'Document', CAST(N'2019-10-09T09:53:04.297' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (309, -1, 1179, N'Document', CAST(N'2019-10-09T09:53:09.857' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (310, -1, 1178, N'Document', CAST(N'2019-10-09T10:07:47.220' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (311, -1, 1175, N'DocumentType', CAST(N'2019-10-09T10:08:28.327' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (312, -1, 1177, N'DocumentType', CAST(N'2019-10-09T10:09:14.353' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (313, -1, 1177, N'DocumentType', CAST(N'2019-10-09T10:30:17.510' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (314, -1, 1177, N'DocumentType', CAST(N'2019-10-09T10:45:54.100' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (315, -1, 1179, N'Document', CAST(N'2019-10-09T11:58:38.140' AS DateTime), N'Move', N'Moved to recycle bin', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (316, -1, 1179, N'Document', CAST(N'2019-10-09T11:58:38.427' AS DateTime), N'Delete', N'Trashed content with Id: 1179 related to original parent content with Id: 1178', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (317, -1, 1180, N'Document', CAST(N'2019-10-09T11:59:07.907' AS DateTime), N'New', N'Content ''quan--10/9/2019 11:59:07 AM'' was created with Id 1180', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (318, -1, 1180, N'Document', CAST(N'2019-10-09T11:59:08.230' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (319, -1, 1181, N'Document', CAST(N'2019-10-09T12:00:39.370' AS DateTime), N'New', N'Content ''quanfx--10/9/2019 12:00:39 PM'' was created with Id 1181', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (320, -1, 1181, N'Document', CAST(N'2019-10-09T12:00:39.897' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (321, -1, 1182, N'Document', CAST(N'2019-10-09T13:43:09.243' AS DateTime), N'New', N'Content ''adminne--10/9/2019 1:43:08 PM'' was created with Id 1182', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (322, -1, 1182, N'Document', CAST(N'2019-10-09T13:43:09.797' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (323, -1, 1180, N'Document', CAST(N'2019-10-09T13:43:25.977' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (324, -1, 1181, N'Document', CAST(N'2019-10-09T13:43:30.030' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (325, -1, 1183, N'Document', CAST(N'2019-10-09T13:51:19.463' AS DateTime), N'New', N'Content ''Nhi--10/9/2019 1:51:19 PM'' was created with Id 1183', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (326, -1, 1183, N'Document', CAST(N'2019-10-09T13:51:19.980' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (327, -1, 1184, N'Document', CAST(N'2019-10-09T14:39:07.240' AS DateTime), N'New', N'Content ''Lux A2.0--10/9/2019 2:39:06 PM'' was created with Id 1184', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (328, -1, 1184, N'Document', CAST(N'2019-10-09T14:39:07.833' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (329, -1, 1185, N'Document', CAST(N'2019-10-09T14:49:32.463' AS DateTime), N'New', N'Content ''Lux A2.0--10/9/2019 2:49:32 PM'' was created with Id 1185', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (330, -1, 1185, N'Document', CAST(N'2019-10-09T14:49:33.007' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (331, -1, 1186, N'Document', CAST(N'2019-10-09T14:50:05.003' AS DateTime), N'New', N'Content ''Lux A2.0--10/9/2019 2:50:04 PM'' was created with Id 1186', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (332, -1, 1186, N'Document', CAST(N'2019-10-09T14:50:05.193' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (333, -1, 1187, N'Document', CAST(N'2019-10-09T14:50:46.560' AS DateTime), N'New', N'Content ''quanfx--10/9/2019 2:50:46 PM'' was created with Id 1187', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (334, -1, 1187, N'Document', CAST(N'2019-10-09T14:50:47.127' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (335, -1, 1188, N'Document', CAST(N'2019-10-09T15:33:13.573' AS DateTime), N'New', N'Content ''Product--10/9/2019 3:33:13 PM'' was created with Id 1188', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (336, -1, 1188, N'Document', CAST(N'2019-10-09T15:33:14.110' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (337, -1, 1140, N'DocumentType', CAST(N'2019-10-09T15:45:30.763' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (338, -1, 1141, N'Document', CAST(N'2019-10-09T15:45:59.280' AS DateTime), N'Copy', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (339, -1, 1189, N'Document', CAST(N'2019-10-09T15:45:59.440' AS DateTime), N'Copy', N'Copied content with Id: ''1189'' related to original content with Id: ''1141''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (340, -1, 1189, N'Document', CAST(N'2019-10-09T15:45:59.487' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (341, -1, 1190, N'Document', CAST(N'2019-10-09T15:45:59.633' AS DateTime), N'Copy', N'Copied content with Id: ''1190'' related to original content with Id: ''1148''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (342, -1, 1191, N'Document', CAST(N'2019-10-09T15:45:59.640' AS DateTime), N'Copy', N'Copied content with Id: ''1191'' related to original content with Id: ''1149''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (343, -1, 1192, N'Document', CAST(N'2019-10-09T15:45:59.647' AS DateTime), N'Copy', N'Copied content with Id: ''1192'' related to original content with Id: ''1154''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (344, -1, 1193, N'Document', CAST(N'2019-10-09T15:45:59.653' AS DateTime), N'Copy', N'Copied content with Id: ''1193'' related to original content with Id: ''1155''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (345, -1, 1194, N'Document', CAST(N'2019-10-09T15:45:59.660' AS DateTime), N'Copy', N'Copied content with Id: ''1194'' related to original content with Id: ''1156''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (346, -1, 1195, N'Document', CAST(N'2019-10-09T15:45:59.667' AS DateTime), N'Copy', N'Copied content with Id: ''1195'' related to original content with Id: ''1157''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (347, -1, 1196, N'Document', CAST(N'2019-10-09T15:45:59.670' AS DateTime), N'Copy', N'Copied content with Id: ''1196'' related to original content with Id: ''1162''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (348, -1, 1197, N'Document', CAST(N'2019-10-09T15:45:59.677' AS DateTime), N'Copy', N'Copied content with Id: ''1197'' related to original content with Id: ''1170''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (349, -1, 1198, N'Document', CAST(N'2019-10-09T15:45:59.680' AS DateTime), N'Copy', N'Copied content with Id: ''1198'' related to original content with Id: ''1171''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (350, -1, 1199, N'Document', CAST(N'2019-10-09T15:45:59.687' AS DateTime), N'Copy', N'Copied content with Id: ''1199'' related to original content with Id: ''1172''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (351, -1, 1200, N'Document', CAST(N'2019-10-09T15:45:59.690' AS DateTime), N'Copy', N'Copied content with Id: ''1200'' related to original content with Id: ''1173''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (352, -1, 1201, N'Document', CAST(N'2019-10-09T15:45:59.697' AS DateTime), N'Copy', N'Copied content with Id: ''1201'' related to original content with Id: ''1178''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (353, -1, 1202, N'Document', CAST(N'2019-10-09T15:45:59.700' AS DateTime), N'Copy', N'Copied content with Id: ''1202'' related to original content with Id: ''1180''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (354, -1, 1203, N'Document', CAST(N'2019-10-09T15:45:59.723' AS DateTime), N'Copy', N'Copied content with Id: ''1203'' related to original content with Id: ''1181''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (355, -1, 1204, N'Document', CAST(N'2019-10-09T15:45:59.727' AS DateTime), N'Copy', N'Copied content with Id: ''1204'' related to original content with Id: ''1182''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (356, -1, 1205, N'Document', CAST(N'2019-10-09T15:45:59.733' AS DateTime), N'Copy', N'Copied content with Id: ''1205'' related to original content with Id: ''1183''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (357, -1, 1206, N'Document', CAST(N'2019-10-09T15:45:59.737' AS DateTime), N'Copy', N'Copied content with Id: ''1206'' related to original content with Id: ''1184''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (358, -1, 1207, N'Document', CAST(N'2019-10-09T15:45:59.743' AS DateTime), N'Copy', N'Copied content with Id: ''1207'' related to original content with Id: ''1185''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (359, -1, 1208, N'Document', CAST(N'2019-10-09T15:45:59.747' AS DateTime), N'Copy', N'Copied content with Id: ''1208'' related to original content with Id: ''1186''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (360, -1, 1209, N'Document', CAST(N'2019-10-09T15:45:59.750' AS DateTime), N'Copy', N'Copied content with Id: ''1209'' related to original content with Id: ''1187''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (361, -1, 1210, N'Document', CAST(N'2019-10-09T15:45:59.757' AS DateTime), N'Copy', N'Copied content with Id: ''1210'' related to original content with Id: ''1188''', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (362, -1, 1141, N'Document', CAST(N'2019-10-09T15:47:27.737' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (363, -1, 1189, N'Document', CAST(N'2019-10-09T15:47:33.533' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (364, -1, 1189, N'Document', CAST(N'2019-10-09T15:49:21.210' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (365, -1, 1189, N'Document', CAST(N'2019-10-09T15:49:25.140' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (366, -1, 1189, N'Document', CAST(N'2019-10-09T15:49:43.997' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (367, -1, 1189, N'Document', CAST(N'2019-10-09T15:49:45.507' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (368, -1, 1190, N'Document', CAST(N'2019-10-09T15:50:37.313' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (369, -1, 1191, N'Document', CAST(N'2019-10-09T15:50:43.103' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (370, -1, 1192, N'Document', CAST(N'2019-10-09T15:50:45.493' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (371, -1, 1196, N'Document', CAST(N'2019-10-09T15:50:47.493' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (372, -1, 1197, N'Document', CAST(N'2019-10-09T15:50:49.227' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (373, -1, 1201, N'Document', CAST(N'2019-10-09T15:50:52.137' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (374, -1, 1201, N'Document', CAST(N'2019-10-09T15:54:45.800' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (375, -1, 1197, N'Document', CAST(N'2019-10-09T15:54:52.737' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (376, -1, 1196, N'Document', CAST(N'2019-10-09T15:55:02.673' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (377, -1, 1192, N'Document', CAST(N'2019-10-09T15:55:26.240' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (378, -1, 1211, N'DataType', CAST(N'2019-10-09T16:43:11.140' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (379, -1, 1140, N'DocumentType', CAST(N'2019-10-09T16:43:37.837' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (380, -1, 1141, N'Document', CAST(N'2019-10-09T16:47:01.467' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (381, -1, 1189, N'Document', CAST(N'2019-10-09T16:47:13.847' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (382, -1, 1189, N'Document', CAST(N'2019-10-09T16:47:21.253' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (383, -1, 1141, N'Document', CAST(N'2019-10-09T17:46:39.877' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (384, -1, 1141, N'Document', CAST(N'2019-10-09T20:09:27.423' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (385, -1, 1189, N'Document', CAST(N'2019-10-09T20:09:30.370' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (386, -1, 1212, N'Document', CAST(N'2019-10-09T20:10:06.053' AS DateTime), N'New', N'Content ''quanfx--10/9/2019 8:10:05 PM'' was created with Id 1212', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (387, -1, 1212, N'Document', CAST(N'2019-10-09T20:10:06.250' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (388, -1, 1141, N'Document', CAST(N'2019-10-09T20:58:29.467' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (389, -1, 1197, N'Document', CAST(N'2019-10-09T21:00:33.700' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (390, -1, 1197, N'Document', CAST(N'2019-10-09T21:01:10.317' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (391, -1, 1199, N'Document', CAST(N'2019-10-09T21:01:13.920' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (392, -1, 1200, N'Document', CAST(N'2019-10-09T21:01:17.007' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (393, -1, 1198, N'Document', CAST(N'2019-10-09T21:01:19.460' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (394, -1, 1198, N'Document', CAST(N'2019-10-09T21:03:28.430' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (395, -1, 1199, N'Document', CAST(N'2019-10-09T21:03:36.757' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (396, -1, 1189, N'Document', CAST(N'2019-10-09T21:04:02.883' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (397, -1, 1213, N'Document', CAST(N'2019-10-09T21:29:28.540' AS DateTime), N'New', N'Content ''quanfx--10/9/2019 9:29:27 PM'' was created with Id 1213', NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (398, -1, 1213, N'Document', CAST(N'2019-10-09T21:29:31.430' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (399, -1, 1214, N'Document', CAST(N'2019-10-09T21:30:37.087' AS DateTime), N'New', N'Content ''admin--10/9/2019 9:30:37 PM'' was created with Id 1214', NULL)
GO
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (400, -1, 1214, N'Document', CAST(N'2019-10-09T21:30:37.697' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (401, -1, 1169, N'DocumentType', CAST(N'2019-10-11T13:58:29.137' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (402, -1, 1215, N'Media', CAST(N'2019-10-11T13:59:13.587' AS DateTime), N'Save', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (403, -1, 1216, N'Document', CAST(N'2019-10-11T13:59:24.803' AS DateTime), N'Publish', NULL, NULL)
INSERT [dbo].[umbracoLog] ([id], [userId], [NodeId], [entityType], [Datestamp], [logHeader], [logComment], [parameters]) VALUES (404, -1, 1216, N'Document', CAST(N'2019-10-11T14:43:41.430' AS DateTime), N'Publish', NULL, NULL)
SET IDENTITY_INSERT [dbo].[umbracoLog] OFF
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (25, NULL)
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (26, NULL)
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (27, NULL)
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (28, N'/media/662af6ca411a4c93a6c722c4845698e7/00000006000000000000000000000000/16403439029_f500be349b_o.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (29, N'/media/55514845b8bd487cb3709724852fd6bb/00000006000000000000000000000000/4730684907_8a7f8759cb_b.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (30, N'/media/20e3a8ffad1b4fe9b48cb8461c46d2d0/00000006000000000000000000000000/7371127652_e01b6ab56f_b.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (31, N'/media/1bc5280b8658402789d958e2576e469b/00000006000000000000000000000000/14272036539_469ca21d5c_h.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (32, N'/media/c09ec77f08e3466aac58c979befd3cd6/00000006000000000000000000000000/5852022211_9028df67c0_b.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (33, N'/media/b76ddb4ee603401499066087984740ec/00000006000000000000000000000000/5852022091_87c5d045ab_b.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (34, N'/media/46a025d6095a4b148b961b59ca4e951c/00000006000000000000000000000000/7377957524_347859faac_b.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (35, N'/media/17552d12081d4d01b68132c495d6576f/00000006000000000000000000000000/7373036290_5e8420bf36_b.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (36, N'/media/1d0b713a022a49c8b842a2463c83a553/00000006000000000000000000000000/7373036208_30257976a0_b.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (37, N'/media/cf1ab8dcad0f4a8e974b87b84777b0d6/00000006000000000000000000000000/18720470241_ff77768544_h.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (38, N'/media/eee91c05b2e84031a056dcd7f28eff89/00000006000000000000000000000000/18531852339_981b067419_h.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (39, N'/media/fa763e0d0ceb408c8720365d57e06e32/00000006000000000000000000000000/18531854019_351c579559_h.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (40, N'/media/c0969cab13ab4de9819a848619ac2b5d/00000006000000000000000000000000/18095416144_44a566a5f4_h.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (41, N'/media/34371d0892c84015912ebaacd002c5d0/00000006000000000000000000000000/18530280048_459b8b61b2_h.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (69, N'/media/lnnnp4wz/2bd58947-57fb-4dbd-b202-f5884830bcfb_4c1a0bcd-e53c-4ba9-96a6-f8cbe1935013_2.jpg')
INSERT [dbo].[umbracoMediaVersion] ([id], [path]) VALUES (223, N'/media/hwwde1d1/vn.png')
SET IDENTITY_INSERT [dbo].[umbracoNode] ON 

INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-99, N'8f1ef1e1-9de4-40d3-a072-6673f631ca64', -1, 1, N'-1,-99', 39, 0, -1, N'Label (decimal)', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-98, N'a97cec69-9b71-4c30-8b12-ec398860d7e8', -1, 1, N'-1,-98', 38, 0, -1, N'Label (time)', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-97, N'aa2c52a0-ce87-4e65-a47c-7df09358585d', -1, 1, N'-1,-97', 2, 0, -1, N'List View - Members', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-96, N'3a0156c4-3b8c-4803-bdc1-6871faa83fff', -1, 1, N'-1,-96', 2, 0, -1, N'List View - Media', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-95, N'c0808dd3-8133-4e4b-8ce8-e2bea84a96a4', -1, 1, N'-1,-95', 2, 0, -1, N'List View - Content', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-94, N'0e9794eb-f9b5-4f20-a788-93acd233a7e4', -1, 1, N'-1,-94', 37, 0, -1, N'Label (datetime)', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-93, N'930861bf-e262-4ead-a704-f99453565708', -1, 1, N'-1,-93', 36, 0, -1, N'Label (bigint)', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-92, N'f0bc4bfb-b499-40d6-ba86-058885a5178c', -1, 1, N'-1,-92', 35, 0, -1, N'Label (string)', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-91, N'8e7f995c-bd81-4627-9932-c40e568ec788', -1, 1, N'-1,-91', 36, 0, -1, N'Label (integer)', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-90, N'84c6b441-31df-4ffe-b67e-67d5bc3ae65a', -1, 1, N'-1,-90', 34, 0, -1, N'Upload', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-89, N'c6bac0dd-4ab9-45b1-8e30-e4b619ee5da3', -1, 1, N'-1,-89', 33, 0, -1, N'Textarea', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-88, N'0cc0eba1-9960-42c9-bf9b-60e150b429ae', -1, 1, N'-1,-88', 32, 0, -1, N'Textstring', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-87, N'ca90c950-0aff-4e72-b976-a30b1ac57dad', -1, 1, N'-1,-87', 4, 0, -1, N'Richtext editor', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-51, N'2e6d3631-066e-44b8-aec4-96f09099b2b5', -1, 1, N'-1,-51', 2, 0, -1, N'Numeric', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-49, N'92897bc6-a5f3-4ffe-ae27-f2e7e33dda49', -1, 1, N'-1,-49', 2, 0, -1, N'True/false', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-43, N'fbaf13a8-4036-41f2-93a3-974f678c312a', -1, 1, N'-1,-43', 2, 0, -1, N'Checkbox list', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-42, N'f38f0ac7-1d27-439c-9f3f-089cd8825a53', -1, 1, N'-1,-42', 2, 0, -1, N'Dropdown multiple', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-41, N'5046194e-4237-453c-a547-15db3a07c4e1', -1, 1, N'-1,-41', 2, 0, -1, N'Date Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-40, N'bb5f57c9-ce2b-4bb9-b697-4caca783a805', -1, 1, N'-1,-40', 2, 0, -1, N'Radiobox', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-39, N'0b6a45e7-44ba-430d-9da5-4e46060b9e03', -1, 1, N'-1,-39', 2, 0, -1, N'Dropdown', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-37, N'0225af17-b302-49cb-9176-b9f35cab9c17', -1, 1, N'-1,-37', 2, 0, -1, N'Approved Color', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.783' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-36, N'e4d66c0f-b935-4200-81f0-025f7256b89a', -1, 1, N'-1,-36', 2, 0, -1, N'Date Picker with time', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-21, N'bf7c7cbc-952f-4518-97a2-69e9c7b33842', -1, 0, N'-1,-21', 0, 0, -1, N'Recycle Bin', N'cf3d8e34-1c1c-41e9-ae56-878b57b32113', CAST(N'2019-10-03T17:05:35.780' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-20, N'0f582a79-1e41-4cf0-bfa0-76340651891a', -1, 0, N'-1,-20', 0, 0, -1, N'Recycle Bin', N'01bb7ff2-24dc-4c0c-95a2-c24ef72bbac8', CAST(N'2019-10-03T17:05:35.780' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (-1, N'916724a5-173d-4619-b97e-b9de133dd6f5', -1, 0, N'-1', 0, 0, -1, N'SYSTEM DATA: umbraco master root', N'ea7d8624-4cfe-4578-a871-24aa946bf34d', CAST(N'2019-10-03T17:05:35.777' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1031, N'f38bd2d7-65d0-48e6-95dc-87ce06ec2d3d', -1, 1, N'-1,1031', 2, 0, -1, N'Folder', N'4ea4382b-2f5a-4c2b-9587-ae9b3cf3602e', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1032, N'cc07b313-0843-4aa8-bbda-871c8da728c8', -1, 1, N'-1,1032', 2, 0, -1, N'Image', N'4ea4382b-2f5a-4c2b-9587-ae9b3cf3602e', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1033, N'4c52d8ab-54e6-40cd-999c-7a5f24903e4d', -1, 1, N'-1,1033', 2, 0, -1, N'File', N'4ea4382b-2f5a-4c2b-9587-ae9b3cf3602e', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1041, N'b6b73142-b9c1-4bf8-a16d-e1c23320b549', -1, 1, N'-1,1041', 2, 0, -1, N'Tags', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1043, N'1df9f033-e6d4-451f-b8d2-e0cbc50a836f', -1, 1, N'-1,1043', 2, 0, -1, N'Image Cropper', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1044, N'd59be02f-1df9-4228-aa1e-01917d806cda', -1, 1, N'-1,1044', 0, 0, -1, N'Member', N'9b5416fb-e72f-45a9-a07b-5a9a2709ce43', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1046, N'fd1e0da5-5606-4862-b679-5d0cf3a52a59', -1, 1, N'-1,1046', 2, 0, -1, N'Content Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1047, N'1ea2e01f-ebd8-4ce1-8d71-6b1149e63548', -1, 1, N'-1,1047', 2, 0, -1, N'Member Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1048, N'135d60e0-64d9-49ed-ab08-893c9ba44ae5', -1, 1, N'-1,1048', 2, 0, -1, N'Media Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1049, N'9dbbcbbb-2327-434a-b355-af1b84e5010a', -1, 1, N'-1,1049', 2, 0, -1, N'Multiple Media Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1050, N'b4e3535a-1753-47e2-8568-602cf8cfee6f', -1, 1, N'-1,1050', 2, 0, -1, N'Multi URL Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:35.790' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1051, N'3ece86aa-61ad-45d5-b1dd-8f02f25df949', -1, 1, N'-1,1051', 29, 0, -1, N'Home - Call To Action Link - Content Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.657' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1052, N'71548df5-836c-41f8-96ac-5b98fe5e2e19', -1, 1, N'-1,1052', 30, 0, -1, N'Home - Color Theme - Radio button list', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.677' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1053, N'34a7f8f5-a84e-439b-9322-466ff7ed8866', -1, 1, N'-1,1053', 31, 0, -1, N'Home - Content - Grid layout', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.683' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1054, N'fff3c360-3f90-45b3-a554-e29ca72cdce4', -1, 1, N'-1,1054', 32, 0, -1, N'Home - Font - Radio button list', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.690' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1055, N'519eaa41-b905-4371-aa7c-40c9b3946f66', -1, 1, N'-1,1055', 33, 0, -1, N'Home - Logo - Media Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.693' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1056, N'9d5ba2c5-ed7a-41f8-b454-9fc65f48752e', -1, 1, N'-1,1056', 34, 0, -1, N'Blog - How many posts should be shown - Slider', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.697' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1057, N'9d0726e6-5fa7-4455-8fe6-ed5bf9057cc4', -1, 1, N'-1,1057', 35, 0, -1, N'Blogpost - Categories - Tags', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.700' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1058, N'59accdfb-e0e7-4ff2-b8ab-4822b78d2c64', -1, 1, N'-1,1058', 36, 0, -1, N'Contact - Pick a Contact Form - Form Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.703' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1059, N'ecbbac0d-974e-490c-b533-fead926dc525', -1, 1, N'-1,1059', 37, 0, -1, N'Contact - Contact Intro - Rich Text Editor', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.710' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1060, N'ea0ed4c1-c489-43b8-9058-a70babf430ff', -1, 1, N'-1,1060', 38, 0, -1, N'Content Base - Content - Grid layout', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.713' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1061, N'4885450e-a60f-42bb-984a-43988baf5283', -1, 1, N'-1,1061', 39, 0, -1, N'People - Featured People - Multinode Treepicker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.713' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1062, N'b58c1530-91c9-4c83-aa35-032aca0f7b89', -1, 1, N'-1,1062', 40, 0, -1, N'Person - Department - Tags', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.720' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1063, N'e26a8d91-a9d7-475b-bc3b-2a09f4743754', -1, 1, N'-1,1063', 41, 0, -1, N'Person - Photo - Media Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.720' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1064, N'59ac0a33-9d34-4236-9886-99310d13d7f1', -1, 1, N'-1,1064', 42, 0, -1, N'Product - Category - Tags', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.723' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1065, N'30c35221-91b0-49a5-a599-42d1965161ea', -1, 1, N'-1,1065', 43, 0, -1, N'Navigation Base - Keywords - Tags', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.727' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1066, N'84bbeb67-df7e-4043-8db8-55df52d01456', -1, 1, N'-1,1066', 44, 0, -1, N'Product - Features - Nested Content', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.730' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1067, N'3ada047c-6dfc-4a14-afc4-7efb79390f92', -1, 1, N'-1,1067', 45, 0, -1, N'Product - Photos - Media Picker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.733' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1068, N'3f6619b6-08a4-4be7-8d6a-2761844ee567', -1, 1, N'-1,1068', 46, 0, -1, N'Product - Price - Decimal', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.737' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1069, N'28a6aeda-b3fc-4c4d-926e-607854a07891', -1, 1, N'-1,1069', 47, 0, -1, N'Products - Default Currency - Dropdown list', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.737' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1070, N'd0382188-119b-49b7-86d3-84119a02645f', -1, 1, N'-1,1070', 48, 0, -1, N'Products - Featured Products - Multinode Treepicker', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.740' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1071, N'fc1475d2-7c7b-4b2f-bc53-54c86fd43d6c', -1, 1, N'-1,1071', 49, 0, -1, N'Contact - Map Coordinates - Open street maps', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-03T17:05:59.747' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1119, N'b6f11172-373f-4473-af0f-0b0e5aefd21c', -21, 1, N'-1,-21,1119', 0, 1, -1, N'Design', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.687' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1120, N'1fd2ecaf-f371-4c00-9306-867fa4585e7a', -21, 1, N'-1,-21,1120', 0, 1, -1, N'People', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.717' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1121, N'6d5bf746-cb82-45c5-bd15-dd3798209b87', -21, 1, N'-1,-21,1121', 0, 1, -1, N'Products', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.730' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1122, N'662af6ca-411a-4c93-a6c7-22c4845698e7', 1119, 2, N'-1,-21,1119,1122', 0, 1, -1, N'Umbraco Campari Meeting Room', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.760' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1123, N'55514845-b8bd-487c-b370-9724852fd6bb', 1121, 2, N'-1,-21,1121,1123', 0, 1, -1, N'Biker Jacket', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.773' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1124, N'20e3a8ff-ad1b-4fe9-b48c-b8461c46d2d0', 1121, 2, N'-1,-21,1121,1124', 0, 1, -1, N'Tattoo', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.787' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1125, N'1bc5280b-8658-4027-89d9-58e2576e469b', 1121, 2, N'-1,-21,1121,1125', 0, 1, -1, N'Unicorn', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.793' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1126, N'c09ec77f-08e3-466a-ac58-c979befd3cd6', 1121, 2, N'-1,-21,1121,1126', 0, 1, -1, N'Ping Pong Ball', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.807' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1127, N'b76ddb4e-e603-4014-9906-6087984740ec', 1121, 2, N'-1,-21,1121,1127', 0, 1, -1, N'Bowling Ball', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.813' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1128, N'46a025d6-095a-4b14-8b96-1b59ca4e951c', 1121, 2, N'-1,-21,1121,1128', 0, 1, -1, N'Jumpsuit', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.823' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1129, N'17552d12-081d-4d01-b681-32c495d6576f', 1121, 2, N'-1,-21,1121,1129', 0, 1, -1, N'Banjo', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.833' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1130, N'1d0b713a-022a-49c8-b842-a2463c83a553', 1121, 2, N'-1,-21,1121,1130', 0, 1, -1, N'Knitted West', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.843' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1131, N'cf1ab8dc-ad0f-4a8e-974b-87b84777b0d6', 1120, 2, N'-1,-21,1120,1131', 0, 1, -1, N'Jan Skovgaard', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.880' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1132, N'eee91c05-b2e8-4031-a056-dcd7f28eff89', 1120, 2, N'-1,-21,1120,1132', 0, 1, -1, N'Matt Brailsford', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.890' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1133, N'fa763e0d-0ceb-408c-8720-365d57e06e32', 1120, 2, N'-1,-21,1120,1133', 0, 1, -1, N'Lee Kelleher', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.900' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1134, N'c0969cab-13ab-4de9-819a-848619ac2b5d', 1120, 2, N'-1,-21,1120,1134', 0, 1, -1, N'Jeavon Leopold', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.907' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1135, N'34371d08-92c8-4015-912e-baacd002c5d0', 1120, 2, N'-1,-21,1120,1135', 0, 1, -1, N'Jeroen Breuer', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:06:01.917' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1137, N'fac75b5d-16d9-4a3c-854f-2f6cea807908', 1119, 2, N'-1,-21,1119,1137', 0, 1, -1, N'2Bd58947 57Fb 4Dbd B202 F5884830bcfb 4C1a0bcd E53c 4Ba9 96A6 F8cbe1935013 2', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-03T17:20:19.670' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1138, N'f81ac86c-7adc-44a4-a5f0-574b8669b505', -1, 1, N'-1,1138', 0, 0, NULL, N'WebbaseTemplate', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-04T11:44:59.240' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1139, N'02435d2f-83e6-4a7b-9c54-49336658f3c8', 1138, 1, N'-1,1138,1139', 0, 0, NULL, N'Home', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-04T11:45:21.747' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1140, N'179c7123-c392-48ba-94b2-689eadb5665b', -1, 1, N'-1,1140', 0, 0, -1, N'Home', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-04T11:46:24.263' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1141, N'1eb223fc-2240-4817-9067-593659dbe988', -1, 1, N'-1,1141', 0, 0, -1, N'EN', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-04T11:49:53.607' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1143, N'99eb327f-2e48-47c4-b24b-0eaa3af93ae6', 1138, 1, N'-1,1138,1143', 0, 0, NULL, N'Generic', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-04T14:32:02.620' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1144, N'bfaedd9b-6f50-4c2f-add2-b6129944e6e9', 1138, 1, N'-1,1138,1144', 0, 0, NULL, N'Elements', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-04T14:32:42.293' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1145, N'760f7dab-66e9-408d-9016-8579da8b062b', -1, 1, N'-1,1145', 1, 0, -1, N'Generic', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-04T14:33:05.257' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1146, N'8887ca2c-1d22-4c66-89e6-02178a5b4062', -1, 1, N'-1,1146', 2, 0, -1, N'Elements', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-04T14:33:58.717' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1147, N'5bde5feb-07d3-4cad-a87b-a70ffd3529f8', -20, 1, N'-1,-20,1147', 0, 1, -1, N'Generic', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-04T14:35:00.327' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1148, N'8f97b92e-2706-4487-870e-c0a5da5b84da', 1141, 2, N'-1,1141,1148', 0, 0, -1, N'Generic', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-04T14:36:12.630' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1149, N'0321f2e2-be8e-445f-93e9-9e277301ea8d', 1141, 2, N'-1,1141,1149', 1, 0, -1, N'Elements', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-04T14:36:27.350' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1150, N'b8816a2c-f2ac-457d-8acf-7822ca803290', -20, 1, N'-1,-20,1150', 1, 1, -1, N'TestForNewContent', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-07T10:28:16.107' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1151, N'9a7a5438-f3e1-48bd-b2ea-92570d461d7e', 1138, 1, N'-1,1138,1151', 0, 0, NULL, N'About us', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-07T10:32:24.247' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1152, N'c202a07c-c556-4f7e-9952-abae546821c8', -1, 1, N'-1,1152', 3, 0, -1, N'About us', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-07T10:32:54.777' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1153, N'05b3b59b-2bf3-41ee-a71b-3ea1e8258073', -1, 1, N'-1,1153', 4, 0, -1, N'Company Overview', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-07T10:34:39.063' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1154, N'c0b8ab1e-2019-4e39-b558-ad8e665bd024', 1141, 2, N'-1,1141,1154', 2, 0, -1, N'About us', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-07T10:36:36.790' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1155, N'ecc7e4d4-a051-493b-a094-13271f542170', 1154, 3, N'-1,1141,1154,1155', 0, 0, -1, N'Company Overview', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-07T10:37:06.953' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1156, N'f89cfbde-53e0-4045-bef1-d88e1a6cb43a', 1154, 3, N'-1,1141,1154,1156', 1, 0, -1, N'Privacy', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-07T11:04:45.690' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1157, N'5b3f3622-3a56-414f-becd-5bbc597fb1e2', 1154, 3, N'-1,1141,1154,1157', 2, 0, -1, N'Disclamer', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-07T11:05:23.800' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1158, N'f9f88932-ac36-40a9-a1b2-037f58ce2812', -1, 1, N'-1,1158', 0, 0, -1, N'Custom Types', N'2f7a2769-6b0b-4468-90dd-af42d64f7f16', CAST(N'2019-10-07T11:30:43.163' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1159, N'bb109839-0c0f-4be8-b233-4b643a47cbbb', -1, 1, N'-1,1159', 50, 0, -1, N'Introduction Control - Introduction - Rich Text Editor', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-07T11:37:28.883' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1160, N'ba9ae19a-3659-4665-afe7-fd0f227765d2', 1158, 2, N'-1,1158,1160', 0, 0, -1, N'Introduction Control', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-07T11:38:45.760' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1161, N'e31329c8-9bcd-4ca2-bc35-fd5343c63479', -1, 1, N'-1,1161', 51, 0, -1, N'Home - Hello Banner - File upload', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-07T13:51:04.340' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1162, N'1cadbbde-6b42-4cbb-ada6-1954724c1644', 1141, 2, N'-1,1141,1162', 3, 0, -1, N'Welcome', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-07T22:00:29.827' AS DateTime))
GO
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1163, N'843ec027-e8c5-4c0f-8f7f-4ffaeb1ab886', -1, 1, N'-1,1163', 0, 0, NULL, N'HidePageFromNavigation', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-07T22:18:01.520' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1164, N'1bad8559-7ec8-4a96-b022-41cd0ef5b800', 1158, 2, N'-1,1158,1164', 1, 0, -1, N'HidePageFromNavigation', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-07T22:18:01.583' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1165, N'2ef2260d-1260-48a6-8812-41fad6dfc5fd', -1, 1, N'-1,1165', 52, 0, -1, N'HidePageFromNavigation - Textbox', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-07T23:24:11.147' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1166, N'd59e1553-65b3-4e99-9625-7faaf01e8852', 1138, 1, N'-1,1138,1166', 0, 0, NULL, N'News', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-08T09:43:58.997' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1167, N'1e052886-8e4b-4e5e-843c-4d7117d207ac', -1, 1, N'-1,1167', 5, 0, -1, N'News', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-08T09:43:59.047' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1168, N'83417a09-7758-4b74-854b-9d0fcf320643', 1138, 1, N'-1,1138,1168', 0, 0, NULL, N'News Detail', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-08T09:48:03.480' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1169, N'5ec11022-940d-49be-8b1c-531c80eb7cd6', 1167, 2, N'-1,1167,1169', 0, 0, -1, N'News Detail', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-08T09:48:03.510' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1170, N'41acea12-9ea2-40b6-99d9-709ccb4e99d9', 1141, 2, N'-1,1141,1170', 4, 0, -1, N'News', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-08T09:51:13.083' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1171, N'51fcf072-383a-45c4-ad46-de193f107e8d', 1170, 3, N'-1,1141,1170,1171', 0, 0, -1, N'News 1', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-08T09:52:57.620' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1172, N'9a541a5d-f38a-4ed5-8a04-5d3fa3b023ad', 1170, 3, N'-1,1141,1170,1172', 1, 0, -1, N'News 2', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-08T09:54:20.113' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1173, N'd99214c6-21de-46d7-95eb-1425feaa1106', 1170, 3, N'-1,1141,1170,1173', 2, 0, -1, N'News 3', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-08T09:55:37.663' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1174, N'0382b6d2-6a01-4e6b-887c-8f5d2774ca8f', 1138, 1, N'-1,1138,1174', 0, 0, NULL, N'Contact', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-09T09:32:46.250' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1175, N'65b19872-bd74-45f9-ab44-e1e277f203b1', -1, 1, N'-1,1175', 6, 0, -1, N'Contact', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-09T09:32:46.307' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1176, N'8645daa1-1398-49df-8502-ad82f0972730', 1138, 1, N'-1,1138,1176', 0, 0, NULL, N'Contact Content', N'6fbde604-4178-42ce-a10b-8a2600a2f07d', CAST(N'2019-10-09T09:33:00.467' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1177, N'6bfadd3e-c195-4ef6-97ff-e4c93256b698', -1, 1, N'-1,1177', 7, 0, -1, N'Contact Content', N'a2cb7800-f571-4787-9638-bc48539a0efb', CAST(N'2019-10-09T09:33:00.480' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1178, N'3e86294b-20bf-438f-a1d3-c2a4042d4abc', 1141, 2, N'-1,1141,1178', 5, 0, -1, N'Contact Us', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T09:36:03.423' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1179, N'b57f7914-f540-4d9e-b35c-4143bcc46fd5', -20, 1, N'-1,-20,1179', 2, 1, -1, N'quanle-2907', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T09:37:41.443' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1180, N'8236a71a-6c53-4c08-97ad-f2d010cd914e', 1178, 3, N'-1,1141,1178,1180', 0, 0, -1, N'quan--10/9/2019 11:59:07 AM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T11:59:07.870' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1181, N'a2f1c196-97d9-455e-99f7-9bca9c231883', 1178, 3, N'-1,1141,1178,1181', 1, 0, -1, N'quanfx--10/9/2019 12:00:39 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T12:00:39.273' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1182, N'732730c6-9712-480c-9c67-5b4be95dfd10', 1178, 3, N'-1,1141,1178,1182', 2, 0, -1, N'adminne--10/9/2019 1:43:08 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T13:43:09.127' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1183, N'9de8fd42-31c5-4b4a-9261-5663e17df281', 1178, 3, N'-1,1141,1178,1183', 3, 0, -1, N'Nhi--10/9/2019 1:51:19 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T13:51:19.383' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1184, N'29249b74-cdec-4eca-8871-9a3317025b0f', 1178, 3, N'-1,1141,1178,1184', 4, 0, -1, N'Lux A2.0--10/9/2019 2:39:06 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T14:39:07.113' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1185, N'c791562c-28fb-4905-a531-35fbfd7bb65c', 1178, 3, N'-1,1141,1178,1185', 5, 0, -1, N'Lux A2.0--10/9/2019 2:49:32 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T14:49:32.347' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1186, N'6479c5e3-aee9-4479-97c8-aa3e0bfd80bb', 1178, 3, N'-1,1141,1178,1186', 6, 0, -1, N'Lux A2.0--10/9/2019 2:50:04 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T14:50:04.937' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1187, N'8407a4a7-91a5-4819-9b56-92044f2ea1f3', 1178, 3, N'-1,1141,1178,1187', 7, 0, -1, N'quanfx--10/9/2019 2:50:46 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T14:50:46.417' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1188, N'c44221dd-0144-4d1e-9b80-4d473d547b7d', 1178, 3, N'-1,1141,1178,1188', 8, 0, -1, N'Product--10/9/2019 3:33:13 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:33:13.470' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1189, N'bbcd6ab5-3f3a-4200-9a85-b91b6c08352f', -1, 1, N'-1,1189', 1, 0, -1, N'VN', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.120' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1190, N'16012a4d-18cd-49fa-96a6-c50fd7befde6', 1189, 2, N'-1,1189,1190', 0, 0, -1, N'Generic', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.207' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1191, N'730e353e-266c-4b02-ba6f-7962d966300d', 1189, 2, N'-1,1189,1191', 1, 0, -1, N'Elements', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.213' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1192, N'f7acdadb-8e68-4577-86d3-4f9e7a2c9385', 1189, 2, N'-1,1189,1192', 2, 0, -1, N'Giới thiệu về công ty', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.213' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1193, N'a4cc7235-2426-4df3-8a98-b21ea216e1b7', 1192, 3, N'-1,1189,1192,1193', 0, 0, -1, N'Company Overview', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.217' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1194, N'22f4e236-e59d-4a25-ae2e-b2e7008ec0ff', 1192, 3, N'-1,1189,1192,1194', 1, 0, -1, N'Privacy', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.220' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1195, N'c5d196df-be13-4a6e-a42e-f134c5572f93', 1192, 3, N'-1,1189,1192,1195', 2, 0, -1, N'Disclamer', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.223' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1196, N'f6aec346-3f17-4354-9a9e-1da572fc1f6c', 1189, 2, N'-1,1189,1196', 3, 0, -1, N'Chào mừng', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.227' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1197, N'c7d5475d-7bfc-4315-bc61-3299894eb69c', 1189, 2, N'-1,1189,1197', 4, 0, -1, N'Tin tức', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.230' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1198, N'46c26d34-50b9-45fe-ac25-4e92ed9d942d', 1197, 3, N'-1,1189,1197,1198', 0, 0, -1, N'Tin tuc 1', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.233' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1199, N'5fa0207b-b188-4eca-939d-2b9bbb39da59', 1197, 3, N'-1,1189,1197,1199', 1, 0, -1, N'Tin tuc 2', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.233' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1200, N'5b3e605d-36ab-490f-a95f-16f73813be7c', 1197, 3, N'-1,1189,1197,1200', 2, 0, -1, N'News 3', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.240' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1201, N'c1ecc8c9-e06e-4174-b3d6-45ebb0d5e459', 1189, 2, N'-1,1189,1201', 5, 0, -1, N'Liên hệ', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.243' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1202, N'63ebe790-d816-4335-94c1-83910b1b3286', 1201, 3, N'-1,1189,1201,1202', 0, 0, -1, N'quan--10/9/2019 11:59:07 AM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.247' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1203, N'da3ee125-0c37-413c-aab8-0d6622edf1b4', 1201, 3, N'-1,1189,1201,1203', 1, 0, -1, N'quanfx--10/9/2019 12:00:39 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.250' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1204, N'f3a072ec-0427-4a94-be9c-274568badab0', 1201, 3, N'-1,1189,1201,1204', 2, 0, -1, N'adminne--10/9/2019 1:43:08 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.253' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1205, N'b17b2aad-02a3-4192-af4b-299563f672ef', 1201, 3, N'-1,1189,1201,1205', 3, 0, -1, N'Nhi--10/9/2019 1:51:19 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.257' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1206, N'fa68424b-a3c9-480f-8218-5c702494f1bb', 1201, 3, N'-1,1189,1201,1206', 4, 0, -1, N'Lux A2.0--10/9/2019 2:39:06 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.260' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1207, N'a4c0031f-8316-4bc3-868c-69a6f66eee83', 1201, 3, N'-1,1189,1201,1207', 5, 0, -1, N'Lux A2.0--10/9/2019 2:49:32 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.263' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1208, N'2a291312-1953-443c-a248-f9240416b45e', 1201, 3, N'-1,1189,1201,1208', 6, 0, -1, N'Lux A2.0--10/9/2019 2:50:04 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.267' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1209, N'7bca936c-0298-44c0-b211-ee132341f5f4', 1201, 3, N'-1,1189,1201,1209', 7, 0, -1, N'quanfx--10/9/2019 2:50:46 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.273' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1210, N'798bf4c3-e241-4e98-b1d1-9a2e65de9aae', 1201, 3, N'-1,1189,1201,1210', 8, 0, -1, N'Product--10/9/2019 3:33:13 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T15:45:59.277' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1211, N'f5074389-09b6-4318-b6ea-a3a7933ed650', -1, 1, N'-1,1211', 53, 0, -1, N'Home - Language fla - File upload', N'30a2a501-1978-4ddb-a57b-f7efed43ba3c', CAST(N'2019-10-09T16:43:11.100' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1212, N'0c7d64c5-866e-41b5-95d5-4a5cecf71c2a', 1178, 3, N'-1,1141,1178,1212', 9, 0, -1, N'quanfx--10/9/2019 8:10:05 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T20:10:06.017' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1213, N'ca74dd8b-2a7d-466d-be29-53c1b00915e4', 1178, 3, N'-1,1141,1178,1213', 10, 0, -1, N'quanfx--10/9/2019 9:29:27 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T21:29:28.350' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1214, N'3fe22e20-112b-4d50-8aaa-b328e86e6049', 1201, 3, N'-1,1189,1201,1214', 9, 0, -1, N'admin--10/9/2019 9:30:37 PM', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-09T21:30:37.060' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1215, N'6c32ad3b-197b-4ede-a3ef-a7b934bb1561', -1, 1, N'-1,1215', 0, 0, -1, N'Vn', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', CAST(N'2019-10-11T13:59:13.520' AS DateTime))
INSERT [dbo].[umbracoNode] ([id], [uniqueId], [parentId], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [nodeObjectType], [createDate]) VALUES (1216, N'45d837a0-7ec4-48fe-9818-51aeb46ba2b7', 1170, 3, N'-1,1141,1170,1216', 0, 0, -1, N'New4', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', CAST(N'2019-10-11T13:59:24.763' AS DateTime))
SET IDENTITY_INSERT [dbo].[umbracoNode] OFF
SET IDENTITY_INSERT [dbo].[umbracoPropertyData] ON 

INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (900, 37, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/cf1ab8dcad0f4a8e974b87b84777b0d6/00000006000000000000000000000000/18720470241_ff77768544_h.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (901, 38, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/eee91c05b2e84031a056dcd7f28eff89/00000006000000000000000000000000/18531852339_981b067419_h.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (902, 39, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/fa763e0d0ceb408c8720365d57e06e32/00000006000000000000000000000000/18531854019_351c579559_h.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (903, 40, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/c0969cab13ab4de9819a848619ac2b5d/00000006000000000000000000000000/18095416144_44a566a5f4_h.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (904, 41, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/34371d0892c84015912ebaacd002c5d0/00000006000000000000000000000000/18530280048_459b8b61b2_h.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (905, 29, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/55514845b8bd487cb3709724852fd6bb/00000006000000000000000000000000/4730684907_8a7f8759cb_b.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (906, 30, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/20e3a8ffad1b4fe9b48cb8461c46d2d0/00000006000000000000000000000000/7371127652_e01b6ab56f_b.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (907, 31, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/1bc5280b8658402789d958e2576e469b/00000006000000000000000000000000/14272036539_469ca21d5c_h.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (908, 32, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/c09ec77f08e3466aac58c979befd3cd6/00000006000000000000000000000000/5852022211_9028df67c0_b.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (909, 33, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/b76ddb4ee603401499066087984740ec/00000006000000000000000000000000/5852022091_87c5d045ab_b.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (910, 34, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/46a025d6095a4b148b961b59ca4e951c/00000006000000000000000000000000/7377957524_347859faac_b.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (911, 35, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/17552d12081d4d01b68132c495d6576f/00000006000000000000000000000000/7373036290_5e8420bf36_b.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (912, 36, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/1d0b713a022a49c8b842a2463c83a553/00000006000000000000000000000000/7373036208_30257976a0_b.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (913, 28, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/662af6ca411a4c93a6c722c4845698e7/00000006000000000000000000000000/16403439029_f500be349b_o.jpg","focalPoint":null,"crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (914, 69, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/lnnnp4wz/2bd58947-57fb-4dbd-b202-f5884830bcfb_4c1a0bcd-e53c-4ba9-96a6-f8cbe1935013_2.jpg","crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (915, 69, 7, NULL, NULL, 1920, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (916, 69, 8, NULL, NULL, 1256, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (917, 69, 9, NULL, NULL, NULL, NULL, NULL, N'77482', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (918, 69, 10, NULL, NULL, NULL, NULL, NULL, N'jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (919, 77, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED </span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (921, 88, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED </span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (923, 89, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (925, 90, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (927, 90, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/5be11441-0d50-40a0-9041-1843a1e4d8f4_home-bg.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (929, 91, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (931, 92, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (933, 92, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/b0ff5512-f2b2-479b-b849-c3d6b65b61ab_download.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (935, 93, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (937, 94, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (939, 94, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (941, 95, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (943, 95, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (945, 95, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (947, 95, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (949, 95, 41, NULL, NULL, NULL, NULL, NULL, N'https://www.google.com/', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (951, 96, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (953, 96, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (955, 96, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (957, 96, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (959, 96, 41, NULL, NULL, NULL, NULL, NULL, N'https://www.google.com/', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (961, 97, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (963, 97, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (965, 97, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (967, 97, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (969, 97, 41, NULL, NULL, NULL, NULL, NULL, N'https://www.google.com/', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (971, 98, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (973, 98, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (975, 98, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (977, 98, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (979, 98, 41, NULL, NULL, NULL, NULL, NULL, N'http://localhost:38083/welcome/', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (991, 101, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (993, 101, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (995, 101, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (997, 101, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (999, 101, 41, NULL, NULL, NULL, NULL, NULL, N'https://www.google.com/', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1006, 102, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1008, 102, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1010, 102, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1012, 102, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1014, 102, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1016, 103, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1018, 103, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1020, 103, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1022, 103, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1024, 103, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1026, 103, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1028, 104, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1030, 104, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1032, 104, 43, NULL, NULL, NULL, NULL, NULL, N'/about', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1034, 104, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1036, 104, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1038, 104, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1040, 104, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1042, 104, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1044, 105, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1046, 105, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1048, 105, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1050, 105, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1052, 105, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1054, 105, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1056, 105, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1058, 105, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1072, 112, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1074, 106, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1076, 106, 38, NULL, NULL, NULL, NULL, NULL, N'/media/r4ajm0g1/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1078, 106, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1080, 106, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1082, 106, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1084, 106, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1086, 106, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1088, 106, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1090, 115, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is the news page which contains the list of news in the system should be display here.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1091, 116, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is the news page which contains the list of news in the system should be display here.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1092, 117, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1094, 117, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>News 1</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1096, 118, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1098, 118, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>News 1</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1100, 100, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1101, 120, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1102, 81, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1103, 121, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1104, 113, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1105, 122, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1106, 123, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1108, 123, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Content of news 2</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1110, 119, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1112, 119, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Content of News 1</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1114, 126, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1116, 126, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is content of news 3</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1118, 125, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1120, 125, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Content of News 1</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1122, 124, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1124, 124, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Content of news 2</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1126, 127, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1128, 127, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is content of news 3</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1130, 128, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1132, 128, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Content of News 1</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1134, 131, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1136, 131, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Content of News 1</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1138, 132, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1140, 132, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1142, 129, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1144, 129, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1146, 130, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1148, 130, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1150, 135, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1151, 136, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1152, 135, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1153, 136, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1154, 134, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1156, 134, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1158, 133, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1159, 138, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1160, 133, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1161, 138, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1162, 137, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1163, 139, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1164, 137, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1165, 139, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1166, 142, 50, NULL, NULL, NULL, NULL, NULL, N'Quan Le', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1168, 142, 51, NULL, NULL, NULL, NULL, NULL, N'quanle@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1170, 142, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaaaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1172, 143, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1174, 143, 50, NULL, NULL, NULL, NULL, NULL, N'Quan Le', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1176, 143, 51, NULL, NULL, NULL, NULL, NULL, N'quanle@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1178, 143, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaaaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1180, 144, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1182, 144, 50, NULL, NULL, NULL, NULL, NULL, N'Quan Le', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1184, 144, 51, NULL, NULL, NULL, NULL, NULL, N'quanle@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1186, 144, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaaaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1188, 146, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1189, 146, 50, NULL, NULL, NULL, NULL, NULL, N'Quan Le', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1190, 146, 51, NULL, NULL, NULL, NULL, NULL, N'quanle@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1191, 146, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaaaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1192, 147, 50, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1194, 147, 51, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1196, 147, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'quan')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1198, 149, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1200, 149, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1202, 149, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'Xin chào các bạn, 
Mình tên là quân.
HCM')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1204, 151, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1205, 152, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1206, 151, 50, NULL, NULL, NULL, NULL, NULL, N'adminne', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1207, 152, 50, NULL, NULL, NULL, NULL, NULL, N'adminne', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1208, 151, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1209, 152, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1210, 151, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaaasssss')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1211, 152, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaaasssss')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1212, 148, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1213, 153, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1214, 148, 50, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1215, 153, 50, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1216, 148, 51, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1217, 153, 51, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1218, 148, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'quan')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1219, 153, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'quan')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1220, 150, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1221, 154, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1222, 150, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1223, 154, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1224, 150, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1225, 154, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1226, 150, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'Xin chào các bạn, 
Mình tên là quân.
HCM')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1227, 154, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'Xin chào các bạn, 
Mình tên là quân.
HCM')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1228, 155, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1229, 156, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1230, 155, 50, NULL, NULL, NULL, NULL, NULL, N'Nhi', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1231, 156, 50, NULL, NULL, NULL, NULL, NULL, N'Nhi', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1232, 155, 51, NULL, NULL, NULL, NULL, NULL, N'quanfoxy@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1233, 156, 51, NULL, NULL, NULL, NULL, NULL, N'quanfoxy@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1234, 155, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'hahahaahah')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1235, 156, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'hahahaahah')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1236, 157, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1237, 158, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1238, 157, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1239, 158, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1240, 157, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1241, 158, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1242, 157, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaa')
GO
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1243, 158, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1244, 159, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1245, 160, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1246, 159, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1247, 160, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1248, 159, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1249, 160, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1250, 159, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1251, 160, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1252, 161, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1253, 162, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1254, 161, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1255, 162, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1256, 161, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1257, 162, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1258, 161, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'welcome to my chanel')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1259, 162, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'welcome to my chanel')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1260, 163, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1261, 164, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1262, 163, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1263, 164, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1264, 163, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1265, 164, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1266, 163, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'welcome to my chanel')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1267, 164, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'welcome to my chanel')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1268, 165, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1269, 166, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1270, 165, 50, NULL, NULL, NULL, NULL, NULL, N'Product', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1271, 166, 50, NULL, NULL, NULL, NULL, NULL, N'Product', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1272, 165, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1273, 166, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1274, 165, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'second way to send mail')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1275, 166, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'second way to send mail')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1294, 180, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1295, 180, 50, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1296, 180, 51, NULL, NULL, NULL, NULL, NULL, N'quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1297, 180, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'quan')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1298, 181, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1299, 181, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1300, 181, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1301, 181, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'Xin chào các bạn, 
Mình tên là quân.
HCM')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1302, 182, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1303, 182, 50, NULL, NULL, NULL, NULL, NULL, N'adminne', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1304, 182, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1305, 182, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaaasssss')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1306, 183, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1307, 183, 50, NULL, NULL, NULL, NULL, NULL, N'Nhi', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1308, 183, 51, NULL, NULL, NULL, NULL, NULL, N'quanfoxy@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1309, 183, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'hahahaahah')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1310, 184, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1311, 184, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1312, 184, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1313, 184, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1314, 185, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1315, 185, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1316, 185, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1317, 185, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'aaa')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1318, 186, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1319, 186, 50, NULL, NULL, NULL, NULL, NULL, N'Lux A2.0', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1320, 186, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1321, 186, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'welcome to my chanel')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1322, 187, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1323, 187, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1324, 187, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1325, 187, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'welcome to my chanel')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1326, 188, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1327, 188, 50, NULL, NULL, NULL, NULL, NULL, N'Product', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1328, 188, 51, NULL, NULL, NULL, NULL, NULL, N'quanfx98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1329, 188, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'second way to send mail')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1338, 114, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1340, 114, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1342, 114, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1344, 114, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1346, 114, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1348, 114, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1350, 114, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1352, 114, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1354, 167, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1356, 167, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1358, 167, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1360, 167, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1362, 167, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1364, 167, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1366, 167, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1368, 167, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1370, 190, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1372, 190, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1374, 190, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1376, 190, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1378, 190, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1380, 190, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1382, 190, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1384, 190, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1386, 191, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1388, 191, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1390, 191, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1392, 191, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1394, 191, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1396, 191, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1398, 191, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
GO
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1400, 191, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1402, 168, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1403, 193, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1404, 170, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1406, 174, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1408, 175, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is the news page which contains the list of news in the system should be display here.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1410, 197, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is the news page which contains the list of news in the system should be display here.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1412, 196, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1413, 201, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1414, 195, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1415, 202, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1416, 189, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1418, 189, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1420, 189, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1422, 189, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1424, 189, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1426, 189, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1428, 189, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1430, 189, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1432, 189, 53, NULL, NULL, NULL, NULL, NULL, N'/media/k30bkhnr/american.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1434, 192, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1436, 192, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1438, 192, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1440, 192, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1442, 192, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1444, 192, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1446, 192, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1448, 192, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1450, 192, 53, NULL, NULL, NULL, NULL, NULL, N'/media/dy4gvogl/vn.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1452, 204, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1454, 204, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1456, 204, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1458, 204, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1460, 204, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1462, 204, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1464, 204, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1466, 204, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1468, 204, 53, NULL, NULL, NULL, NULL, NULL, N'/media/dy4gvogl/vn.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1470, 203, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1472, 203, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1474, 203, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1476, 203, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1478, 203, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1480, 203, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1482, 203, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1484, 203, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1486, 203, 53, NULL, NULL, NULL, NULL, NULL, N'/media/k30bkhnr/american.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1488, 205, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1490, 205, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1492, 205, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1494, 205, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1496, 205, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1498, 205, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1500, 205, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1502, 205, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1504, 205, 53, NULL, NULL, NULL, NULL, NULL, N'/media/dy4gvogl/vn.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1506, 208, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1507, 209, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1508, 208, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1509, 209, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1510, 208, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1511, 209, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1512, 208, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'chào bạn quan ')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1513, 209, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'chào bạn quan ')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1514, 206, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Welcome to my channel !!!</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1515, 210, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Welcome to my channel !!!</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1516, 206, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1517, 210, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1518, 206, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1519, 210, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1520, 206, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1521, 210, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1522, 206, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1523, 210, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1524, 206, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1525, 210, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1526, 206, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1527, 210, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1528, 206, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1529, 210, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1530, 206, 53, NULL, NULL, NULL, NULL, NULL, N'/media/k30bkhnr/american.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1531, 210, 53, NULL, NULL, NULL, NULL, NULL, N'/media/k30bkhnr/american.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1532, 200, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is the news page which contains the list of news in the system should be display here.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1534, 211, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is the news page which contains the list of news in the system should be display here.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1535, 212, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>This is the news page which contains the list of news in the system should be display here.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1536, 177, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1538, 177, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1540, 178, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1541, 214, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1542, 178, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1543, 214, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem3 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1544, 176, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1546, 176, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1548, 215, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1549, 216, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1550, 215, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1551, 216, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1552, 213, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1553, 217, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1554, 213, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
GO
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1555, 217, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem2 Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p> </p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1556, 207, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1557, 218, 37, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>Introspect: <span> A free + fully responsive<br />site template by TEMPLATED - Quan Le Hoang</span></p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1558, 207, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1559, 218, 38, NULL, NULL, NULL, NULL, NULL, N'/media/y1eottnb/banner.jpg', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1560, 207, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1561, 218, 43, NULL, NULL, NULL, NULL, NULL, N'/about-us', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1562, 207, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1563, 218, 44, NULL, NULL, NULL, NULL, NULL, N'About', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1564, 207, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1565, 218, 39, NULL, NULL, NULL, NULL, NULL, N'MAGNA ETIAM LOREM Quan', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1566, 207, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1567, 218, 40, NULL, NULL, NULL, NULL, NULL, NULL, N'Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1568, 207, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1569, 218, 41, NULL, NULL, NULL, NULL, NULL, N'/welcome', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1570, 207, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1571, 218, 42, NULL, NULL, NULL, NULL, NULL, N'Click Here', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1572, 207, 53, NULL, NULL, NULL, NULL, NULL, N'/media/dy4gvogl/vn.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1573, 218, 53, NULL, NULL, NULL, NULL, NULL, N'/media/dy4gvogl/vn.png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1574, 219, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1575, 220, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1576, 219, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1577, 220, 50, NULL, NULL, NULL, NULL, NULL, N'quanfx', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1578, 219, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1579, 220, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1580, 219, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'sssssssssssss')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1581, 220, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'sssssssssssss')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1582, 221, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1583, 222, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1584, 221, 50, NULL, NULL, NULL, NULL, NULL, N'admin', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1585, 222, 50, NULL, NULL, NULL, NULL, NULL, N'admin', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1586, 221, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1587, 222, 51, NULL, NULL, NULL, NULL, NULL, N'q.foxy98@gmail.com', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1588, 221, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'cuong an cac')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1589, 222, 52, NULL, NULL, NULL, NULL, NULL, NULL, N'cuong an cac')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1590, 223, 6, NULL, NULL, NULL, NULL, NULL, NULL, N'{"src":"/media/hwwde1d1/vn.png","crops":null}')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1591, 223, 7, NULL, NULL, 320, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1592, 223, 8, NULL, NULL, 320, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1593, 223, 9, NULL, NULL, NULL, NULL, NULL, N'40947', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1594, 223, 10, NULL, NULL, NULL, NULL, NULL, N'png', NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1595, 224, 48, NULL, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1597, 224, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>New4 content</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1599, 224, 54, NULL, NULL, NULL, NULL, NULL, NULL, N'umb://media/6c32ad3b197b4edea3efa7b934bb1561')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1601, 225, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1602, 226, 48, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1603, 225, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>New4 content</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1604, 226, 49, NULL, NULL, NULL, NULL, NULL, NULL, N'<p>New4 content</p>')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1605, 225, 54, NULL, NULL, NULL, NULL, NULL, NULL, N'umb://media/6c32ad3b197b4edea3efa7b934bb1561')
INSERT [dbo].[umbracoPropertyData] ([id], [versionId], [propertyTypeId], [languageId], [segment], [intValue], [decimalValue], [dateValue], [varcharValue], [textValue]) VALUES (1606, 226, 54, NULL, NULL, NULL, NULL, NULL, NULL, N'umb://media/6c32ad3b197b4edea3efa7b934bb1561')
SET IDENTITY_INSERT [dbo].[umbracoPropertyData] OFF
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'5d9ed00a-52b4-4860-b39c-0b15fb183244', N'8f97b92e-2706-4487-870e-c0a5da5b84da', CAST(N'2019-10-09T13:09:28.060' AS DateTime), N'/home/generic', N'', N'a6a4080c65b3e08b1923a7a25f45c8ff0fa8a799')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'ff8c1909-9ccc-445b-8bfb-1c6882d14f84', N'c7d5475d-7bfc-4315-bc61-3299894eb69c', CAST(N'2019-10-09T08:54:52.960' AS DateTime), N'/vn/news', N'', N'275a87343175dd903d32d1157ca217450b7c48ab')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'bb3d3950-1d51-402c-8920-2de85b324543', N'1eb223fc-2240-4817-9067-593659dbe988', CAST(N'2019-10-09T13:09:28.050' AS DateTime), N'/home', N'', N'fb365c1216b59e1cfc86950425867007a60f4435')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'ed85a86e-1d5b-4754-823b-5fdf36d1d7dd', N'5fa0207b-b188-4eca-939d-2b9bbb39da59', CAST(N'2019-10-09T14:03:36.890' AS DateTime), N'/vn/tin-tuc/news-2', N'', N'5fb99681db8dc7c9f05b60398d0974cf24cb026e')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'85409228-f8b8-4155-8c75-827807f5d0f6', N'ecc7e4d4-a051-493b-a094-13271f542170', CAST(N'2019-10-09T13:09:28.067' AS DateTime), N'/home/about-us/company-overview', N'', N'e0e041f74023d0cacdb6f25ed42a666dae732c0a')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'3c656d7f-92c5-4f36-b8cf-9a79e692ca95', N'0321f2e2-be8e-445f-93e9-9e277301ea8d', CAST(N'2019-10-09T13:09:28.063' AS DateTime), N'/home/elements', N'', N'096591a3c3f244baeb87534545e6ef659f68b5df')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'bbd91811-ebd9-45a0-ac3a-a151ebb7add7', N'5b3f3622-3a56-414f-becd-5bbc597fb1e2', CAST(N'2019-10-09T13:09:28.073' AS DateTime), N'/home/about-us/disclamer', N'', N'f00ad66317c3b83328e4f0b1324cf2709ca427be')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'ce31e271-7021-4ff4-9245-ae81ed9bc13e', N'1cadbbde-6b42-4cbb-ada6-1954724c1644', CAST(N'2019-10-09T13:09:28.077' AS DateTime), N'/home/welcome', N'', N'4c4a3c9d017e4e9c23445fe217601947d04d976a')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'faac1475-5c45-4dda-8e3e-b8c5a131b32c', N'c1ecc8c9-e06e-4174-b3d6-45ebb0d5e459', CAST(N'2019-10-09T08:54:46.200' AS DateTime), N'/vn/contact-us', N'', N'2661804f5fad0c10fc9e56002b6b763c7a38f8a2')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'71f7c841-2513-4858-87e2-c400e64d07a4', N'46c26d34-50b9-45fe-ac25-4e92ed9d942d', CAST(N'2019-10-09T14:03:28.600' AS DateTime), N'/vn/tin-tuc/news-1', N'', N'6d69b90e0948fd1b193f862ae0a3680767984910')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'c16a7624-f6d6-41db-83d1-d9b7ec596a77', N'f89cfbde-53e0-4045-bef1-d88e1a6cb43a', CAST(N'2019-10-09T13:09:28.073' AS DateTime), N'/home/about-us/privacy', N'', N'c99a7aa820399a5fb26666330e3316eaed931049')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'4f0e485b-b3e6-432d-a969-e527df1398b9', N'c0b8ab1e-2019-4e39-b558-ad8e665bd024', CAST(N'2019-10-09T13:09:28.063' AS DateTime), N'/home/about-us', N'', N'677a2a9712800f57cf42ea558ce3823e725092b5')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'e6727d80-0551-4c0d-b446-ec448f9240e8', N'f6aec346-3f17-4354-9a9e-1da572fc1f6c', CAST(N'2019-10-09T08:55:02.817' AS DateTime), N'/vn/welcome', N'', N'81c882be9913832d8b239c6756f81c28ae7f3970')
INSERT [dbo].[umbracoRedirectUrl] ([id], [contentKey], [createDateUtc], [url], [culture], [urlHash]) VALUES (N'81266c1f-976c-471d-8fbb-ff9375eb5183', N'f7acdadb-8e68-4577-86d3-4f9e7a2c9385', CAST(N'2019-10-09T08:55:26.483' AS DateTime), N'/vn/about-us', N'', N'143f1113e9610dbc9eb44c1639f2685e402708bc')
SET IDENTITY_INSERT [dbo].[umbracoRelation] ON 

INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (27, -1, 1120, 3, CAST(N'2019-10-04T11:42:56.430' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (28, 1120, 1131, 3, CAST(N'2019-10-04T11:42:56.470' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (29, 1120, 1132, 3, CAST(N'2019-10-04T11:42:56.520' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (30, 1120, 1133, 3, CAST(N'2019-10-04T11:42:56.523' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (31, 1120, 1134, 3, CAST(N'2019-10-04T11:42:56.527' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (32, 1120, 1135, 3, CAST(N'2019-10-04T11:42:56.533' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (33, -1, 1121, 3, CAST(N'2019-10-04T11:43:02.670' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (34, 1121, 1123, 3, CAST(N'2019-10-04T11:43:02.673' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (35, 1121, 1124, 3, CAST(N'2019-10-04T11:43:02.677' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (36, 1121, 1125, 3, CAST(N'2019-10-04T11:43:02.677' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (37, 1121, 1126, 3, CAST(N'2019-10-04T11:43:02.680' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (38, 1121, 1127, 3, CAST(N'2019-10-04T11:43:02.683' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (39, 1121, 1128, 3, CAST(N'2019-10-04T11:43:02.687' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (40, 1121, 1129, 3, CAST(N'2019-10-04T11:43:02.690' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (41, 1121, 1130, 3, CAST(N'2019-10-04T11:43:02.690' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (42, -1, 1119, 3, CAST(N'2019-10-04T11:43:09.547' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (43, 1119, 1122, 3, CAST(N'2019-10-04T11:43:09.547' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (44, 1119, 1137, 3, CAST(N'2019-10-04T11:43:09.550' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (45, -1, 1147, 2, CAST(N'2019-10-04T14:35:59.053' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (46, -1, 1150, 2, CAST(N'2019-10-07T10:29:12.440' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (47, 1178, 1179, 2, CAST(N'2019-10-09T11:58:38.420' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (48, 1141, 1189, 1, CAST(N'2019-10-09T15:45:59.433' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (49, 1148, 1190, 1, CAST(N'2019-10-09T15:45:59.633' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (50, 1149, 1191, 1, CAST(N'2019-10-09T15:45:59.640' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (51, 1154, 1192, 1, CAST(N'2019-10-09T15:45:59.647' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (52, 1155, 1193, 1, CAST(N'2019-10-09T15:45:59.653' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (53, 1156, 1194, 1, CAST(N'2019-10-09T15:45:59.660' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (54, 1157, 1195, 1, CAST(N'2019-10-09T15:45:59.663' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (55, 1162, 1196, 1, CAST(N'2019-10-09T15:45:59.670' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (56, 1170, 1197, 1, CAST(N'2019-10-09T15:45:59.677' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (57, 1171, 1198, 1, CAST(N'2019-10-09T15:45:59.680' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (58, 1172, 1199, 1, CAST(N'2019-10-09T15:45:59.683' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (59, 1173, 1200, 1, CAST(N'2019-10-09T15:45:59.690' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (60, 1178, 1201, 1, CAST(N'2019-10-09T15:45:59.697' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (61, 1180, 1202, 1, CAST(N'2019-10-09T15:45:59.700' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (62, 1181, 1203, 1, CAST(N'2019-10-09T15:45:59.720' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (63, 1182, 1204, 1, CAST(N'2019-10-09T15:45:59.727' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (64, 1183, 1205, 1, CAST(N'2019-10-09T15:45:59.730' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (65, 1184, 1206, 1, CAST(N'2019-10-09T15:45:59.737' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (66, 1185, 1207, 1, CAST(N'2019-10-09T15:45:59.740' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (67, 1186, 1208, 1, CAST(N'2019-10-09T15:45:59.747' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (68, 1187, 1209, 1, CAST(N'2019-10-09T15:45:59.750' AS DateTime), N'')
INSERT [dbo].[umbracoRelation] ([id], [parentId], [childId], [relType], [datetime], [comment]) VALUES (69, 1188, 1210, 1, CAST(N'2019-10-09T15:45:59.757' AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[umbracoRelation] OFF
SET IDENTITY_INSERT [dbo].[umbracoRelationType] ON 

INSERT [dbo].[umbracoRelationType] ([id], [typeUniqueId], [dual], [parentObjectType], [childObjectType], [name], [alias]) VALUES (1, N'4cbeb612-e689-3563-b755-bf3ede295433', 1, N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', N'Relate Document On Copy', N'relateDocumentOnCopy')
INSERT [dbo].[umbracoRelationType] ([id], [typeUniqueId], [dual], [parentObjectType], [childObjectType], [name], [alias]) VALUES (2, N'0cc3507c-66ab-3091-8913-3d998148e423', 0, N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', N'c66ba18e-eaf3-4cff-8a22-41b16d66a972', N'Relate Parent Document On Delete', N'relateParentDocumentOnDelete')
INSERT [dbo].[umbracoRelationType] ([id], [typeUniqueId], [dual], [parentObjectType], [childObjectType], [name], [alias]) VALUES (3, N'8307994f-faf2-3844-bab9-72d34514edf2', 0, N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', N'b796f64c-1f99-4ffb-b886-4bf4bc011a9c', N'Relate Parent Media Folder On Delete', N'relateParentMediaFolderOnDelete')
SET IDENTITY_INSERT [dbo].[umbracoRelationType] OFF
SET IDENTITY_INSERT [dbo].[umbracoServer] ON 

INSERT [dbo].[umbracoServer] ([id], [address], [computerName], [registeredDate], [lastNotifiedDate], [isActive], [isMaster]) VALUES (1, N'http://localhost:38083/umbraco', N'HSSSC1PCL01841//LM/W3SVC/2/ROOT', CAST(N'2019-10-03T17:06:32.567' AS DateTime), CAST(N'2019-10-11T18:08:09.607' AS DateTime), 1, 1)
INSERT [dbo].[umbracoServer] ([id], [address], [computerName], [registeredDate], [lastNotifiedDate], [isActive], [isMaster]) VALUES (2, N'http://localhost:38083/umbraco', N'QUANFX//LM/W3SVC/2/ROOT', CAST(N'2019-10-04T23:15:17.690' AS DateTime), CAST(N'2019-10-09T22:09:28.543' AS DateTime), 0, 0)
INSERT [dbo].[umbracoServer] ([id], [address], [computerName], [registeredDate], [lastNotifiedDate], [isActive], [isMaster]) VALUES (3, N'http://localhost:7419/umbraco', N'QUANFX//LM/W3SVC/25/ROOT', CAST(N'2019-10-06T20:59:04.107' AS DateTime), CAST(N'2019-10-06T21:09:04.333' AS DateTime), 0, 0)
SET IDENTITY_INSERT [dbo].[umbracoServer] OFF
SET IDENTITY_INSERT [dbo].[umbracoUser] ON 

INSERT [dbo].[umbracoUser] ([id], [userDisabled], [userNoConsole], [userName], [userLogin], [userPassword], [passwordConfig], [userEmail], [userLanguage], [securityStampToken], [failedLoginAttempts], [lastLockoutDate], [lastPasswordChangeDate], [lastLoginDate], [emailConfirmedDate], [invitedDate], [createDate], [updateDate], [avatar], [tourData]) VALUES (-1, 0, 0, N'Le Hoang Quan', N'q.foxy98@gmail.com', N'0TSlDJnyNSHSxjxe2CdAiQ==6JtPg3+rYo7B9ciZD5bvVLXhasZlVTqe3BA9x1jx5eU=', N'{"hashAlgorithm":"HMACSHA256"}', N'q.foxy98@gmail.com', N'en-US', N'a814fa93-0067-4a9f-88b6-3e2989dbc1dc', 0, NULL, CAST(N'2019-10-03T17:05:36.613' AS DateTime), CAST(N'2019-10-11T14:26:11.377' AS DateTime), NULL, NULL, CAST(N'2019-10-03T17:05:35.693' AS DateTime), CAST(N'2019-10-11T14:26:11.380' AS DateTime), NULL, N'[{"alias":"umbIntroIntroduction","completed":true,"disabled":false},{"alias":"theStarterKitTheContentSection","completed":true,"disabled":false},{"alias":"theStarterKitCreatingContent","completed":true,"disabled":false},{"alias":"theStarterKitTheMediaLibrary","completed":true,"disabled":false},{"alias":"theStarterKitAddingMediaToContent","completed":true,"disabled":false}]')
SET IDENTITY_INSERT [dbo].[umbracoUser] OFF
INSERT [dbo].[umbracoUser2UserGroup] ([userId], [userGroupId]) VALUES (-1, 1)
INSERT [dbo].[umbracoUser2UserGroup] ([userId], [userGroupId]) VALUES (-1, 5)
SET IDENTITY_INSERT [dbo].[umbracoUserGroup] ON 

INSERT [dbo].[umbracoUserGroup] ([id], [userGroupAlias], [userGroupName], [userGroupDefaultPermissions], [createDate], [updateDate], [icon], [startContentId], [startMediaId]) VALUES (1, N'admin', N'Administrators', N'CADMOSKTPIURZ:5F7ï', CAST(N'2019-10-03T17:05:36.143' AS DateTime), CAST(N'2019-10-03T17:05:36.143' AS DateTime), N'icon-medal', -1, -1)
INSERT [dbo].[umbracoUserGroup] ([id], [userGroupAlias], [userGroupName], [userGroupDefaultPermissions], [createDate], [updateDate], [icon], [startContentId], [startMediaId]) VALUES (2, N'writer', N'Writers', N'CAH:F', CAST(N'2019-10-03T17:05:36.147' AS DateTime), CAST(N'2019-10-03T17:05:36.147' AS DateTime), N'icon-edit', -1, -1)
INSERT [dbo].[umbracoUserGroup] ([id], [userGroupAlias], [userGroupName], [userGroupDefaultPermissions], [createDate], [updateDate], [icon], [startContentId], [startMediaId]) VALUES (3, N'editor', N'Editors', N'CADMOSKTPUZ:5Fï', CAST(N'2019-10-03T17:05:36.147' AS DateTime), CAST(N'2019-10-03T17:05:36.147' AS DateTime), N'icon-tools', -1, -1)
INSERT [dbo].[umbracoUserGroup] ([id], [userGroupAlias], [userGroupName], [userGroupDefaultPermissions], [createDate], [updateDate], [icon], [startContentId], [startMediaId]) VALUES (4, N'translator', N'Translators', N'AF', CAST(N'2019-10-03T17:05:36.147' AS DateTime), CAST(N'2019-10-03T17:05:36.147' AS DateTime), N'icon-globe', -1, -1)
INSERT [dbo].[umbracoUserGroup] ([id], [userGroupAlias], [userGroupName], [userGroupDefaultPermissions], [createDate], [updateDate], [icon], [startContentId], [startMediaId]) VALUES (5, N'sensitiveData', N'Sensitive data', N'', CAST(N'2019-10-03T17:05:36.150' AS DateTime), CAST(N'2019-10-03T17:05:36.150' AS DateTime), N'icon-lock', -1, -1)
SET IDENTITY_INSERT [dbo].[umbracoUserGroup] OFF
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (1, N'content')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (1, N'forms')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (1, N'media')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (1, N'member')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (1, N'packages')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (1, N'settings')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (1, N'users')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (2, N'content')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (3, N'content')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (3, N'forms')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (3, N'media')
INSERT [dbo].[umbracoUserGroup2App] ([userGroupId], [app]) VALUES (4, N'translation')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'fd888469-8a87-43ce-9a7d-018be0f3c951', -1, CAST(N'2019-10-07T14:27:32.747' AS DateTime), CAST(N'2019-10-07T14:59:13.987' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'041a4941-7e49-41c4-923c-01b3df393c08', -1, CAST(N'2019-10-04T06:20:04.860' AS DateTime), CAST(N'2019-10-04T06:30:19.740' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'2021e35c-4de8-4fa6-a52d-01c98670f704', -1, CAST(N'2019-10-07T03:27:22.967' AS DateTime), CAST(N'2019-10-07T04:03:11.460' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'c3c203fb-af69-4326-a886-02d30769359a', -1, CAST(N'2019-10-07T04:30:26.330' AS DateTime), CAST(N'2019-10-07T04:48:46.133' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'8d0d2ae7-8d07-4382-98b0-05a9443d8f44', -1, CAST(N'2019-10-04T02:42:18.523' AS DateTime), CAST(N'2019-10-04T02:49:21.203' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'e82d564c-6d41-48e7-af6a-0606d98ce327', -1, CAST(N'2019-10-07T04:03:11.537' AS DateTime), CAST(N'2019-10-07T04:04:24.370' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'dbfd1394-0a57-4d36-b2c9-0a2eb1da84bb', -1, CAST(N'2019-10-03T10:07:52.237' AS DateTime), CAST(N'2019-10-03T10:55:57.303' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'ad42cad0-017f-4e2f-b664-14c99edaa84c', -1, CAST(N'2019-10-10T09:53:21.990' AS DateTime), CAST(N'2019-10-10T09:54:41.530' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'a5e37664-998f-4260-8802-151179a1adad', -1, CAST(N'2019-10-08T09:31:44.933' AS DateTime), CAST(N'2019-10-08T09:43:03.303' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'267adc6f-9830-4ced-b0f6-151bd8b1b60a', -1, CAST(N'2019-10-09T03:46:13.283' AS DateTime), CAST(N'2019-10-09T04:02:29.207' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'f06de044-7560-4bd1-adb6-2a11ac0cd4c5', -1, CAST(N'2019-10-04T09:19:35.417' AS DateTime), CAST(N'2019-10-04T09:19:35.417' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'067af07b-54cc-4ee3-8284-2a94cbaa1125', -1, CAST(N'2019-10-07T02:54:39.413' AS DateTime), CAST(N'2019-10-07T02:54:39.437' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'91a3e352-dede-416b-97b9-2ce7db58fbd9', -1, CAST(N'2019-10-04T04:33:18.173' AS DateTime), CAST(N'2019-10-04T04:51:29.267' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'31008580-aa88-4228-bbdc-2e8458d19950', -1, CAST(N'2019-10-09T09:39:43.227' AS DateTime), CAST(N'2019-10-09T09:47:01.380' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'8a05d7e6-eba0-4bfa-a3c4-30f207231885', -1, CAST(N'2019-10-09T08:36:44.300' AS DateTime), CAST(N'2019-10-09T09:17:23.403' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'6b67b887-9267-4338-8448-329cedf7454b', -1, CAST(N'2019-10-09T13:57:46.187' AS DateTime), CAST(N'2019-10-09T14:16:23.537' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'654ed357-37a6-4aeb-8d48-3a7f9b66d7b6', -1, CAST(N'2019-10-09T13:09:19.073' AS DateTime), CAST(N'2019-10-09T13:14:35.060' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'bcb2e522-3924-494a-b312-3aeb696a7bd8', -1, CAST(N'2019-10-08T08:08:28.207' AS DateTime), CAST(N'2019-10-08T08:14:46.963' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'd416e5de-bfb7-4034-830d-3b88423a8b85', -1, CAST(N'2019-10-07T06:26:09.573' AS DateTime), CAST(N'2019-10-07T07:24:16.223' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'29d05921-d705-4add-b6e7-4042bd2dea91', -1, CAST(N'2019-10-04T08:38:47.417' AS DateTime), CAST(N'2019-10-04T08:38:47.460' AS DateTime), CAST(N'2019-10-04T08:38:49.750' AS DateTime), N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'97c1450c-b34c-4ef9-b42b-40a1cc215d71', -1, CAST(N'2019-10-04T07:14:47.360' AS DateTime), CAST(N'2019-10-04T07:19:24.640' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'd6f68998-cae1-4f28-ac91-452f1c1d7226', -1, CAST(N'2019-10-08T10:09:01.040' AS DateTime), CAST(N'2019-10-08T10:09:01.063' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'c095b427-ca42-4ab9-82c2-4f8623778987', -1, CAST(N'2019-10-10T09:15:16.437' AS DateTime), CAST(N'2019-10-10T09:16:37.157' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'48da9db2-5fbf-4324-816d-4fbf6b40a5cc', -1, CAST(N'2019-10-11T07:26:11.443' AS DateTime), CAST(N'2019-10-11T07:43:31.863' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'7cc364cd-fb58-4f8f-a607-5c62e603c2c9', -1, CAST(N'2019-10-04T16:15:31.927' AS DateTime), CAST(N'2019-10-04T16:15:31.927' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'6c881717-ee63-47d9-a374-5f0f92832aa5', -1, CAST(N'2019-10-08T03:56:19.160' AS DateTime), CAST(N'2019-10-08T04:42:01.987' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'252b7054-6b9b-4881-a2d5-6b8f2ce38ef7', -1, CAST(N'2019-10-08T08:30:50.467' AS DateTime), CAST(N'2019-10-08T08:33:36.767' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'612695ac-4751-4e27-ae2e-6cc7c8162e56', -1, CAST(N'2019-10-09T03:07:12.053' AS DateTime), CAST(N'2019-10-09T03:26:35.233' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'645dcc0d-eb8b-44c4-a127-709bb63fdd68', -1, CAST(N'2019-10-07T02:32:37.927' AS DateTime), CAST(N'2019-10-07T02:33:52.133' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'c07545ff-cea3-4a3e-90ae-7265689d2886', -1, CAST(N'2019-10-07T14:59:14.067' AS DateTime), CAST(N'2019-10-07T15:22:52.893' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'd6647666-da0f-40ad-bc66-766d82ae74af', -1, CAST(N'2019-10-07T08:24:46.887' AS DateTime), CAST(N'2019-10-07T08:46:32.163' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'79152f03-0024-4d69-9b84-7fb4268fab17', -1, CAST(N'2019-10-09T07:35:36.533' AS DateTime), CAST(N'2019-10-09T07:55:11.200' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'22de913b-d07f-4b91-a8d0-988d8678ce6a', -1, CAST(N'2019-10-06T14:00:28.380' AS DateTime), CAST(N'2019-10-06T14:14:16.013' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'6594bc9d-c76b-467f-803c-992036e3513e', -1, CAST(N'2019-10-09T03:26:36.243' AS DateTime), CAST(N'2019-10-09T03:45:53.957' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'343ef1a4-5914-41c6-a2cb-9a3e0d82c28e', -1, CAST(N'2019-10-07T15:51:22.647' AS DateTime), CAST(N'2019-10-07T15:51:22.780' AS DateTime), NULL, N'127.0.0.1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'412a02ad-6835-4cea-b9c6-9b3735a426f4', -1, CAST(N'2019-10-08T02:37:38.447' AS DateTime), CAST(N'2019-10-08T03:09:14.490' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'1ff88b9b-839c-415f-bae8-ab14daca5043', -1, CAST(N'2019-10-07T16:21:27.913' AS DateTime), CAST(N'2019-10-07T16:27:03.957' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'175ea3b4-60a3-4ea0-b273-b9c04c81984c', -1, CAST(N'2019-10-09T04:43:39.710' AS DateTime), CAST(N'2019-10-09T05:00:52.610' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'6e189d75-f52f-4ca6-930a-c71ae3d2d45b', -1, CAST(N'2019-10-08T06:57:10.730' AS DateTime), CAST(N'2019-10-08T06:58:57.150' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'fa561103-b49e-4e12-be41-c90ecb5c3baa', -1, CAST(N'2019-10-04T08:38:52.247' AS DateTime), CAST(N'2019-10-04T08:38:52.247' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'029b6fc7-e745-4d95-9bab-cb86ebe965e9', -1, CAST(N'2019-10-11T06:57:07.333' AS DateTime), CAST(N'2019-10-11T07:02:22.237' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'2a20c1fa-ab6d-4c9a-9a73-cdb765426087', -1, CAST(N'2019-10-09T02:32:05.163' AS DateTime), CAST(N'2019-10-09T03:07:11.883' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'5bc9e561-e580-42cf-a6ab-e785f4978c3a', -1, CAST(N'2019-10-10T08:07:20.140' AS DateTime), CAST(N'2019-10-10T08:07:20.140' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'28f80654-e7f6-45b0-811b-f943944ee542', -1, CAST(N'2019-10-04T07:19:24.820' AS DateTime), CAST(N'2019-10-04T08:08:55.623' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'060f5c34-b2e3-4b1e-9649-fa66e42681e0', -1, CAST(N'2019-10-09T06:36:31.110' AS DateTime), CAST(N'2019-10-09T06:50:54.380' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'01632c39-40f7-40ff-a066-fc0b91d46b05', -1, CAST(N'2019-10-07T13:59:45.070' AS DateTime), CAST(N'2019-10-07T13:59:45.070' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'b6f80b0c-382a-4c2e-9cbf-fc8b30fabb6f', -1, CAST(N'2019-10-08T06:15:55.077' AS DateTime), CAST(N'2019-10-08T06:35:30.500' AS DateTime), NULL, N'::1')
INSERT [dbo].[umbracoUserLogin] ([sessionId], [userId], [loggedInUtc], [lastValidatedUtc], [loggedOutUtc], [ipAddress]) VALUES (N'002181da-6668-468b-9a0f-fdc964af645c', -1, CAST(N'2019-10-09T10:33:52.030' AS DateTime), CAST(N'2019-10-09T10:49:26.013' AS DateTime), NULL, N'::1')
ALTER TABLE [dbo].[cmsContentType] ADD  CONSTRAINT [DF_cmsContentType_thumbnail]  DEFAULT ('folder.png') FOR [thumbnail]
GO
ALTER TABLE [dbo].[cmsContentType] ADD  CONSTRAINT [DF_cmsContentType_isContainer]  DEFAULT ('0') FOR [isContainer]
GO
ALTER TABLE [dbo].[cmsContentType] ADD  CONSTRAINT [DF_cmsContentType_isElement]  DEFAULT ('0') FOR [isElement]
GO
ALTER TABLE [dbo].[cmsContentType] ADD  CONSTRAINT [DF_cmsContentType_allowAtRoot]  DEFAULT ('0') FOR [allowAtRoot]
GO
ALTER TABLE [dbo].[cmsContentType] ADD  CONSTRAINT [DF_cmsContentType_variations]  DEFAULT ('1') FOR [variations]
GO
ALTER TABLE [dbo].[cmsContentTypeAllowedContentType] ADD  CONSTRAINT [df_cmsContentTypeAllowedContentType_sortOrder]  DEFAULT ('0') FOR [SortOrder]
GO
ALTER TABLE [dbo].[cmsDocumentType] ADD  CONSTRAINT [DF_cmsDocumentType_IsDefault]  DEFAULT ('0') FOR [IsDefault]
GO
ALTER TABLE [dbo].[cmsMacro] ADD  CONSTRAINT [DF_cmsMacro_macroUseInEditor]  DEFAULT ('0') FOR [macroUseInEditor]
GO
ALTER TABLE [dbo].[cmsMacro] ADD  CONSTRAINT [DF_cmsMacro_macroRefreshRate]  DEFAULT ('0') FOR [macroRefreshRate]
GO
ALTER TABLE [dbo].[cmsMacro] ADD  CONSTRAINT [DF_cmsMacro_macroCacheByPage]  DEFAULT ('1') FOR [macroCacheByPage]
GO
ALTER TABLE [dbo].[cmsMacro] ADD  CONSTRAINT [DF_cmsMacro_macroCachePersonalized]  DEFAULT ('0') FOR [macroCachePersonalized]
GO
ALTER TABLE [dbo].[cmsMacro] ADD  CONSTRAINT [DF_cmsMacro_macroDontRender]  DEFAULT ('0') FOR [macroDontRender]
GO
ALTER TABLE [dbo].[cmsMacroProperty] ADD  CONSTRAINT [DF_cmsMacroProperty_macroPropertySortOrder]  DEFAULT ('0') FOR [macroPropertySortOrder]
GO
ALTER TABLE [dbo].[cmsMember] ADD  CONSTRAINT [DF_cmsMember_Email]  DEFAULT ('''') FOR [Email]
GO
ALTER TABLE [dbo].[cmsMember] ADD  CONSTRAINT [DF_cmsMember_LoginName]  DEFAULT ('''') FOR [LoginName]
GO
ALTER TABLE [dbo].[cmsMember] ADD  CONSTRAINT [DF_cmsMember_Password]  DEFAULT ('''') FOR [Password]
GO
ALTER TABLE [dbo].[cmsMemberType] ADD  CONSTRAINT [DF_cmsMemberType_memberCanEdit]  DEFAULT ('0') FOR [memberCanEdit]
GO
ALTER TABLE [dbo].[cmsMemberType] ADD  CONSTRAINT [DF_cmsMemberType_viewOnProfile]  DEFAULT ('0') FOR [viewOnProfile]
GO
ALTER TABLE [dbo].[cmsMemberType] ADD  CONSTRAINT [DF_cmsMemberType_isSensitive]  DEFAULT ('0') FOR [isSensitive]
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD  CONSTRAINT [DF_cmsPropertyType_sortOrder]  DEFAULT ('0') FOR [sortOrder]
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD  CONSTRAINT [DF_cmsPropertyType_mandatory]  DEFAULT ('0') FOR [mandatory]
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD  CONSTRAINT [DF_cmsPropertyType_variations]  DEFAULT ('1') FOR [variations]
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD  CONSTRAINT [DF_cmsPropertyType_UniqueID]  DEFAULT (newid()) FOR [UniqueID]
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] ADD  CONSTRAINT [DF_cmsPropertyTypeGroup_uniqueID]  DEFAULT (newid()) FOR [uniqueID]
GO
ALTER TABLE [dbo].[umbracoAccess] ADD  CONSTRAINT [DF_umbracoAccess_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[umbracoAccess] ADD  CONSTRAINT [DF_umbracoAccess_updateDate]  DEFAULT (getdate()) FOR [updateDate]
GO
ALTER TABLE [dbo].[umbracoAccessRule] ADD  CONSTRAINT [DF_umbracoAccessRule_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[umbracoAccessRule] ADD  CONSTRAINT [DF_umbracoAccessRule_updateDate]  DEFAULT (getdate()) FOR [updateDate]
GO
ALTER TABLE [dbo].[umbracoAudit] ADD  CONSTRAINT [DF_umbracoAudit_eventDateUtc]  DEFAULT (getdate()) FOR [eventDateUtc]
GO
ALTER TABLE [dbo].[umbracoCacheInstruction] ADD  CONSTRAINT [DF_umbracoCacheInstruction_instructionCount]  DEFAULT ('1') FOR [instructionCount]
GO
ALTER TABLE [dbo].[umbracoConsent] ADD  CONSTRAINT [DF_umbracoConsent_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[umbracoContentVersion] ADD  CONSTRAINT [DF_umbracoContentVersion_versionDate]  DEFAULT (getdate()) FOR [versionDate]
GO
ALTER TABLE [dbo].[umbracoExternalLogin] ADD  CONSTRAINT [DF_umbracoExternalLogin_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[umbracoKeyValue] ADD  CONSTRAINT [DF_umbracoKeyValue_updated]  DEFAULT (getdate()) FOR [updated]
GO
ALTER TABLE [dbo].[umbracoLanguage] ADD  CONSTRAINT [DF_umbracoLanguage_isDefaultVariantLang]  DEFAULT ('0') FOR [isDefaultVariantLang]
GO
ALTER TABLE [dbo].[umbracoLanguage] ADD  CONSTRAINT [DF_umbracoLanguage_mandatory]  DEFAULT ('0') FOR [mandatory]
GO
ALTER TABLE [dbo].[umbracoLog] ADD  CONSTRAINT [DF_umbracoLog_Datestamp]  DEFAULT (getdate()) FOR [Datestamp]
GO
ALTER TABLE [dbo].[umbracoNode] ADD  CONSTRAINT [DF_umbracoNode_uniqueId]  DEFAULT (newid()) FOR [uniqueId]
GO
ALTER TABLE [dbo].[umbracoNode] ADD  CONSTRAINT [DF_umbracoNode_trashed]  DEFAULT ('0') FOR [trashed]
GO
ALTER TABLE [dbo].[umbracoNode] ADD  CONSTRAINT [DF_umbracoNode_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[umbracoRelation] ADD  CONSTRAINT [DF_umbracoRelation_datetime]  DEFAULT (getdate()) FOR [datetime]
GO
ALTER TABLE [dbo].[umbracoServer] ADD  CONSTRAINT [DF_umbracoServer_registeredDate]  DEFAULT (getdate()) FOR [registeredDate]
GO
ALTER TABLE [dbo].[umbracoUser] ADD  CONSTRAINT [DF_umbracoUser_userDisabled]  DEFAULT ('0') FOR [userDisabled]
GO
ALTER TABLE [dbo].[umbracoUser] ADD  CONSTRAINT [DF_umbracoUser_userNoConsole]  DEFAULT ('0') FOR [userNoConsole]
GO
ALTER TABLE [dbo].[umbracoUser] ADD  CONSTRAINT [DF_umbracoUser_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[umbracoUser] ADD  CONSTRAINT [DF_umbracoUser_updateDate]  DEFAULT (getdate()) FOR [updateDate]
GO
ALTER TABLE [dbo].[umbracoUserGroup] ADD  CONSTRAINT [DF_umbracoUserGroup_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[umbracoUserGroup] ADD  CONSTRAINT [DF_umbracoUserGroup_updateDate]  DEFAULT (getdate()) FOR [updateDate]
GO
ALTER TABLE [dbo].[cmsContentNu]  WITH NOCHECK ADD  CONSTRAINT [FK_cmsContentNu_umbracoContent_nodeId] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoContent] ([nodeId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[cmsContentNu] CHECK CONSTRAINT [FK_cmsContentNu_umbracoContent_nodeId]
GO
ALTER TABLE [dbo].[cmsContentType]  WITH CHECK ADD  CONSTRAINT [FK_cmsContentType_umbracoNode_id] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsContentType] CHECK CONSTRAINT [FK_cmsContentType_umbracoNode_id]
GO
ALTER TABLE [dbo].[cmsContentType2ContentType]  WITH CHECK ADD  CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_child] FOREIGN KEY([childContentTypeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] CHECK CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_child]
GO
ALTER TABLE [dbo].[cmsContentType2ContentType]  WITH CHECK ADD  CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_parent] FOREIGN KEY([parentContentTypeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] CHECK CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_parent]
GO
ALTER TABLE [dbo].[cmsDocumentType]  WITH CHECK ADD  CONSTRAINT [FK_cmsDocumentType_umbracoNode_id] FOREIGN KEY([contentTypeNodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsDocumentType] CHECK CONSTRAINT [FK_cmsDocumentType_umbracoNode_id]
GO
ALTER TABLE [dbo].[cmsLanguageText]  WITH CHECK ADD  CONSTRAINT [FK_cmsLanguageText_umbracoLanguage_id] FOREIGN KEY([languageId])
REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[cmsLanguageText] CHECK CONSTRAINT [FK_cmsLanguageText_umbracoLanguage_id]
GO
ALTER TABLE [dbo].[cmsMacroProperty]  WITH CHECK ADD  CONSTRAINT [FK_cmsMacroProperty_cmsMacro_id] FOREIGN KEY([macro])
REFERENCES [dbo].[cmsMacro] ([id])
GO
ALTER TABLE [dbo].[cmsMacroProperty] CHECK CONSTRAINT [FK_cmsMacroProperty_cmsMacro_id]
GO
ALTER TABLE [dbo].[cmsMember]  WITH CHECK ADD  CONSTRAINT [FK_cmsMember_umbracoContent_nodeId] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
ALTER TABLE [dbo].[cmsMember] CHECK CONSTRAINT [FK_cmsMember_umbracoContent_nodeId]
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup]  WITH CHECK ADD  CONSTRAINT [FK_cmsMember2MemberGroup_cmsMember_nodeId] FOREIGN KEY([Member])
REFERENCES [dbo].[cmsMember] ([nodeId])
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] CHECK CONSTRAINT [FK_cmsMember2MemberGroup_cmsMember_nodeId]
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup]  WITH CHECK ADD  CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id] FOREIGN KEY([MemberGroup])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] CHECK CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id]
GO
ALTER TABLE [dbo].[cmsMemberType]  WITH CHECK ADD  CONSTRAINT [FK_cmsMemberType_umbracoNode_id] FOREIGN KEY([NodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsMemberType] CHECK CONSTRAINT [FK_cmsMemberType_umbracoNode_id]
GO
ALTER TABLE [dbo].[cmsPropertyType]  WITH CHECK ADD  CONSTRAINT [FK_cmsPropertyType_cmsPropertyTypeGroup_id] FOREIGN KEY([propertyTypeGroupId])
REFERENCES [dbo].[cmsPropertyTypeGroup] ([id])
GO
ALTER TABLE [dbo].[cmsPropertyType] CHECK CONSTRAINT [FK_cmsPropertyType_cmsPropertyTypeGroup_id]
GO
ALTER TABLE [dbo].[cmsPropertyType]  WITH CHECK ADD  CONSTRAINT [FK_cmsPropertyType_umbracoDataType_nodeId] FOREIGN KEY([dataTypeId])
REFERENCES [dbo].[umbracoDataType] ([nodeId])
GO
ALTER TABLE [dbo].[cmsPropertyType] CHECK CONSTRAINT [FK_cmsPropertyType_umbracoDataType_nodeId]
GO
ALTER TABLE [dbo].[cmsTagRelationship]  WITH CHECK ADD  CONSTRAINT [FK_cmsTagRelationship_cmsContent] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
ALTER TABLE [dbo].[cmsTagRelationship] CHECK CONSTRAINT [FK_cmsTagRelationship_cmsContent]
GO
ALTER TABLE [dbo].[cmsTagRelationship]  WITH CHECK ADD  CONSTRAINT [FK_cmsTagRelationship_cmsPropertyType] FOREIGN KEY([propertyTypeId])
REFERENCES [dbo].[cmsPropertyType] ([id])
GO
ALTER TABLE [dbo].[cmsTagRelationship] CHECK CONSTRAINT [FK_cmsTagRelationship_cmsPropertyType]
GO
ALTER TABLE [dbo].[cmsTagRelationship]  WITH CHECK ADD  CONSTRAINT [FK_cmsTagRelationship_cmsTags_id] FOREIGN KEY([tagId])
REFERENCES [dbo].[cmsTags] ([id])
GO
ALTER TABLE [dbo].[cmsTagRelationship] CHECK CONSTRAINT [FK_cmsTagRelationship_cmsTags_id]
GO
ALTER TABLE [dbo].[cmsTags]  WITH CHECK ADD  CONSTRAINT [FK_cmsTags_umbracoLanguage_id] FOREIGN KEY([languageId])
REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[cmsTags] CHECK CONSTRAINT [FK_cmsTags_umbracoLanguage_id]
GO
ALTER TABLE [dbo].[cmsTemplate]  WITH CHECK ADD  CONSTRAINT [FK_cmsTemplate_umbracoNode] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsTemplate] CHECK CONSTRAINT [FK_cmsTemplate_umbracoNode]
GO
ALTER TABLE [dbo].[umbracoAccess]  WITH CHECK ADD  CONSTRAINT [FK_umbracoAccess_umbracoNode_id] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoAccess] CHECK CONSTRAINT [FK_umbracoAccess_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoAccess]  WITH CHECK ADD  CONSTRAINT [FK_umbracoAccess_umbracoNode_id1] FOREIGN KEY([loginNodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoAccess] CHECK CONSTRAINT [FK_umbracoAccess_umbracoNode_id1]
GO
ALTER TABLE [dbo].[umbracoAccess]  WITH CHECK ADD  CONSTRAINT [FK_umbracoAccess_umbracoNode_id2] FOREIGN KEY([noAccessNodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoAccess] CHECK CONSTRAINT [FK_umbracoAccess_umbracoNode_id2]
GO
ALTER TABLE [dbo].[umbracoAccessRule]  WITH CHECK ADD  CONSTRAINT [FK_umbracoAccessRule_umbracoAccess_id] FOREIGN KEY([accessId])
REFERENCES [dbo].[umbracoAccess] ([id])
GO
ALTER TABLE [dbo].[umbracoAccessRule] CHECK CONSTRAINT [FK_umbracoAccessRule_umbracoAccess_id]
GO
ALTER TABLE [dbo].[umbracoContent]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContent_umbracoNode_id] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoContent] CHECK CONSTRAINT [FK_umbracoContent_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoContentSchedule]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContentSchedule_umbracoContent_nodeId] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
ALTER TABLE [dbo].[umbracoContentSchedule] CHECK CONSTRAINT [FK_umbracoContentSchedule_umbracoContent_nodeId]
GO
ALTER TABLE [dbo].[umbracoContentSchedule]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContentSchedule_umbracoLanguage_id] FOREIGN KEY([languageId])
REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[umbracoContentSchedule] CHECK CONSTRAINT [FK_umbracoContentSchedule_umbracoLanguage_id]
GO
ALTER TABLE [dbo].[umbracoContentVersion]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContentVersion_umbracoContent_nodeId] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
ALTER TABLE [dbo].[umbracoContentVersion] CHECK CONSTRAINT [FK_umbracoContentVersion_umbracoContent_nodeId]
GO
ALTER TABLE [dbo].[umbracoContentVersion]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContentVersion_umbracoUser_id] FOREIGN KEY([userId])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoContentVersion] CHECK CONSTRAINT [FK_umbracoContentVersion_umbracoUser_id]
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoContentVersion_id] FOREIGN KEY([versionId])
REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] CHECK CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoContentVersion_id]
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoLanguage_id] FOREIGN KEY([languageId])
REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] CHECK CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoLanguage_id]
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoUser_id] FOREIGN KEY([availableUserId])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] CHECK CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoUser_id]
GO
ALTER TABLE [dbo].[umbracoDataType]  WITH CHECK ADD  CONSTRAINT [FK_umbracoDataType_umbracoNode_id] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoDataType] CHECK CONSTRAINT [FK_umbracoDataType_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoDocument]  WITH CHECK ADD  CONSTRAINT [FK_umbracoDocument_umbracoContent_nodeId] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
ALTER TABLE [dbo].[umbracoDocument] CHECK CONSTRAINT [FK_umbracoDocument_umbracoContent_nodeId]
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoLanguage_id] FOREIGN KEY([languageId])
REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] CHECK CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoLanguage_id]
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoNode_id] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] CHECK CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoDocumentVersion]  WITH CHECK ADD  CONSTRAINT [FK_umbracoDocumentVersion_umbracoContentVersion_id] FOREIGN KEY([id])
REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
ALTER TABLE [dbo].[umbracoDocumentVersion] CHECK CONSTRAINT [FK_umbracoDocumentVersion_umbracoContentVersion_id]
GO
ALTER TABLE [dbo].[umbracoDomain]  WITH CHECK ADD  CONSTRAINT [FK_umbracoDomain_umbracoNode_id] FOREIGN KEY([domainRootStructureID])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoDomain] CHECK CONSTRAINT [FK_umbracoDomain_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoLanguage]  WITH CHECK ADD  CONSTRAINT [FK_umbracoLanguage_umbracoLanguage_id] FOREIGN KEY([fallbackLanguageId])
REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[umbracoLanguage] CHECK CONSTRAINT [FK_umbracoLanguage_umbracoLanguage_id]
GO
ALTER TABLE [dbo].[umbracoLog]  WITH CHECK ADD  CONSTRAINT [FK_umbracoLog_umbracoUser_id] FOREIGN KEY([userId])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoLog] CHECK CONSTRAINT [FK_umbracoLog_umbracoUser_id]
GO
ALTER TABLE [dbo].[umbracoMediaVersion]  WITH CHECK ADD  CONSTRAINT [FK_umbracoMediaVersion_umbracoContentVersion_id] FOREIGN KEY([id])
REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
ALTER TABLE [dbo].[umbracoMediaVersion] CHECK CONSTRAINT [FK_umbracoMediaVersion_umbracoContentVersion_id]
GO
ALTER TABLE [dbo].[umbracoNode]  WITH CHECK ADD  CONSTRAINT [FK_umbracoNode_umbracoNode_id] FOREIGN KEY([parentId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoNode] CHECK CONSTRAINT [FK_umbracoNode_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoNode]  WITH CHECK ADD  CONSTRAINT [FK_umbracoNode_umbracoUser_id] FOREIGN KEY([nodeUser])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoNode] CHECK CONSTRAINT [FK_umbracoNode_umbracoUser_id]
GO
ALTER TABLE [dbo].[umbracoPropertyData]  WITH CHECK ADD  CONSTRAINT [FK_umbracoPropertyData_cmsPropertyType_id] FOREIGN KEY([propertyTypeId])
REFERENCES [dbo].[cmsPropertyType] ([id])
GO
ALTER TABLE [dbo].[umbracoPropertyData] CHECK CONSTRAINT [FK_umbracoPropertyData_cmsPropertyType_id]
GO
ALTER TABLE [dbo].[umbracoPropertyData]  WITH CHECK ADD  CONSTRAINT [FK_umbracoPropertyData_umbracoContentVersion_id] FOREIGN KEY([versionId])
REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
ALTER TABLE [dbo].[umbracoPropertyData] CHECK CONSTRAINT [FK_umbracoPropertyData_umbracoContentVersion_id]
GO
ALTER TABLE [dbo].[umbracoPropertyData]  WITH CHECK ADD  CONSTRAINT [FK_umbracoPropertyData_umbracoLanguage_id] FOREIGN KEY([languageId])
REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[umbracoPropertyData] CHECK CONSTRAINT [FK_umbracoPropertyData_umbracoLanguage_id]
GO
ALTER TABLE [dbo].[umbracoRelation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoRelation_umbracoNode] FOREIGN KEY([parentId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoRelation] CHECK CONSTRAINT [FK_umbracoRelation_umbracoNode]
GO
ALTER TABLE [dbo].[umbracoRelation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoRelation_umbracoNode1] FOREIGN KEY([childId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoRelation] CHECK CONSTRAINT [FK_umbracoRelation_umbracoNode1]
GO
ALTER TABLE [dbo].[umbracoRelation]  WITH CHECK ADD  CONSTRAINT [FK_umbracoRelation_umbracoRelationType_id] FOREIGN KEY([relType])
REFERENCES [dbo].[umbracoRelationType] ([id])
GO
ALTER TABLE [dbo].[umbracoRelation] CHECK CONSTRAINT [FK_umbracoRelation_umbracoRelationType_id]
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoNode_id] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify] CHECK CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoUser_id] FOREIGN KEY([userId])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify] CHECK CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoUser_id]
GO
ALTER TABLE [dbo].[umbracoUser2UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUser2UserGroup_umbracoUser_id] FOREIGN KEY([userId])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoUser2UserGroup] CHECK CONSTRAINT [FK_umbracoUser2UserGroup_umbracoUser_id]
GO
ALTER TABLE [dbo].[umbracoUser2UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUser2UserGroup_umbracoUserGroup_id] FOREIGN KEY([userGroupId])
REFERENCES [dbo].[umbracoUserGroup] ([id])
GO
ALTER TABLE [dbo].[umbracoUser2UserGroup] CHECK CONSTRAINT [FK_umbracoUser2UserGroup_umbracoUserGroup_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_startContentId_umbracoNode_id] FOREIGN KEY([startContentId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup] CHECK CONSTRAINT [FK_startContentId_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_startMediaId_umbracoNode_id] FOREIGN KEY([startMediaId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup] CHECK CONSTRAINT [FK_startMediaId_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup2App]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUserGroup2App_umbracoUserGroup_id] FOREIGN KEY([userGroupId])
REFERENCES [dbo].[umbracoUserGroup] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup2App] CHECK CONSTRAINT [FK_umbracoUserGroup2App_umbracoUserGroup_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id] FOREIGN KEY([nodeId])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] CHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoUserGroup_id] FOREIGN KEY([userGroupId])
REFERENCES [dbo].[umbracoUserGroup] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] CHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoUserGroup_id]
GO
ALTER TABLE [dbo].[umbracoUserLogin]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUserLogin_umbracoUser_id] FOREIGN KEY([userId])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoUserLogin] CHECK CONSTRAINT [FK_umbracoUserLogin_umbracoUser_id]
GO
ALTER TABLE [dbo].[umbracoUserStartNode]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUserStartNode_umbracoNode_id] FOREIGN KEY([startNode])
REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoUserStartNode] CHECK CONSTRAINT [FK_umbracoUserStartNode_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUserStartNode]  WITH CHECK ADD  CONSTRAINT [FK_umbracoUserStartNode_umbracoUser_id] FOREIGN KEY([userId])
REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoUserStartNode] CHECK CONSTRAINT [FK_umbracoUserStartNode_umbracoUser_id]
GO
